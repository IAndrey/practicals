package Part_7;

import Part_7.Ex_1.MySet;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.function.Predicate;

public class Test_Ex_1 {
    @Test
    public void Ex_1() {
        HashSet<Integer> actual = MySet.getSet(15);  //проверяю создание сета от 2
        HashSet<Integer> expected = getSet(15);
        Assert.assertEquals(expected, actual);

        Predicate<Integer> myFilter = MySet.createTester(15); //проверяю фильтр (по условию s^2 > n )
        Assert.assertTrue(myFilter.test(3));
        Assert.assertFalse(myFilter.test(4));

        HashSet<Integer> expectedResult = new HashSet<>();
        expectedResult.add(2);
        expectedResult.add(3);
        expectedResult.add(5);
        expectedResult.add(7);
        expectedResult.add(11);
        expectedResult.add(13);

        MySet.filterSet(actual, myFilter);

        Assert.assertEquals(expectedResult, actual);

    }

    public static HashSet<Integer> getSet(int n) {
        HashSet<Integer> set = new HashSet<>();
        for (int i = 2; i <= n; i++) {
            set.add(i);
        }
        return set;
    }

    public static Predicate<Integer> createTester(int n) {
        Predicate<Integer> filter = (setElement) -> {
            if (setElement * setElement > n) return false;
            else return true;
        };
        return filter;
    }
}
