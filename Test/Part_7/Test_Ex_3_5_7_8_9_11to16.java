package Part_7;

import Part_7.Ex_13.Cache;
import Part_7.Ex_14_15_16.UnchangeableList;
import Part_7.Ex_7.Ex_7;
import Part_7.Ex_8.Ex_8;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class Test_Ex_3_5_7_8_9_11to16 {

    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private ByteArrayOutputStream errContent = new ByteArrayOutputStream();

//    @Before
//    public void setUpStreams() {
//        outContent = new ByteArrayOutputStream();
//        errContent = new ByteArrayOutputStream();
//        System.setOut(new PrintStream(outContent));
//        System.setErr(new PrintStream(errContent));
//    }
//
//    @After
//    public void cleanUpStreams() {
//        System.setOut(null);
//        System.setErr(null);
//    }

    @Test
    public void Ex_3() {
        Assert.assertEquals(getSet(1, 6), Ex_3.union(getSet(1, 4), getSet(3, 6)));
        Assert.assertEquals(getSet(3, 4), Ex_3.intersect(getSet(1, 4), getSet(3, 6)));
        Assert.assertEquals(getSet(1, 4), Ex_3.difference(getSet(1, 4), getSet(3, 6)));

    }

    private Set<String> getSet(int start, int finish) {
        Set<String> set = new HashSet<>();
        for (int i = start; i <= finish; i++) {
            set.add(i + "");
        }
        return set;
    }

    @Test
    public void Ex_5() {
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        Ex_5.swap(list, 0, 1);
        list.forEach(System.out::println);
        Assert.assertEquals("2\n1\n", outContent.toString().replaceAll("\r", ""));
    }

    @Test
    public void Ex_7() throws IOException {
        Map<String, Integer> actual = Ex_7.readFiles(new File("./Test/Part_7/Resources/Ex_7.txt"));
        Map<String, Integer> expected = new TreeMap<>();
        expected.put("aa", 1);
        expected.put("a", 1);
        expected.put("g", 1);
        expected.put("gg", 2);
        expected.put("s", 2);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void Ex_8() throws IOException {
        List<String> actual = Ex_8.readFiles(new File("./Test/Part_7/Resources/Ex_7.txt"));
        Collections.sort(actual);
        List<String> expected = new ArrayList<>();
        expected.add("a");
        expected.add("aa");
        expected.add("g");
        expected.add("gg");
        expected.add("gg");
        expected.add("s");
        expected.add("s");
        Collections.sort(expected);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void Ex_9() throws IOException {
        String[] strings = new String[5];
        strings[0] = "a";
        strings[1] = "b";
        strings[2] = "a";
        strings[3] = "b";
        strings[4] = "c";
        Map<String, Integer> expected = new HashMap<>();
        expected.put("a", 2);
        expected.put("b", 2);
        expected.put("c", 1);

        Assert.assertEquals(expected, Ex_9.myContains(strings));
        Assert.assertEquals(expected, Ex_9.myGet(strings));
        Assert.assertEquals(expected, Ex_9.myGetOrDefault(strings));
        Assert.assertEquals(expected, Ex_9.myMerge(strings));
        Assert.assertEquals(expected, Ex_9.putIfAbsent(strings));
    }

    @Test
    public void Ex_11_12() {
        String exp = "ad fa cd ot ho lb";
        StringBuilder actual = new StringBuilder();
        Ex_11.getWords(exp).forEach(actual::append);
        Assert.assertNotEquals(exp, actual);

        Assert.assertNotEquals(exp, Ex_12.getWords(exp));
    }

    @Test
    public void Ex_13() {
        Cache<Integer, String> expected = new Cache<>(10);
        for (int i = 0; i < 15; i++) {
            expected.put(i, "" + i);
        }

        Cache<Integer, String> actual = new Cache<>(10);
        for (int i = 0; i < 10; i++) {
            actual.put(i, "" + i);
        }

        Assert.assertEquals(expected, actual);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void Ex_14() {
        UnchangeableList<Integer> list = new UnchangeableList<>();
        list.add(1);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void Ex_15() {
        UnchangeableList<Integer> list = new UnchangeableList<>();
        list.size();
    }

    @Test
    public void Ex_16() {
        UnchangeableList<Integer> list = new UnchangeableList<>();
        list.getUnchangeableListAndHash(1000, (element) -> element*100);
        Set<Integer> hash= list.getCash();

        Set<Integer> expected = new HashSet<>();
        for (int i = 900; i < 1000; i++) {
            expected.add(i * 100);
        }
        Assert.assertEquals(expected, hash);
    }
}
