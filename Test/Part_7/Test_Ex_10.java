package Part_7;

import Part_7.Ex_10.City;
import Part_7.Ex_10.Ex_10;
import Part_7.Ex_10.Neighbor;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class Test_Ex_10 {
    @Test
    public void Ex_10() {
        City city1 = new City("Samara");
        City city2 = new City("Saratov");
        City city3 = new City("Togliatti");
        City city4 = new City("sizran");
        City city5 = new City("Penza");
        City city6 = new City("Moskva");

        city1.addNeighbor(new Neighbor(city2, 550));
        city1.addNeighbor(new Neighbor(city3, 100));

        city2.addNeighbor(new Neighbor(city1, 550));
        city2.addNeighbor(new Neighbor(city4, 300));
        city2.addNeighbor(new Neighbor(city6, 1200));

        city3.addNeighbor(new Neighbor(city1, 100));
        city3.addNeighbor(new Neighbor(city4, 50));

        city4.addNeighbor(new Neighbor(city3, 50));
        city4.addNeighbor(new Neighbor(city2, 300));
        city4.addNeighbor(new Neighbor(city5, 600));
        city4.addNeighbor(new Neighbor(city6, 900));

        city5.addNeighbor(new Neighbor(city4, 600));
        city5.addNeighbor(new Neighbor(city6, 150));

        city6.addNeighbor(new Neighbor(city5, 150));
        city6.addNeighbor(new Neighbor(city4, 900));
        city6.addNeighbor(new Neighbor(city2, 1200));

        Ex_10.searchLength(city6, city4);
        ArrayList<String> actual = Ex_10.getResult();
        actual.forEach(System.out::println);
        ArrayList<String> expected = new ArrayList<>();
        expected.add("Moskva - Penza - sizran, 750");
        expected.add("Moskva - sizran, 900");
        expected.add("Moskva - Saratov - sizran, 1500");
        expected.add("Moskva - Saratov - Samara - Togliatti - sizran, 1900");
        Assert.assertEquals(expected, actual);
    }
}
