package Part_7;

import Part_7.Ex_2.Ex_2;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class Test_Ex_2 {
    @Test
    public void Ex_2() {
        String [] expect = {"АНДРЕЙ", "НИКУЛИН"};
        String [] testElements = {"андрей", "никулин"};
        String [] actual = Ex_2.part1(testElements);
        String [] actual2 = Ex_2.part2(testElements);
        String [] actual3 = Ex_2.part3(testElements);

        Assert.assertArrayEquals(expect, actual);
        Assert.assertArrayEquals(expect, actual2);
        Assert.assertArrayEquals(expect, actual3);
    }
}
