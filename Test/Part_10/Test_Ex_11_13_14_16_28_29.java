package Part_10;

import Part_10.Ex_16.Ex_16;
import Part_10.Ex_16.MyRunnableCreateQueue;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.function.Supplier;

import Part_10.Ex_29;

public class Test_Ex_11_13_14_16_28_29 {
    @Test
    public void Ex_11() throws InterruptedException, IOException {
        Path root = Paths.get("./Test/Part_10/TestDirectory");
        ArrayBlockingQueue<File> queue = new ArrayBlockingQueue<>(10);
        Ex_11.searchWordInFileOnTwoThreads("search", queue);
        Thread.sleep(500);
        Ex_11.searchFileOneThreadAndAddInQueue(root, queue);

        Thread.sleep(3000);
    }

    @Test
    public void Ex_13() throws InterruptedException, ExecutionException {
        Path root = Paths.get("./Test/Part_10/TestDirectory");
        ArrayBlockingQueue<File> queue = new ArrayBlockingQueue<>(200);
        Ex_13.searchFileOneThreadAndAddInQueue(root, queue);
        Map<String, Integer> actual = Ex_13.searchWordInFileOnTwoThreads(queue);
        Map<String, Integer> exp = new LinkedHashMap<>();
        exp.put("gj", 6);
        exp.put("d", 6);
        exp.put("dj", 6);
        exp.put("dspf", 6);
        exp.put("ospd", 6);
        exp.put("as", 6);
        exp.put("s", 6);
        exp.put("nasd", 6);
        exp.put("dfkgndnv", 6);
        exp.put("mj", 6);
        Assert.assertEquals(exp, actual);
    }

    @Test
    public void Ex_14() throws InterruptedException, ExecutionException {
        Path root = Paths.get("./Test/Part_10/TestDirectory");
        ArrayBlockingQueue<Future<String[]>> queue = new ArrayBlockingQueue<>(200);
        Ex_14.searchFileOneThreadAndAddInQueue(root, queue);
        Thread.sleep(1000);
        Map<String, Integer> actual = Ex_14.searchWordInFileOnTwoThreads(queue);
        Map<String, Integer> exp = new LinkedHashMap<>();
        exp.put("gj", 6);
        exp.put("d", 6);
        exp.put("dj", 6);
        exp.put("dspf", 6);
        exp.put("ospd", 6);
        exp.put("as", 6);
        exp.put("s", 6);
        exp.put("nasd", 6);
        exp.put("dfkgndnv", 6);
        exp.put("mj", 6);
        Assert.assertEquals(exp, actual);
    }

    @Test
    public void Ex_16() throws InterruptedException, ExecutionException {
        ArrayBlockingQueue<File> queue = MyRunnableCreateQueue.getQueue(Paths.get("./Test/Part_10/TestDirectory"));
        ArrayList<String> list = new ArrayList<>();
        Map<String, Integer> actual = Ex_16.getMap(queue, list);
        Map<String, Integer> exp = new LinkedHashMap<>();
        exp.put("gj", 6);
        exp.put("d", 6);
        exp.put("dj", 6);
        exp.put("dspf", 6);
        exp.put("ospd", 6);
        exp.put("as", 6);
        exp.put("s", 6);
        exp.put("nasd", 6);
        exp.put("dfkgndnv", 6);
        exp.put("mj", 6);
        Assert.assertEquals(exp, actual);
    }

    @Test
    public void Ex_28() throws InterruptedException, ExecutionException {
        CompletableFuture<Integer> com = new CompletableFuture<>();
        com.completeAsync(() -> 1);
        CompletableFuture<Integer> com2 = new CompletableFuture<>();
        com2.completeAsync(() -> 2);
        List<CompletableFuture<Integer>> list = new ArrayList<>();
        list.add(com);
        list.add(com2);
        CompletableFuture<List<Integer>> actual = Ex_28.allOf(list);

        List<Integer> exp = new ArrayList<>();
        exp.add(1);
        exp.add(2);
        Assert.assertEquals(exp, actual.get());
     }

    @Test
    public void Ex_29() throws ExecutionException, InterruptedException {
        List<Supplier<String>> list = new ArrayList<>();
        String[] strings = {"test1", "test2", "test3", "test4", "test5", "test6", "test7"};
        list.add(Ex_29.getMySupplier(strings, "test10"));
        list.add(Ex_29.getMySupplier(strings, "test4"));
        CompletableFuture<String> cfs = Ex_29.anyOf(list, Executors.newFixedThreadPool(10));
        Assert.assertEquals("test4", cfs.get());
    }
}
