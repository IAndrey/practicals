package Part_4;

import Part_4.Ex_1_2_3.LabeledPoint;
import Part_4.Ex_1_2_3.Point;
import Part_4.Ex_12.*;
import Part_4.Ex_13.*;
import Part_4.Ex_4_5.Circle;
import Part_4.Ex_4_5.Line;
import Part_4.Ex_4_5.Rectangle;
import Part_4.Ex_6.DiscountedItem;
import Part_4.Ex_6.Item;
import Part_4.Ex_7.Color;
import Part_4.Ex_8.*;
import Part_4.Ex_9.Ex_9;
import org.junit.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Test_part4 {
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }

    @Test
    public void Ex_1_2_3() {
        LabeledPoint lp = new LabeledPoint("extednd", 5, 5);
        Point point = new Point(5, 5);
        Assert.assertEquals(lp, point);
        Assert.assertEquals("Point {x = 5.0, y = 5.0}", point.toString());
        Assert.assertEquals("LabeledPoint {x = 5.0, y = 5.0}", lp.toString());
    }

    @Test
    public void Ex_4_5() {
        Line line = new Line(new Point(10, 5), new Point(15, 10));
        Rectangle rectangle = new Rectangle(new Point(10, 10), 5, 5);
        Circle circle = new Circle(new Point(10, 10), 5);
        line.moveBy(5, 5);
        rectangle.moveBy(5, 5);
        circle.moveBy(5, 5);

        double[] expectLine = {15.0, 10.0};
        double[] expRectangle = {17.5, 12.5};
        double[] expCircle = {15.0, 15.0};

        double[] actualLine = {line.getCenter().getX(), line.getCenter().getY()};
        double[] actualRectangle = {rectangle.getCenter().getX(), rectangle.getCenter().getY()};
        double[] actualCircle = {circle.getCenter().getX(), circle.getCenter().getY()};
        Assert.assertArrayEquals(expectLine, actualLine, 0);
        Assert.assertArrayEquals(expRectangle, actualRectangle, 0);
        Assert.assertArrayEquals(expCircle, actualCircle, 0);
    }

    @Test
    public void Ex_4_6() {
        Item item = new Item("1", 6);
        Item item2 = new Item("1", 6);
        Assert.assertEquals(item, item2);

        DiscountedItem discountedItem = new DiscountedItem("1", 6, 10);
        DiscountedItem discountedItem2 = new DiscountedItem("1", 6, 10);

        Assert.assertEquals(item, discountedItem);
        Assert.assertEquals(discountedItem, discountedItem2);
    }

    @Test
    public void Ex_4_7() {
        Color color = Color.GREEN;
        Assert.assertEquals(color, color.getColor());
        Assert.assertEquals(Color.RED, Color.getRed());
    }

    @Test
    public void Ex_4_8() throws IOException {
        Ex_8.outExecuteClassMethods(new int[5]);
        Ex_8.outExecuteClassMethods(new MyClass<String>());
        Ex_8.outExecuteClassMethods(new MyClass.MyInnerClass());

        String exp = "int[]\n" +
                "int[]\n" +
                "int[]\n" +
                "class [I\n" +
                "[I\n" +
                "int[]\n" +
                "//---------------------------------------//\n" +
                "public class Part_4.Ex_8.MyClass<T>\n" +
                "MyClass\n" +
                "Part_4.Ex_8.MyClass\n" +
                "class Part_4.Ex_8.MyClass\n" +
                "Part_4.Ex_8.MyClass\n" +
                "Part_4.Ex_8.MyClass\n" +
                "//---------------------------------------//\n" +
                "public static class Part_4.Ex_8.MyClass$MyInnerClass\n" +
                "MyInnerClass\n" +
                "Part_4.Ex_8.MyClass.MyInnerClass\n" +
                "class Part_4.Ex_8.MyClass$MyInnerClass\n" +
                "Part_4.Ex_8.MyClass$MyInnerClass\n" +
                "Part_4.Ex_8.MyClass$MyInnerClass\n" +
                "//---------------------------------------//";

        Assert.assertEquals(exp.replaceAll("\n", ""), outContent.toString().replaceAll("[\r\n]", ""));
    }

    @Test
    public void Ex_4_9() {
        Ex_9.main(null);
        String exp = "{\n" +
                "String x=[str x],\n" +
                "double w=[10.0],\n" +
                "int y=[10],\n" +
                "}";

        Assert.assertEquals(exp.replaceAll("\n", ""), outContent.toString().replaceAll("[\r\n]", ""));
    }

    @Test
    public void Ex_4_10() {
        Ex_10.main(null);
        String exp = "finalize []\n" +
                "wait [long]\n" +
                "wait [long, int]\n" +
                "wait []\n" +
                "equals [class java.lang.Object]\n" +
                "toString []\n" +
                "hashCode []\n" +
                "getClass []\n" +
                "clone []\n" +
                "notify []\n" +
                "notifyAll []\n" +
                "registerNatives []";

        Assert.assertEquals(exp.replaceAll("\n", ""), outContent.toString().replaceAll("[\r\n]", ""));
    }

    @Test
    public void Ex_4_11() {
        Ex_11.main(null);
        Assert.assertEquals("Hello World!", outContent.toString().replaceAll("[\r\n-]", ""));
    }

    @Test
    public void Ex_4_12() {
        Long x = Ex_12.timeDefaultMethod();
        Long y = Ex_12.timeReflectMethod();
        boolean check = x > 0 && y > 0 && x < y;
        Assert.assertTrue(check);
    }

    @Test
    public void Ex_4_13() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method1 = System.out.getClass().getMethod("println", String.class);
        Method method2 = Math.class.getDeclaredMethod("sqrt", double.class);
        try {
            Ex_13.valuesTable(method1, 10, 22, 3);
            Assert.fail("Не прогружается ошибка, параметр метода не Double");
        } catch (RuntimeException e) {
        }
        Ex_13.valuesTable(method2, 16, 16, 3);
        String expStr = "4,00 ";
        Assert.assertEquals(expStr, outContent.toString().trim().replaceAll("[\r\n-]", ""));

        outContent = new ByteArrayOutputStream();
        System.setOut(null);
        System.setOut(new PrintStream(outContent));
        Ex_13.valuesTable(Math::sqrt, 16, 16, 3);
        Assert.assertEquals(expStr, outContent.toString().trim().replaceAll("[\r\n-]", ""));
        System.setOut(null);
    }

    @Test(expected = RuntimeException.class)
    public void Ex_4_13_Erros() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method1 = System.out.getClass().getMethod("println", String.class);
        Ex_13.valuesTable(method1, 10, 22, 3);
    }
}
