package Part_8;

import Part_8.Ex_2.Ex_2;
import Part_8.Ex_7.Ex_7;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test_Ex_2_5to10_14 {

    @Test
    public void Ex_2() throws IOException {
        Long actual1 = Ex_2.timeReadFileStream("./src/Part_8/Ex_2/textVoinaMir.txt");
        Long actual2 = Ex_2.timeReadFileParallelStream("./src/Part_8/Ex_2/textVoinaMir.txt");
        Assert.assertTrue(actual2 < actual1);
    }

    @Test
    public void Ex_5() {
        List<String> actual = Ex_5.myCodePoints("abcde").collect(Collectors.toList());
        List<String> expected = Arrays.asList("a", "b", "c", "d", "e");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void Ex_6() {
        String isWord = "word";
        Assert.assertTrue(Ex_6.isWord(isWord));
        isWord = "wo1rd";
        Assert.assertFalse(Ex_6.isWord(isWord));

        String isIndentificstor = "MyClass";
        Assert.assertTrue(Ex_6.isIdentificator(isIndentificstor));
        isIndentificstor = "_MyClass";
        Assert.assertTrue(Ex_6.isIdentificator(isIndentificstor));

        isIndentificstor = "1MyClass";
        Assert.assertFalse(Ex_6.isIdentificator(isIndentificstor));
        isIndentificstor = "My Class";
        Assert.assertFalse(Ex_6.isIdentificator(isIndentificstor));
        isIndentificstor = "public";
        Assert.assertFalse(Ex_6.isIdentificator(isIndentificstor));
    }

    @Test
    public void Ex_7() throws IOException {
        Map<String, Integer> actual = Ex_7.tenOftenWords("./Test/Part_8/Test_ex7.txt");
        actual.forEach((K, V) -> System.out.println(K + " " + V));

        Map<String, Integer> expected = new  LinkedHashMap<>();
        expected.put("dd", 4);
        expected.put("f", 3);
        expected.put("we", 2);
        expected.put("k", 2);
        expected.put("po", 2);
        expected.put("kl", 1);
        expected.put("o", 1);
        expected.put("mb", 1);
        expected.put("dsfdsf", 1);
        expected.put("kdsjfdposjf", 1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void Ex_9() throws IOException {
        List<String> actual = Ex_9.fiveWords("./Test/Part_8/Test_Ex8.txt");
        List<String> expected = new ArrayList<>();
        expected.add("dadedudido");
        expected.add("dadedudido");
        expected.add("dadedudido");
        expected.add("dadedudido");
        expected.add("dadedudido");
    }

    @Test
    public void Ex_10() {
        int actual = Ex_10.averageLength(Stream.of("sad", "sd", "sd", "sd"));
        Assert.assertEquals(2, actual);
    }

    @Test
    public void Ex_11() {
        List<String> actual = Ex_11.maxLengthWord(Stream.of("asa", "s", "asdds", "as", "oireg"));
        List<String> expected = new ArrayList<>();
        expected.add("asdds");
        expected.add("oireg");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void Ex_15() {
        double actual = Ex_15.getAverage(Stream.of(3.5, 3.5, 3.5));
        double expected = 3.5;
        Assert.assertEquals(actual, expected, 0);
    }
}
