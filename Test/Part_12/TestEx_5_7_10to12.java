package Part_12;


import Part_12.Ex_10_11.ClockEngine;
import Part_12.Ex_12.Task;
import Part_12.Ex_12.TaskEngine;
import Part_12.Ex_7.TimeInternal;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class TestEx_5_7_10to12 {
    private ByteArrayOutputStream out;

    @Before
    public void systemOutContent() {
        out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
    }

    @Test
    public void Ex_5() {
        LocalDate date = LocalDate.now();
        Assert.assertEquals(0, (long) Ex_5.getDays(date));
    }

    @Test
    public void Ex_7() {
        TimeInternal internal = new TimeInternal();
        internal.setDate(20, 12, 0, 14, 0);
        internal.setDate(20, 16, 0, 17, 0);

        boolean b = internal.intersection(20, 13, 0, 13, 20);
        Assert.assertTrue(b);

        boolean b2 = internal.intersection(20, 14, 0, 14, 20);
        Assert.assertFalse(b2);
    }

    @Test
    public void Ex_10() {
        ClockEngine clEng = new ClockEngine();
        clEng.finishCityTime("Лос-Анджелес", LocalTime.of(3, 5), "Франкфурт", LocalTime.of(10, 50));
        String exp = "Вы прибудете в место назначения Франкфурт в 22 55 по местному времени";
        Assert.assertEquals(exp, out.toString().replaceAll("[\r\n]", ""));
        systemOutContent();
        clEng.finishCityTime("Лос-Анджелес", LocalTime.of(3, 5), "Лос-Анджелес", LocalTime.of(10, 50));
        String exp2 = "Вы прибудете в место назначения Лос-Анджелес в 13 55 по местному времени";
        Assert.assertEquals(exp2, out.toString().replaceAll("[\r\n]", ""));
    }

    @Test
    public void Ex_11() {
        ClockEngine clEng = new ClockEngine();
        clEng.timeTravel("Лос-Анджелес", LocalTime.of(3, 5), "Франкфурт", LocalTime.of(22, 55));
        String exp = "Ваш путь из Лос-Анджелес в Франкфурт будет длиться 10 часов";
        Assert.assertEquals(exp, out.toString().replaceAll("[\r\n]", ""));
    }

    @Test
    public void Ex_12() {
        Task task1 = new Task(2019, 10, 18, 11, 0, 1, 0, "America/Belem");
        Task task2 = new Task(12, 30, 1, 0, "America/Belem");
        Task task3 = new Task(12, 0, 1, 0, "America/Belem");
        Task task4 = new Task(18, 13, 0, 1, 0, "America/Belem");
        TaskEngine myEngine = new TaskEngine("America/Belem");
        myEngine.addTask(task1, task2, task3, task4);
        List<Task> tasks = myEngine.getTasks(LocalTime.of(12, 0), LocalTime.of(1, 0));
        Assert.assertEquals(2, tasks.size());
    }
}
