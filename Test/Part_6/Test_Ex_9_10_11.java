package Part_6;

import Part_6.Ex_7_8.Pair;
import Part_6.Ex_9_10_11.Arrays;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;
import java.util.ArrayList;

public class Test_Ex_9_10_11 {


    @Test
    public void Ex_9() {
        ArrayList<Number> ints = new ArrayList<>();
        ints.add(1);
        ints.add(10.8);
        ints.add(new BigInteger("10000000000000000000000000000000000000000000000"));
        Pair<Number> pair = Arrays.firstLast(ints);
        Number[] expected = {1, new BigInteger("10000000000000000000000000000000000000000000000")};
        Number[] actual = {pair.getElement1(), pair.getElement2()};
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void Ex_10() {
        ArrayList<Number> ints = new ArrayList<>();
        ints.add(1);
        ints.add(10.8);
        ints.add(new BigInteger("10000000000000000000000000000000000000000000000"));
        Number[] expected = {1.0, 1.0E46};
        Number[] actual = {Arrays.min(ints), Arrays.max(ints)};
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void Ex_11() {
        ArrayList<Number> ints = new ArrayList<>();
        ints.add(1);
        ints.add(10.8);
        ints.add(new BigInteger("10000000000000000000000000000000000000000000000"));
        Pair<Number> pair = Arrays.minMax(ints);
        Number[] expected = {1.0, 1.0E46};
        Number[] actual = {pair.getElement1(), pair.getElement2()};
        Assert.assertArrayEquals(expected, actual);
    }
}
