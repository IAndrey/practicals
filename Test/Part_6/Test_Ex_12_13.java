package Part_6;

import Part_6.Ex_12_13.Ex_12_13;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class Test_Ex_12_13 implements ITest{
    @Test
    public void Ex_12() {
        List<Integer> list = init();
        List<Integer> result = new ArrayList<>() ;
        Ex_12_13.minMax(list, Integer::compareTo, result);
        int [] expected = {0, 9};
        int[] actual = {result.get(0), result.get(1)};
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void Ex_13() {
        List<Integer> list = init();
        List<Integer> result = new ArrayList<>() ;
        Ex_12_13.maxMin(list, Integer::compareTo, result);
        int [] expected = {9, 0};
        int[] actual = {result.get(0), result.get(1)};
        Assert.assertArrayEquals(expected, actual);
    }

}
