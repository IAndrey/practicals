package Part_6;


import Part_6.Ex_1and2.Ex_1_Stack;
import Part_6.Ex_1and2.Ex_2_Stack;
import Part_6.Ex_1and2.Ex_2_part_2_Stack;
import Part_6.Ex_1and2.IStack;
import org.junit.Assert;
import org.junit.Test;

public class Test_Ex_1_2 {


    @Test
    public void Ex_1() {
        testStack(new Ex_1_Stack<String>());
    }

    @Test
    public void Ex_2() {
        testStack(new Ex_2_Stack<String>());
    }

    @Test
    public void Ex_3() {
        testStack(new Ex_2_part_2_Stack<String>());
    }

    public void testStack(IStack stack) {
//        Ex_1_Stack<String> stack = new Ex_1_Stack<String>();
        stack.push("One");
        stack.push("Two");
        stack.push("Three");

        Assert.assertEquals("Three", stack.pop());
        Assert.assertEquals("Two", stack.pop());
        Assert.assertFalse(stack.isEmpty());
        stack.pop();
        Assert.assertTrue(stack.isEmpty());
    }

}
