package Part_6;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class Test_Ex_15 implements ITest {
    @Test
    public void Ex_15() {
        List<Integer> init = init();
        List<Integer> actual = Ex_15.map(init, (element) -> element + 10);
        Integer[] ecpected = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
        Integer[] actuals = new Integer[init.size()];
        actuals = actual.toArray(actuals);
        Assert.assertArrayEquals(ecpected, actuals);
    }
}
