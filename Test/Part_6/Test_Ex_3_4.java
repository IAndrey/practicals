package Part_6;

import Part_6.Ex_3_4.ITable;
import Part_6.Ex_3_4.Table;
import Part_6.Ex_3_4.Entry;
import Part_6.Ex_3_4.Table_ex4;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class Test_Ex_3_4 {

    @Test
    public void test_ex_4() {
        Table_ex4<Integer, String> elements = new Table_ex4<>();
        Table_ex4.Entry entry = elements. new Entry(1, "1");
        Table_ex4.Entry entry2 = elements. new Entry(2, "2");
        Table_ex4.Entry entry3 = elements. new Entry(3, "3");
        elements.add(entry);
        elements.add(entry2);
        elements.add(entry3);
        test_Table(elements);
    }

    @Test
    public void test_ex_3() {
        Table<Integer, String> elements = new Table<>();
        Entry<Integer, String> entry = new Entry<>(1, "1");
        Entry<Integer, String> entry2 = new Entry<>(2, "2");
        Entry<Integer, String> entry3 = new Entry<>(3, "3");
        elements.add(entry);
        elements.add(entry2);
        elements.add(entry3);
        test_Table(elements);
    }

    public static void test_Table(ITable<Integer, String> elements) {
        Assert.assertEquals("2", elements.getValue(2));
        Assert.assertEquals(3, elements.size());
        elements.removeKey(2);
        Assert.assertEquals(2, elements.size());

        try {
            Assert.assertEquals("2", elements.getValue(2));
            Assert.fail("Не пробросилась ошибка \"Ключ не найден\"");
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        Assert.assertEquals("1", elements.getValue(1));
        elements.setValue(1, "new 1");
        Assert.assertEquals("new 1", elements.getValue(1));

    }
}
