package Part_6;

import java.util.ArrayList;

public interface ITest {
    default ArrayList<Integer> init() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        return list;
    }
}
