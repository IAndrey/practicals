package Part_6;

import Part_6.Ex_21.Arrays;
import Part_6.Ex_23.Ex_23;
import Part_6.Ex_23.Exceptions;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

public class Test_Ex_20_21_22_23 {
    @Test
    public void Ex_20() {
        String[] actual = Ex_20.repeat(3, "Hi", "s");
        String[] expected = {"Hi", "Hi", "Hi", "s", "s", "s"};
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void Ex_21() {
        Map<Integer, String> list = new HashMap<>();
        list.put(1, "Hi");
        list.put(2, "Hi");
        Map<Integer, String>[] actual = Arrays.construct(10, list);
        actual[0] = list;
        actual[1] = list;

        Map<Integer, String>[] expected = new HashMap[10];
        expected[0] = list;
        expected[1] = list;

        Assert.assertEquals(expected, actual);
    }

    @Test(expected = ArithmeticException.class)
    public void Ex_22() {

        Ex_22.doWork(() -> 10/0, NullPointerException.class);
    }

    @Test
    public void Ex_23() throws IOException {

        Exceptions.doWork(() -> 10/0);
    }
}
