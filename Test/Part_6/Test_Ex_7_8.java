package Part_6;

import Part_6.Ex_7_8.Pair;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;

public class Test_Ex_7_8 {

    @Test
    public void test_ex_7() {
        Pair<Number> pair = new Pair<>(new BigInteger("10000000000000000000000000000"), 5.5);
        Assert.assertEquals(new BigInteger("10000000000000000000000000000"), pair.getElement1());
        Assert.assertEquals(5.5, pair.getElement2());
    }

    @Test
    public void test_ex_8() {
        Pair<Number> pair = new Pair<>(new BigInteger("10000000000000000000000000000"), 5.5);
        Assert.assertEquals(new BigInteger("10000000000000000000000000000"), pair.max());
        Assert.assertEquals(5.5, pair.min());
    }
}
