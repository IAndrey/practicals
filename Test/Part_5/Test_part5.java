package Part_5;

import Part_5.Ex_1.Ex_1;
import Part_5.Ex_4.Ex_4;
import Part_5.Ex_6.Ex_6;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

/*
Задания 7 - 10, 12, 15 - на теорию, на них проверки не делал
 */

public class Test_part5 {
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        outContent = new ByteArrayOutputStream();
        errContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    @Test
    public void Ex_1() throws IOException {
        ArrayList<Double> list = Ex_1.readValues("./Test/Part_5/Files/Ex_1.txt");
        ArrayList<Double> expList = new ArrayList<>();
        expList.add(2.0);
        expList.add(29.9);
        expList.add(0.0);
        expList.add(1000.0);
        String str = errContent.toString().replaceAll("[\n\r ]", "");
        Assert.assertEquals(expList, list);
        Assert.assertTrue(str.matches(".*java.lang.NumberFormatException: For input string: \"3dfdf\".*".replaceAll("[\n\r ]", "")));
    }

    @Test
    public void Ex_2() throws Exception {
        double expRezult = 1510.6;
        double actualRezult = Ex_2.sumOfValues("./Test/Part_5/Files/Ex_2.txt");
        Assert.assertEquals(expRezult, actualRezult, 0.0);

        String str = errContent.toString().replaceAll("[\n\r ]", "");
        Assert.assertTrue(str.matches(".*java.lang.NumberFormatException: For input string: \"3ds\".*".replaceAll("[\n\r ]", "")));
    }

    @Test
    public void Ex_3() throws Exception {
        double expRezult = 1510.6;
        double actualRezult = Ex_2.sumOfValues("./Test/Part_5/Files/Ex_2.txt");
        System.out.println(actualRezult);

        String strOut = outContent.toString().replaceAll("[\n\r ]", "");
        Assert.assertEquals("1510.6", strOut);

        String strErr = errContent.toString().replaceAll("[\n\r ]", "");
        Assert.assertTrue(strErr.matches(".*java.lang.NumberFormatException: For input string: \"3ds\".*".replaceAll("[\n\r ]", "")));
    }

    @Test
    public void Ex_4() {
//        double actualRezult = ;
        ArrayList<Double> doubles = Ex_4.readValues("./Test/Part_5/Files/Ex_2.t3xt");
        Assert.assertNull(doubles);

        ArrayList<Double> doubles2 = Ex_4.readValues("./Test/Part_5/Files/Ex_2.txt");
        Assert.assertNotNull(doubles2);

        Assert.assertEquals(-1, Ex_4.sumOfValues("./Test/Part_5/Files/Ex_2.t3xt"), 0.0); //в случае ошибки возвращает -1.

        Assert.assertEquals(1510.6, Ex_4.sumOfValues("./Test/Part_5/Files/Ex_2.txt"), 0.0);
    }

    @Test
    public void Ex_5() throws FileNotFoundException {
        String expected = "test string\n" +
                "one\n" +
                "two";
        InputStream stream = new FileInputStream("./Test/Part_5/Files/Ex_5.txt");
        String actual = Ex_5.myMethod(stream);

        Assert.assertEquals(expected.replaceAll("[\n\r ]", ""), actual.replaceAll("[\n\r ]", ""));
    }

    @Test
    public void Ex_6_meth_one() {
        Ex_6.myMethodOne("./Test/Part_5/Files/Ex_5.txt");
        String exp = ("test string\n" +
                "one\n" +
                "two\n" +
                "Exit").replaceAll("[\n\r ]", "");
        String strOut = outContent.toString().replaceAll("[\n\r ]", "");
        Assert.assertEquals(exp, strOut);
    }

    @Test
    public void Ex_6_meth_two() {
        Ex_6.myMethodTwo("./Test/Part_5/Files/Ex_5.txt");
        String exp = ("test string\n" +
                "one\n" +
                "two\n" +
                "Exit").replaceAll("[\n\r ]", "");
        String strOut = outContent.toString().replaceAll("[\n\r ]", "");
        Assert.assertEquals(exp, strOut);
    }

    @Test
    public void Ex_6_meth_three() {
        Ex_6.myMethodThree("./Test/Part_5/Files/Ex_5.txt");
        String exp = ("test string\n" +
                "one\n" +
                "two\n" +
                "Exit").replaceAll("[\n\r ]", "");
        String strOut = outContent.toString().replaceAll("[\n\r ]", "");
        Assert.assertEquals(exp, strOut);
    }

    @Test
    public void Ex_6_exception() {
        Ex_6.myMethodOne("./Test/Part_5/Files/Ex_51.txt");

        String strErrExpected = "Caught IOException: .\\Test\\Part_5\\Files\\Ex_51.txt";
        String strErrActual = errContent.toString().replaceAll("[\r\n]", "");
        Assert.assertEquals(strErrExpected, strErrActual);
    }

    @Test
    public void Ex_11_exception() {
        Assert.assertEquals(1, Ex_11.factorial(0), 0.0);
        Assert.assertEquals(1, Ex_11.factorial(0), 0.0);
        Assert.assertEquals(24, Ex_11.factorial(4), 0.0);
    }

    @Test
    public void Ex_13_exception() {
        int[] test = {20, 900, 21, -1, 10};
        int minActual = Ex_13.min(test);
        Assert.assertEquals(-1, minActual);
    }

    @Test
    public void Ex_14_exception() {
        Logger testLogger = Ex_14.getMyLogger("test.logger");
        Ex_14.setMyFilter(testLogger, "секс", "наркотики", "C++");
        testLogger.finer("секс");
        testLogger.finer("ава");
        testLogger.finer("dadasdd");
        testLogger.finer("наркотики");
        testLogger.finer("C++");

        boolean check = errContent.toString().contains("секс") || errContent.toString().contains("наркотики") ||
                errContent.toString().contains("C++");

        Assert.assertFalse(check);
    }
}
