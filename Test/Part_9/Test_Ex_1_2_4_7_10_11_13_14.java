package Part_9;

import Part_9.Ex_13.Ex_13;
import Part_9.Ex_13.MyTestClass;
import Part_9.Ex_14.Ex_14;
import Part_9.Ex_14.Point;
import Part_9.Ex_2.Ex_2;
import Part_9.Ex_7.Ex_7;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static Part_9.Ex_1.*;

public class Test_Ex_1_2_4_7_10_11_13_14 {
    @Test
    public void Ex_1() throws IOException {
        String expected = "testString";
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        myCopy2(new BufferedInputStream(new ByteArrayInputStream(expected.getBytes())), out);
        Assert.assertEquals(expected, out.toString());

        out = new ByteArrayOutputStream();

        myCopy(new BufferedInputStream(new ByteArrayInputStream(expected.getBytes())), out);
        Assert.assertEquals(expected, out.toString());
        out.close();
    }

    @Test
    public void Ex_2() throws IOException {
        Path path_ = Paths.get("./Test/Part_9/Resources/testEx_2.txt");
        Map<String, String> actual = Ex_2.getMap(Arrays.stream(Files.readString(path_).split("\\PL+")), path_);
        Map<String, String> expected = new LinkedHashMap<>();
        expected.put("aaaaa", "1");
        expected.put("a", "1, 3");
        expected.put("bbb", "2");
        expected.put("ccc", "3");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void Ex_4() throws IOException {
        int expected = 4;
        Path path = Paths.get("./Test/Part_9/Resources/Ex_4");
        Assert.assertEquals(expected, Ex_4.meReadScanner(path));
        Assert.assertEquals(expected, Ex_4.meReadBufferedReader(path));
        Assert.assertEquals(expected, Ex_4.meReadBufferedReader2(path));
    }

    @Test
    public void Ex_7() throws IOException, NoSuchAlgorithmException {
        String actual = Ex_7.getFileHash("./Test/Part_9/Resources/Ex_7.txt");
        String expected = "79a769c653c91aec8afc99e556cc8700dbf06f58";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void Ex_10() throws IOException, NoSuchAlgorithmException {
        String test = "dsk'sdn'sfoisbhfusofg-10apsd - 10 pa;sdjp10d+s+10s-10";
        List<Integer> expected = new ArrayList<>();
        expected.add(-10);
        expected.add(10);
        expected.add(10);
        expected.add(10);
        expected.add(-10);
        List<Integer> actualSplit = Ex_10.getListIntSplit(test);
        List<Integer> actualFind = Ex_10.getListIntFind(test);
        Assert.assertEquals(expected, actualFind);
        Assert.assertEquals(expected, actualSplit);
    }

    @Test
    public void Ex_11() throws IOException, NoSuchAlgorithmException {
        String test = "C:/Users/Pictures/QIP Shot/QIP Shot - Screen 008.png";
        String[] expected = {"C", "Users", "Pictures", "QIP Shot", "QIP Shot - Screen 008.png"};
        String[] actual = Ex_11.nameFolders(test);
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void Ex_13() throws IOException, NoSuchAlgorithmException, ClassNotFoundException {
        MyTestClass testClass1 = new MyTestClass("1");
        testClass1.setCash(10);
        MyTestClass testClass2 = Ex_13.myClone(testClass1);
        testClass2.setCash(15);
        Assert.assertNotEquals(testClass1.getCash(), testClass2.getCash());
    }

    @Test
    public void Ex_14() throws IOException, ClassNotFoundException {
        Point[] expected = {new Point(5, 5), new Point(2, 2), new Point(10, 15)};
        String file = "./Test/Part_9/Resources/Ex_14.txt";
        Ex_14.writeInArray(expected, file);
        Point[] actual = Ex_14.readInArray(file);
        Assert.assertArrayEquals(expected, actual);
    }
}
