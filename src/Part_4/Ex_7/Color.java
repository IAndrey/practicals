package Part_4.Ex_7;

public enum Color {
    BLACK, RED, BLUE, GREEN, CYAN, MAGENTA, YELLOW, WHITE;


    public static Color getRed() {
        return RED;
    }

    static Color getGreen() {
        return GREEN;
    }

    static Color getBlue() {
        return BLUE;
    }

    public Color getColor() {
        return this;
    }
}
