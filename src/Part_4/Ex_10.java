package Part_4;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Ex_10 {
    public static void main(String[] args) {
        int[] mass = new int[10];
        getNameMethods(mass).forEach(System.out::println);

    }

    public static List<String> getNameMethods(Object obj) {
        ArrayList<String> list = new ArrayList<>();
        Class<?> classes = obj.getClass();
        while (classes != null) {
            for (Method method : classes.getDeclaredMethods()) {
                list.add(method.getName() + " " + Arrays.toString(method.getParameterTypes()));
            }
            classes = classes.getSuperclass();
        }
        return list;
    }
}
