package Part_4.Ex_12;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

//Разница в производительности вызова метода и вызова этого метода с помощью рефлексии
public class Ex_12 {
    public static void main(String[] args) {
        Long x = timeDefaultMethod();
        Long y = timeReflectMethod();
        System.out.println(x + " общее кол-во мс при простом вызове метода");
        System.out.println(y + " общее кол-во мс при вызове метода с помощью рефлексии");
        System.out.println("Вызов обычного метода быстрее в " + y/x + " раз(а)");
    }

    public static Long timeDefaultMethod() {
        int counter = 0;
        Long startTime = System.currentTimeMillis();
        while (counter < 2000000) {
            counter += Tester.tester();
        }
        Long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    public static Long timeReflectMethod() {
        try {
            Method method = Tester.class.getDeclaredMethod("tester");
            int counter = 0;
            Long startTime = System.currentTimeMillis();
            while (counter < 2000000) {
                counter += (int) method.invoke(new Tester());
            }
            Long endTime = System.currentTimeMillis();
            return endTime - startTime;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }
}
