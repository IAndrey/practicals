package Part_4.Ex_8;

/**
 * Список 6 методов возвращаемых предст об объекте:
 * String toGenericString()
 * String getSimpleName()
 * String getCanonicalName()
 * String toString()
 * String getName()
 * String getTypeName()
 */
public class Ex_8 {
    public static void main(String[] args) {
        outExecuteClassMethods(new int[5]);
        outExecuteClassMethods(new MyClass<String>());
        outExecuteClassMethods(new MyClass.MyInnerClass());
    }

    public static void outExecuteClassMethods (Object object) {
        Class<?> classes = object.getClass();
        System.out.println(classes.toGenericString());
        System.out.println(classes.getSimpleName());
        System.out.println(classes.getCanonicalName());
        System.out.println(classes.toString());
        System.out.println(classes.getName());
        System.out.println(classes.getTypeName());
        System.out.println("//---------------------------------------//");
    }
}
