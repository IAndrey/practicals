package Part_4.Ex_4_5;

import Part_4.Ex_1_2_3.Point;

public class Line extends  Shape{
    Point to;
    public Line(Point from, Point to) {
        super(from);
        this.to = to;
    }

    @Override
    public Point getCenter() {
        return new Point(point.getX() + (to.getX() - point.getX())/2, point.getY() + (to.getY() - point.getY())/2);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Line(new Point(point.getX(), point.getY()), new Point(to.getX(), to.getY()));
    }
}
