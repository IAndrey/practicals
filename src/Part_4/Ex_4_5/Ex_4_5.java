package Part_4.Ex_4_5;

import Part_4.Ex_1_2_3.Point;
import org.w3c.dom.css.Rect;

public class Ex_4_5 {
    public static void main(String[] args) {
        Line line = new Line(new Point(10, 5), new Point(15, 10));
//        System.out.println(line.getCenter());

        Rectangle rectangle = new Rectangle(new Point(10, 10), 5, 5);
//        System.out.println(rectangle.getCenter());

        // упражнение 5.
        Circle circle = new Circle(new Point(10, 10), 5);
        Rectangle rectangle1 = new Rectangle(new Point(20, 20), 5, 5);
        System.out.println("rectangle center - " + rectangle1.getCenter());
        System.out.println("circle center - " + circle.getCenter());
        System.out.println();
        try {
            Circle clon = (Circle) circle.clone();
            clon.moveBy(5, 5);
            System.out.println("clon circle center - " + clon.getCenter());
            Rectangle clonRec = (Rectangle) rectangle1.clone();
            clonRec.moveBy(5,5);
            System.out.println("clon rectangle center - " + clonRec.getCenter());
            System.out.println();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println("circle center - " + circle.getCenter());
        System.out.println("rectangle center - " + rectangle1.getCenter());
    }
}
