package Part_4.Ex_4_5;

import Part_4.Ex_1_2_3.Point;

public class Circle extends Shape {
    private double radius;

    public Circle(Point center, double radius) {
        super(center);
        this.radius = radius;
    }

    @Override
    public Point getCenter() {
        return point;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Circle(new Point(point.getX(), point.getY()), radius);
    }
}
