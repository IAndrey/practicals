package Part_4.Ex_4_5;

import Part_4.Ex_1_2_3.Point;

public abstract class Shape implements Cloneable{
    protected Point point;

    public Shape(Point point) {
        this.point = point;
    }

    public void moveBy(double dx, double dy) {
        point.setX(point.getX() + dx);
        point.setY(point.getY() + dy);
    }

    public abstract Point getCenter();
}
