package Part_4.Ex_6;

import java.util.Objects;

public class DiscountedItem extends Item {
    private double discount;

    public DiscountedItem(String description, double price, double discount) {
        super(description, price);
        this.discount = discount;
    }

    //По заданию если Object o относится к суперклассу то сверяем без discount.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Item)) return false;
        if (o instanceof DiscountedItem) {
            DiscountedItem that = (DiscountedItem) o;
            return super.equals(o) && discount == that.discount;
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), discount);
    }
}
