package Part_4.Ex_6;

import java.util.Objects;

public class Item {
    String description;
    double price;

    public Item(String description, double price) {
        this.description = description;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Item)) return false;
        return description.equals(((Item) o).description) &&
                price == ((Item) o).price;
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, price);
    }
}
