package Part_4.Ex_6;

public class Ex_6 {
    public static void main(String[] args) {
        Item item = new Item("1", 6);
        Item item2 = new Item("1", 6);
        System.out.println(item.equals(item2));

        DiscountedItem discountedItem = new DiscountedItem("1", 6, 10);
        DiscountedItem discountedItem2 = new DiscountedItem("1", 6, 10);
        System.out.println(discountedItem.equals(item));
        System.out.println(discountedItem.equals(discountedItem2));
    }
}
