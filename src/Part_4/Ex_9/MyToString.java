package Part_4.Ex_9;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class MyToString {
    String x = "str x";
    double w = 10.0;
    private int y = 10;

    @Override
    public String toString() {
        Field [] fields = getClass().getDeclaredFields();
        StringBuilder stringBuilder = new StringBuilder("{\n");
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                stringBuilder.append(field.getType().getSimpleName()).append(" ").append(field.getName()).append("=[").append(field.get(this)).append("],\n");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        stringBuilder.append("}");

        return stringBuilder.toString();
    }
}
