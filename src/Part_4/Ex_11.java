package Part_4;

import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

//Пользуемся методом printLn объекта out в классе System с помощью рефлексии.
public class Ex_11 {
    public static void main(String[] args) {
        try {
            Method method = System.out.getClass().getMethod("println", String.class);
            method.invoke(System.out, "Hello World!");

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
