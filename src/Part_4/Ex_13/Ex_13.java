package Part_4.Ex_13;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.function.DoubleFunction;

/**
 * Безопаснее и удобнее использовать в методе функциональный интерфейс, т.к.
 * в нем указывается тип параметра, а также можно задать возвращаемое значение,
 * что позволяет отлавливать возможные ошибки еще на этапе компиляции. Однако
 * использование класса Method из пакета java.lang.reflect более гибкое. К примеру в данной задаче
 * в методе valuesTable(DoubleFunction<?> function, int min, int max, int step) мне пришлось использовать
 * конструкцию try {} catch () {}, либо жестко задавать в параметре метода возвращаемый тип DoubleFunction<?> function
 * чтобы использовать форматирование строки.
 */
public class Ex_13 {
    public static void main(String[] args) {
        try {
            Method method1 = Double.class.getDeclaredMethod("toHexString", double.class);
            Method method2 = Math.class.getDeclaredMethod("sqrt", double.class);
            valuesTable(method2, 10, 100, 3);
            valuesTable(method1, 10, 100, 3);
            valuesTable(Math::sqrt, 10, 100, 3);
            valuesTable(Double::toHexString, 10, 100, 3);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static void valuesTable(DoubleFunction<?> function, int min, int max, int step) {
        int counter = 0;
        for (int i = min; i <= max; i += step) {
            try {
                System.out.printf("%6.2f ", function.apply(i));
            } catch (Exception e) {
                System.out.printf("%10s ", function.apply(i));
            }
            counter++;
            if (counter % 10 == 0) System.out.println();
        }
        System.out.println();
        System.out.println("------------------------------------------------------------------------------");
    }

    public static void valuesTable(Method method, int min, int max, int step) throws InvocationTargetException, IllegalAccessException {
        Parameter[] parameters = method.getParameters();
        if (parameters.length != 1 || !String.valueOf(parameters[0].getType()).matches("[dD]ouble")) {
            throw new RuntimeException("В параметре метода должен быть тип Double или double");
        }
        int counter = 0;
        for (int i = min; i <= max; i += step) {
            System.out.printf(myFormat(method), method.invoke(method.getDeclaringClass(), i));
            counter++;
            if (counter % 10 == 0) System.out.println();
        }
        System.out.println();
        System.out.println("------------------------------------------------------------------------------");
    }

    private static String myFormat(Method method) {
        switch (method.getReturnType().getSimpleName()) {
            case "String": {
                return "%10s ";
            }
            case "Double": {
                return "%6.2f ";
            }
            case "double": {
                return "%6.2f ";
            }
        }
        return "";
    }
}
