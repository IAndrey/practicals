package Part_7;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

/*
Счетчик в отображении счетчиков можно обновить следующим образом:
    counts.merge(word, 1, Integer::sum);
Сделайте то же самое без метода merge(), воспользовавшись, во первых,
методом contains(); во-вторых, методом get() и проверкой пустых значений
(null); в-третьих, методом getOrDefault() и, в-четвертых, методом putIfAbsent().
 */
public class Ex_9 {
    public static void main(String[] args) {
        String[] strings = new String[5];
        strings[0] = "a";
        strings[1] = "b";
        strings[2] = "a";
        strings[3] = "b";
        strings[4] = "c";
        putIfAbsent(strings).forEach((k, v) -> System.out.println(k + " " + v));
    }

    public static Map<String, Integer> myMerge(String[] elements) {
        Map<String, Integer> map = new HashMap<>();
        for (String str : elements) {
            map.merge(str, 1, Integer::sum);
        }
        return map;
    }

    public static Map<String, Integer> myContains(String[] elements) {
        Map<String, Integer> map = new HashMap<>();
        for (String str : elements) {
            if (map.containsKey(str)) {
                map.put(str, map.get(str) + 1);
            } else {
                map.put(str, 1);
            }
        }
        return map;
    }

    public static Map<String, Integer> myGet(String[] elements) {
        Map<String, Integer> map = new HashMap<>();
        for (String str : elements) {
            if (map.get(str) == null) {
                map.put(str, 1);
            } else map.put(str, map.get(str) + 1);
        }
        return map;
    }

    public static Map<String, Integer> myGetOrDefault(String[] elements) {
        Map<String, Integer> map = new HashMap<>();
        for (String str : elements) {
            if (map.getOrDefault(str, -100) == -100) { // - если в мапе такой ключ не имеется,
                // то метод возвращает дефолтное значение.
                map.put(str, 1);
            } else map.put(str, map.get(str) + 1);
        }
        return map;
    }

    public static Map<String, Integer> putIfAbsent(String[] elements) {
        Map<String, Integer> map = new HashMap<>();
        for (String str : elements) {
            if (map.putIfAbsent(str, 1) == map.get(str)) { // map.putIfAbsent(str, 1) - если в мапе такой ключ имеется,
                // то метод возвращает его значение.
                map.put(str, map.get(str) + 1);
            }
        }
        return map;
    }
}
