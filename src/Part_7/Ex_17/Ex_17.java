package Part_7.Ex_17;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
Покажите, каким образом проверяемое представление может сообщить о конкретной ошибке,
ставшей причиной загрязнения "кучи".

Загрязнение кучи - это результат при котором в коллекции оказываются элементы других типов,
не соответствующие типам указанным в объявлении колекции: пример такого загрязнения показан ниже.
Причем ошибка при выполнении программы возникает при попытке извлеч этот элемент из "кучи", а не
в моменте его ввода в коллекцию (СТРОКА 28)
Чтобы избежать возникновение таких ошибок, следует воспользоваться проверяемым представлением
Collections.checkedList(). Такое представление контролирует все операции добавления элементов в
список и в случае добавления неверного типа возникнет исключение ClassCastException в строке 33.
 */
public class Ex_17 {

    public static void main(String[] args) {
        List ints = new ArrayList<Integer>();
        ints.add(1);
        List<String> strings = ints; //в результате получается что ссылка strings типа List<String> ссылается на
        // объект new ArrayList<Integer>(), что приводит к загрязнению кучи.
        strings.add("S");
//        strings.forEach(System.out::println);

        List strings2 = Collections.checkedList(new ArrayList<>(), Integer.class);
        strings2.add(1);
        List<String> strings3 = strings2;
        strings3.add("1");
        strings2.forEach(System.out::println);
    }
}
