package Part_7;

import java.util.*;

/*
Напишите программу для чтения предложения в списочный массив.
Затем перетасуйте в массиве все слова, кроме первого и последнего,
используя метод Collections.shuffle(), но не копируя слова в другую
коллекцию.
 */
public class Ex_11 {
    public static void main(String[] args) {
        List<String> words = getWords("Напишите программу для чтения предложения в списочный массив");
        words.forEach(System.out::println);
    }

    public static List<String> getWords(String string) {
        List<String> words = new ArrayList<>();
        String[] strings = string.split(" ");
        for (int i = 1; i < strings.length - 1; i++) {
            words.add(strings[i]);
        }
        Collections.shuffle(words);
        words.add(0, strings[0]);
        words.add(strings[strings.length-1]);
        return words;
    }
}
