package Part_7.Ex_8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
Напишите программу для чтения всех слов из файла и вывода строк,
в которых каждое слово встречается в нем. Воспользуйтесь для этой
цели преобразованием из символьных строк в множества.
 */
public class Ex_8 {

    public static List<String> readFiles(File file) throws IOException {
        List<String> list = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String str;
        while ((str = reader.readLine()) != null) {
            String[] elements = str.split(" ");
            for (String element: elements) list.add(element);
        }
        return list;
    }
}
