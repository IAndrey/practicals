package Part_7;

import java.util.*;

/*
Реализуйте метод public static void swap(List<?> list, int i, int j),
выполняющий перестановку элементов обычным образом, когда класс, опреде-
ляющий тип параметра list, реализует интерфейс RandomAccess, а иначе
сводящий к минимуму обход элементов на позициях, обозначаемых индексами
i и j.
*/
public class Ex_5 {
    public static void main(String[] args) {
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        swap(list, 0, 1);
        list.forEach(System.out::println);
    }

    public static void swap(List<?> list, int i, int j) {
        swapHelper(list, i, j); //пользуемся вспомогательным методом захвата типа.
    }

    private static <T> void swapHelper(List<T> list, int i, int j) {
        if (list instanceof RandomAccess) {
            Collections.swap(list, i, j);
        } else {
            T objJ = null;
            for (int k = list.size() - 1; k >= j; k--) {
                if (k == j) {
                    objJ = list.get(k);
                }
            }

            T objI = null;
            for (int k = 0; k <= i; k++) {
                if (k == i) {
                    objI = list.get(k);
                }
            }

            list.set(i, objJ);
            list.set(j, objI);
        }
    }
}
