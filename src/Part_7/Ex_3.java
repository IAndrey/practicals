package Part_7;

import java.util.HashSet;
import java.util.Set;

/*
Как вычислить объеденение, пересечение и разность двух множеств,
используя только методы из интерфейса Set, но не организуя циклы?
 */
public class Ex_3 {

    public static void main(String[] args) {
        Set<String> set1 = new HashSet<>();
        set1.add("1");
        set1.add("2");
        set1.add("3");
        set1.add("4");
        Set<String> set2 = new HashSet<>();
        set2.add("3");
        set2.add("4");
        set2.add("5");
        set2.add("6");
//        Set<String> set4 = union(set1, set2);
        Set<String> set4 = intersect(set1, set2);
//        Set<String> set4 = difference(set1, set2);
        set4.forEach(System.out::println);
    }

    public static <E> Set<E> union(Set<E> set1, Set<E> set2) {
        set1.addAll(set2);
        return set1;
    }

    public static <E> Set<E> intersect(Set<E> set1, Set<E> set2) {
        set1.retainAll(set2); //получаем разность двух множест, т.е. все элементы 1 множества , которых нет во
//        втором множестве удаляются.
        return set1;
    }

    public static <E> Set<E> difference(Set<E> set1, Set<E> set2) {
        set1.containsAll(set2);/* Пусть даны два множества A  и B , тогда их разностью называется
        множество A \ B , содержащее в себе элементы A , но не  B */
        return set1;
    }

}
