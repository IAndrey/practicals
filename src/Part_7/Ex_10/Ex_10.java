package Part_7.Ex_10;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.PriorityQueue;

/*
Реализуйте алгоритм Дейкстры для поиска кратчайших путей между городами,
связанными сетью автомобильных дорог. (Описание этого алгоритма можно найти в
популярной литературе по алгоритмам или в соответствующей статье Википедии).
Воспользуйтесь вспомогательным классом Neighbor для хранения названия соседнего города
и расстояния до него. Представьте полученный граф в виде преобразования названий
городов в множества соседних городов. Воспользуйтесь в данном алгоритме классом
PriorityQueue<Neighbor>.
 */
public class Ex_10 {
    private static ArrayList<String> results = new ArrayList<>();
    private static HashSet<Neighbor> set = new HashSet<>();
    private static City startCity;
    private static int totalLength;
    public static void main(String[] args) {
        City city1 = new City("Samara");
        City city2 = new City("Saratov");
        City city3 = new City("Togliatti");
        City city4 = new City("sizran");
        City city5 = new City("Penza");
        City city6 = new City("Moskva");

        city1.addNeighbor(new Neighbor(city2, 550));
        city1.addNeighbor(new Neighbor(city3, 100));

        city2.addNeighbor(new Neighbor(city1, 550));
        city2.addNeighbor(new Neighbor(city4, 300));
        city2.addNeighbor(new Neighbor(city6, 1200));

        city3.addNeighbor(new Neighbor(city1, 100));
        city3.addNeighbor(new Neighbor(city4, 50));

        city4.addNeighbor(new Neighbor(city3, 50));
        city4.addNeighbor(new Neighbor(city2, 300));
        city4.addNeighbor(new Neighbor(city5, 600));
        city4.addNeighbor(new Neighbor(city6, 900));

        city5.addNeighbor(new Neighbor(city4, 600));
        city5.addNeighbor(new Neighbor(city6, 150));

        city6.addNeighbor(new Neighbor(city5, 150));
        city6.addNeighbor(new Neighbor(city4, 900));
        city6.addNeighbor(new Neighbor(city2, 1200));

        searchLength(city1, city6);
        results.forEach(System.out::println);

    }

    public static void searchLength(City start, City finish) {
        startCity = start;
        searchLength(start, finish, 0); //Сделал вспомогательный перегруженный метод, принимающий расстояние, оно необходимо
//        для вычисления общего расстояния. Но для пользователя его задовать не нужно.
    }

    /*
    Создаем очередь, в которой находятся ближайшие города, для города start.
    Обходим рекурсивно каждый объект из очереди, если конечный город совпада-
    ет с искомым - записываем результат в ощий список этого класса. После
    выхода из очередного рекурсивного метода,удаляем его из множества set пройденых
    городов. Таким образом мы проверяем абсолютно все возможные комбинации.
    Множество set используется что бы не возвращаться в предыдущие города.
    Когда мы рекурсивно выходим из метода и удаляем город из множества,
    мы в него повторно не попадем, так как этот город удаляется из очереди города start.
     */
    private static void searchLength(City start, City finish, int length) {
        totalLength += length;
        PriorityQueue<Neighbor> list = start.getList();
        for (Neighbor neighbor : list) {
            if (neighbor.getNeighboringCity().equals(finish)) {
                StringBuilder distance = new StringBuilder();
                set.forEach((s) -> distance.append(s.getNeighboringCity().getName()+" - "));
                results.add(String.format("%s - %s%s, %s",startCity.getName(), distance.toString(), finish.getName(), (neighbor.getDistance() + totalLength)));
//                totalLength = 0;
//                set.remove(neighbor);
            }
            if (!neighbor.getNeighboringCity().equals(finish) && !neighbor.getNeighboringCity().equals(start) && !set.contains(neighbor) && !startCity.equals(neighbor.getNeighboringCity())) {
                set.add(neighbor);
                searchLength(neighbor.getNeighboringCity(), finish, neighbor.getDistance());
                set.remove(neighbor);
                totalLength -=neighbor.getDistance();
            }
        }
    }

    public static ArrayList<String> getResult() {
        return results;
    }
}
