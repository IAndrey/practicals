package Part_7.Ex_10;

import java.util.Objects;
import java.util.PriorityQueue;

public class City {
    private String name;
    private PriorityQueue<Neighbor> list = new PriorityQueue<>();

    public City(String name) {
        this.name = name;
    }

    public void addNeighbor(Neighbor neighbor) {
        list.add(neighbor);
    }

    public PriorityQueue<Neighbor> getList() {
        return list;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return Objects.equals(name, city.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
