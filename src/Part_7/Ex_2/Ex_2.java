package Part_7.Ex_2;

import java.util.ArrayList;
import java.util.Iterator;

/*
Сделайте все буквы прописными в символьных строках, содержащихся
в массиве. С этой целью воспользуйтесь сначала итератором, затем
перебором индексных значений в цикле и, наконец методом replaceAll().
 */
public class Ex_2 {
    public static void main(String[] args) {
        String[] strings = {"земля", "вода"};
        part3(strings);
    }

    public static String[] part1(String[] strings) {
        for (int i = 0; i < strings.length; i++) {
            strings[i] = upperStringPart1(strings[i].toCharArray());
            System.out.println(strings[i]);
        }
        return strings;
    }

    public static String upperStringPart1(char[] chars) {
        StringBuilder str = new StringBuilder();
        MyIterator iterator = new MyIterator(chars);
        while (iterator.hasNext()) {
            str.append(iterator.upperChar());
        }
        return str.toString();
    }

    public static String[] part2(String[] strings) {
        for (int i = 0; i < strings.length; i++) {
            strings[i] = upperStringPart2(strings[i].toCharArray());
            System.out.println(strings[i]);
        }
        return strings;
    }

    public static String upperStringPart2(char[] chars) {
        StringBuilder str = new StringBuilder();
        for (char c : chars) {
            str.append(("" + c).toUpperCase());
        }
        return str.toString();
    }

    public static String[] part3(String[] strings) {
        for (int i = 0; i < strings.length; i++) {
            strings[i] = strings[i].replaceAll(strings[i], strings[i].toUpperCase());
            System.out.println(strings[i]);
        }
        return strings;
    }
}
