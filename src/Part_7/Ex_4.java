package Part_7;

import java.lang.reflect.Array;
import java.util.*;

/*
Воспроизведите ситуацию, когда возникает исключение типа
ConcurrentModificationException. Что можно предпринять, чтобы избежать
этого?

Данная ошибка возникает в результате модифицирования коллекции, во время итерирования по ней.
Для того что бы избежать ее, нужно вызывать метод remove() у итератора.

        for (Integer x : list) {
            if (x == 2) list.remove(2);
        }

        или

        while (iterator.hasNext()) {
            int i = iterator.next();
            if (i == 2) iterator.remove();
        }

 */
public class Ex_4 {
    public static void main(String[] args) {
        List<Integer> list = init(1, 10);
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            int i = iterator.next();
            if (i == 2) iterator.remove();
        }
        list.forEach(System.out::println);
    }

    private static List<Integer> init(int start, int finish) {
        List<Integer> set = new ArrayList<>();
        for (int i = start; i <= finish; i++) {
            set.add(i);
        }
        return set;
    }
}
