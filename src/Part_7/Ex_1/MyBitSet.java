package Part_7.Ex_1;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.function.Predicate;

public class MyBitSet {
    public static void main(String[] args) {
        int n = 15;
        BitSet bit = getSet(getElements(n));
        Predicate<Integer> myFilter = createTester(n);
        BitSet filter = getSet(filterSet(getElements(n), myFilter));
        bit.xor(filter);
        System.out.println(bit);
    }

    public static BitSet getSet(ArrayList<Integer> list) {
        BitSet bitSet = new BitSet();
        list.forEach(bitSet::set);
        return bitSet;
    }
//  Задаю список от 2 до n.
    public static ArrayList<Integer> getElements(int n) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 2; i <= n; i++) {
            list.add(i);
        }
        return list;
    }
//  Получаю список елементов на удаление, что бы затем выполнить метод BitSet.xor, т.е. взять из списка
//   только те элементы которые отсутствуют в списке на удаление.
    public static ArrayList<Integer> filterSet(ArrayList<Integer> list, Predicate<Integer> filter) {
        ArrayList<Integer> removeObjects = new ArrayList<>();
        int count = 0;
        for (int s : list) {
            if (filter.test(s)) {
                while (searchElementDelited(list, s * (s + count), removeObjects)) {
                    count++;
                }
                count = 0;
            } else break;
        }
        return removeObjects;
    }

    private static boolean searchElementDelited(ArrayList<Integer> list, int element, ArrayList<Integer> result) {
        for (int x : list) {
            if (x == element) {
                result.add(element);
                return true;
            }
        }
        return false;
    }

    public static Predicate<Integer> createTester(int n) {
        Predicate<Integer> filter = (setElement) -> {
            if (setElement * setElement > n) return false;
            else return true;
        };
        return filter;
    }
}
