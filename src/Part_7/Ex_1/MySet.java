package Part_7.Ex_1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.function.Predicate;

/*
Реализуйте алгоритм под названием "Решето Эратосфена" для выявления всех простых
чисел меньше или равных n. Сначала введите все числа от 2 до n в множество. Затем
организуйте повторяющийся поиск наименьшего элемента s в этом множестве, постепенно
удаляя из него элементы s^2, s * (s + 1), s * (s + 2) и т.д. Этот поиск завершается
при условии, когда s^2 > n. Реализуйте данный алгоритм как для множества типа
HashSet<Integer>, так и для множества типа BitSet.
 */
public class MySet {

    public static void main(String[] args) {
        int n = 15;
        HashSet<Integer> set = getSet(n);
        Predicate<Integer> myFilter = createTester(n);
        filterSet(set, myFilter);
        set.forEach(System.out::println);

    }
    public static HashSet<Integer> getSet(int n) {
        HashSet<Integer> set = new HashSet<>();
        for (int i = 2; i <= n; i++) {
            set.add(i);
        }
        return set;
    }

    public static void filterSet(HashSet<Integer> set, Predicate<Integer> filter) {
        ArrayList<Integer> removeObjects = new ArrayList<>();
        int count = 0;
        for (int s : set) {
            if (filter.test(s)) {
                while (searchElementDelited(set, s * (s + count), removeObjects)) {
                    count++;
                }
                count = 0;
            } else break;
        }
        removeObjects.forEach((removeElement) -> set.removeIf((setElement) -> removeElement == setElement));
    }

    private static boolean searchElementDelited(HashSet<Integer> set, Integer element, ArrayList<Integer> result) {
        if (set.contains(element)) {
            result.add(element);
            return true;
        }
        return false;
    }

    public static Predicate<Integer> createTester(int n) {
        Predicate<Integer> filter = (setElement) -> {
            if (setElement * setElement > n) return false;
            else return true;
        };
        return filter;
    }
}
