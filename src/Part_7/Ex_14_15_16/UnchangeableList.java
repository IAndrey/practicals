package Part_7.Ex_14_15_16;

import java.util.*;
import java.util.function.IntFunction;

public class UnchangeableList<T extends Integer> extends ArrayList<Integer> {
    private HashSet<Integer> hash = new HashSet<>();

    @Override
    public Integer set(int index, Integer element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(Integer integer) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, Integer element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer remove(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException();
    }

    public Set<Integer> getCash() {
        return hash;
    }

    /*
    Ex 14
    Напишите метод для получения неизменяемого представления списка чисел от 0 до n,
    не сохраняя эти числа.

    Так же для получения неизменяемого списка, я могу воспользоваться методом
    Collections.unmodifiableCollection(). Но я реализовал свою неизменяемую кол-
    лекцию
    */
    public UnchangeableList<Integer> getUnchangeableList(int n) {
        UnchangeableList<Integer> list = new UnchangeableList<>();
        for (int i = 0; i < n; i++) {
            super.add(i);
        }
        return list;
    }

    /*
    Ex_15
    Обобщите предыдущее упражнение произвольным функциональным интерфейсом IntFunction.
    Имейте в виду, что в коненом итоге может получиться бесконечная коллекция, поэто-
    му некоторые методы (например, size() и toArray()) должны генерировать исклчение
    типа UnsupportedOperationException
     */
    public UnchangeableList<Integer> getUnchangeableList(int n, IntFunction<Integer> function) {
        UnchangeableList<Integer> list = new UnchangeableList<>();
        for (int i = 0; i < n; i++) {
            super.add(function.apply(i));
        }
        return list;
    }

    /*
    Ex 16
    Усовершенствуйте реализацию из предыдущего упражнения, организовав кеширование
    последних 100 значений вычисленых функцией.
     */

    public UnchangeableList<Integer> getUnchangeableListAndHash(int n, IntFunction<Integer> function) {
        UnchangeableList<Integer> list = new UnchangeableList<>();
        for (int i = 0; i < n; i++) {
            if (i >= n - 100) {
                hash.add(function.apply(i));
            }
            super.add(function.apply(i));
        }
        return list;
    }

}
