package Part_7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*
Используя метод Collections.shuffle(), напишите программу для чтения предложения,
перетасовки его слов и вывода результата. Устраните (до и после перетасовки)
написание начального слова с заглавной буквы и наличие точки в конце предложения.
Подсказка: не перетасовывайте при этом слова.
 */
public class Ex_12 {
    public static void main(String[] args) {
        List<String> words = getWords("Напишите программу для чтения предложения в списочный массив.");
        words.forEach(System.out::println);
    }

    public static List<String> getWords(String string) {
        String[] strings = string.split(" ");
        strings[0] = strings[0].toLowerCase();
        if (strings[strings.length-1].endsWith(".")){
            strings[strings.length-1] = strings[strings.length-1].substring(0, strings[strings.length-1].length()-1);
        }
        List<String> words = new ArrayList<>(Arrays.asList(strings));
        Collections.shuffle(words);

        return words;
    }
}
