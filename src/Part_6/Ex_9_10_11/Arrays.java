package Part_6.Ex_9_10_11;

import Part_6.Ex_7_8.Pair;

import java.util.ArrayList;

/*
Ex_9
Предоставьте в служебном классе Arrays следующий метод, возвращающий пару,
состоящую из первого и последнего элементов массива а, указав подходящий аргумент типа:
public static <E> Pair<E> firstLast (ArrayList<___> a)

Ex_10
Предоставьте в служебном классе Arrays обобщенные методы min() и max(), возвращающие
наименьший и наибольший элементы массива соответственно.

Ex_11
Продолжая предыдущее упражнение, предоставьте метод minMax(), возвращающий объект типа
Pair с наименьшим и наибольшим элементами массива.
 */
public class Arrays {
    public static <E extends Number> Pair<E> firstLast(ArrayList<E> a) {
        return new Pair<>(a.get(0), a.get(a.size() - 1));
    }

    public static <E extends Number> E max(ArrayList<E> a) {
        Number max = a.get(0);
        for (Number element : a) {
            if (element.doubleValue() >= max.doubleValue()) max = element.doubleValue();
        }
        return (E) max;
    }
    public static <E extends Number> E min(ArrayList<E> a) {
        Number min = a.get(0);
        for (Number element : a) {
            if (element.doubleValue() <= min.doubleValue()) min = element.doubleValue();
        }
        return (E) min;
    }
    public static <E extends Number> Pair<E> minMax(ArrayList<E> a) {
        Pair<E> pair = new Pair<>(min(a), max(a));
        return pair;
    }
}
