package Part_6;

import java.util.concurrent.Callable;

/*
Усовершенствуйте метод

        public static <V, T extends Throwable> V doWork(Callable<V> c, T ex) throws T {
        try {
            return c.call();
        } catch (Throwable relEx) {
            ex.initCause(relEx);
            throw ex;

        }
    }

представленный в разделе 6.6.7, таким образом, чтобы передать ему объект исключения,
который вряд ли будет вообще использован. Вместо этого данный метод должен принимать ссылку
на класс исключения.
 */
public class Ex_22 {
    public static void main(String[] args) {
        Class<RuntimeException> rClass = RuntimeException.class;
        doWork(() -> 10/0, rClass);
    }

    public static <V, T extends Throwable> V doWork(Callable<V> c, Class<T> ex) throws T {
        try {
            return c.call();
        } catch (Throwable relEx) {
            T exception = null;
            try {
                exception = (T) ex.newInstance(); // используя ссылку на класс искл, создаем ее объект;
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
            relEx.initCause(exception);
            throw (T) relEx;
        }
    }
}
