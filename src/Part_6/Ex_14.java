package Part_6;

import java.util.ArrayList;

/*
Реализуйте усовершенствованный вариант метода closeAll(), представленного в разделе 6.3. Закройте
все элементы даже в том случае, если некоторые из них генерируют исключение. В таком случае сгенери-
руйте исключение в последствии. Если исключение генерируется в результате двух или больше вызовов
данного метода, свяжите их в цепочку.

Раздел 6.3
    public static <T extends AutoCloseable> void  closeAll(ArrayList<T> elems) throws Exception {
        for (T elem : elems) elem.close();
    }
 */
public class Ex_14 {
    public static <T extends AutoCloseable> void closeAll(ArrayList<T> elems) {
        for (T elem : elems) {
            try {
                elem.close();
            } catch (Exception e) {
                try {
                    elem.close();
                } catch (Exception w) {
                    elem = null;
                    w.printStackTrace();
                }
            }

        }
    }
}
