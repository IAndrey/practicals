package Part_6.Ex_1and2;

import java.util.ArrayList;
/*
Реализуйте обобщенный класс Stack<E>, управляющий списочным массивом, состощий из элементов типа Е.
Предоставьте методы pop(), push(E element), isEmpty().
 */
public class Ex_1_Stack<E> implements IStack<E> {
    private ArrayList<E> elements = new ArrayList<>(); // списочный массив

    public void push(E element) {
        elements.add(element);
    }

    public E pop() {
        E element = elements.get(elements.size() - 1);
        elements.remove(elements.size()-1);
        return element;
    }

    public boolean isEmpty() {
        return elements.isEmpty();
    }
}
