package Part_6.Ex_1and2;
//Для удобства тестирования
public interface IStack<E> {
    void push(E element);

    E pop();

    boolean isEmpty();
}
