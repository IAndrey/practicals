package Part_6.Ex_1and2;

import java.util.Arrays;

public class Ex_2_part_2_Stack<E> implements IStack<E> {

    private Object[] elements = new Object[1];
    private int size = 0;

    public void push(E element) {
        if (size > 0 && elements[elements.length - 1] != null) {
            elements = Arrays.copyOf(elements, size + 5);
        }
        elements[size] = element;
        size++;
    }

    public E pop() {
        @SuppressWarnings("unchecked") //Для того что бы подавить предупреждение при низходящем преобразовании.
        E element = (E) elements[size - 1];
        elements = Arrays.copyOf(elements, size - 1);
        size--;
        return element;
    }

    public boolean isEmpty() {
        return size == 0;
    }
}
