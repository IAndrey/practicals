package Part_6.Ex_1and2;

import java.util.Arrays;
/*
Еще раз реализуйте обобщенный класс Stack<E>. Если требуется нарастите массив
в методе push(). Предоставьте два решения этой задачи: Одно с массивом типа E[],
другое - с массивом типа Object[]. Оба решения должны компилироваться без
всяких предупреждений.
 */
public class Ex_2_Stack<E> implements IStack<E> {
    @SuppressWarnings("unchecked") //Для того что бы подавить предупреждение при низходящем преобразовании.
    private E[] elements = (E[]) new Object[1];
    private int size = 0;

    public void push(E element) {
        if (size > 0 && elements[elements.length - 1] != null) {
            elements = Arrays.copyOf(elements, size + 5);
        }
        elements[size] = element;
        size++;
    }

    public E pop() {
        E element = elements[size - 1];
        elements = Arrays.copyOf(elements, size - 1);
        size--;
        return element;
    }

    public boolean isEmpty() {
        return size == 0;
    }
}
