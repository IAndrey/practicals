package Part_6.Ex_21;

import java.lang.reflect.Array;

/*
Используя аннатацию  @SafeVarargs, напишите метод, позволяющий строить
массивы обобщенных типов, как в следующем примере:
    List<String> [] result = Arrays.<List<String>>construct(10);
Устанавливает результат в списке типа List<String>[] длинной 10
 */

public class Arrays {
    @SafeVarargs
    public static <T> T[] construct (int length, T... types) {
        T[] elements = (T[]) Array.newInstance(types[0].getClass(), length);
        return elements;
    }
}
