package Part_6.Ex_12_13;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/*
Ex_12
Реализуйте следующий метод, сохраняющий наименьший и наибольший элементы из
массива elements в списке result:
    public static <T> void minMax(List<T> elements, Comparator<? super T> comp, List<? super T> result)

Ex_13
С учетом метода из предыдущего упражнения рассмотрите следующий метод:

    public static <T> void maxMin(List<T> elements, Comparator<? super T> comp, List<? super T> result) {
        minMax(elements, comp, result);
        Lists.swapHelper (result, 0 ,1);
    }
Почему этот метод нельзя скомпилировать без захвата подстановки? Подсказка:
попробуйте предоставить явный тип Lists.<___>swapHelper(result, 0 ,1).

Этот метод нельзя скомпилировать без захвата подстановки, т.к. в списке result,
нет явного типа, в нем могут быть типы T и его суперклассы. То есть я не могу указать тип
       ? firstElement = list.get(firstIndex);
       ? latElement = list.get(lastIndex);
Для этого используется захват подстановки.

 */
public class Ex_12_13 {

    public static <T> void minMax(List<T> elements, Comparator<? super T> comp, List<? super T> result) {
        T min = Collections.min(elements, comp);
        result.add(min);
        T max = Collections.max(elements, comp);
        result.add(max);
    }

    public static <T> void maxMin(List<T> elements, Comparator<? super T> comp, List<? super T> result) {
        minMax(elements, comp, result);
        Lists.swapHelper(result, 0, 1);
    }
}
