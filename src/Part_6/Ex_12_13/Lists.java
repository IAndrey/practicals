package Part_6.Ex_12_13;

import java.util.ArrayList;
import java.util.List;

public class Lists {


    public static <T> void swapHelper(List<T> list, int firstIndex, int lastIndex) {
        T firstElement = list.get(firstIndex);
        T latElement = list.get(lastIndex);

        list.add(lastIndex, firstElement);
        list.remove(lastIndex + 1);

        list.add(firstIndex, latElement);
        list.remove(firstIndex + 1);
    }

    public static void swap(List<?> list, int firstIndex, int lastIndex) {
        swapHelper(list, firstIndex, lastIndex);
    }
}
