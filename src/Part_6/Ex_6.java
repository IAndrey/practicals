package Part_6;

import java.math.BigDecimal;
import java.util.ArrayList;

/*
Реализуйте обобщенный метод, присоединяющий все элементы из
одного списочного массива к другому. Воспользуйтесь метасимволом
подстановки для обозначения одного из аргументов типа. Предоставьте два
равнозначных решения: одно с подстановочным типом ? extends E,
другое - с подстановочным типом ? super E
 */
public class Ex_6 {
    public static void main(String[] args) {
        ArrayList<Number> list1 = new ArrayList<>();
        ArrayList<Double> list2 = new ArrayList<>();
        ArrayList<BigDecimal> list3 = new ArrayList<>();
        list1.add(1);
        list2.add(1.2);
        list3.add(new BigDecimal("11111111111111111111111111111111111111111111111111111"));
        joining(list1, list2);
        joining(list1, list3);

        final ArrayList<Integer> list4 = new ArrayList<>();
        ArrayList<Number> list5 = new ArrayList<>();
        ArrayList<Object> list6 = new ArrayList<>();
        list4.add(1);
        list5.add(2.2);
        list6.add("object");
        joining2(list4, list5);
        joining2(list4, list6);

//        Number number = 2.54;
//        Object ob = "s";
//        list4.add((Integer) number );
//        list4.add((Integer) ob);

        for (Object obj : list4) {
            System.out.println(obj);
        }


    }

    /*
    часть 1. Означает что я могу в результирующем списке указать любой элемент, так как ограничение задается типом
    E, который я сам задаю. Но добавить в этот список я могу только те элементы, которые являются его наследником.
     */
    public static <E> void joining(ArrayList<E> rezult, ArrayList<? extends E> source) {
        for (E element : source) {
            rezult.add(element);
        }
    }

    /*
    часть 2. Означает что я могу в результирующем списке указать любой элемент, так как ограничение задается типом
    E, который я сам задаю. Но добавить в этот список я могу только те элементы, которые являются его суперклассом.
     */
    public static <E> void joining2(ArrayList<E> rezult, ArrayList<? super E> source) {
        for (Object element: source) {
            rezult.add( (E) element);
        }
//        каким образом выполняется приведение типа, мой лист rezult имеет внутри тип Integer, я могу в этот метод
//        закинуть любой ArrayList имеющий тип суперкласса для Integer. Если это object то он может содержать к
//        примеру строку и в стр 61 при приведении типов должна прогрузиться ошибка. Но этого почему то не происходит.
    }
}
