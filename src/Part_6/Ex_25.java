package Part_6;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;

/*
Наипишите метод public static String genericDeclaration (Method m),
возвращающий объявление метода m(), перечисляющего  параметры типа с их ограничениями
и типами параметров метода, включая их аргументы типа, если это обобщенные типы.
 */
public class Ex_25 {
    public static void main(String[] args) throws NoSuchMethodException {
        Method method = Ex_25.class.getMethod("testMethod", ArrayList.class, int.class);
        genericDeclaration(method);
    }
    public static String genericDeclaration (Method m) {
        String methodName = m.getName();
        String parameters = getParametersString(m);
        String returnType = getReturnsType(m);
        System.out.printf("Method name - %s\nReturn type - %s\nParameters type - %s", methodName, returnType, parameters);
        return null;
    }
    private static String getParametersString(Method m) {
        Class[] parameters = m.getParameterTypes();
        Type[] types = m.getGenericParameterTypes();
        return Arrays.toString(types);
    }

    private static String getReturnsType(Method m) {
        return m.getGenericReturnType().getTypeName() + " " + m.getReturnType();
    }

    public static <T extends Number> T testMethod (ArrayList<Integer> list, int x) {
        return null;
    }
}
