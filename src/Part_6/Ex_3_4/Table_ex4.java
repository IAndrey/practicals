package Part_6.Ex_3_4;

import java.util.ArrayList;
import java.util.Objects;
/*
Сделайте вложенным класс Entry, из предыдущего упражнения. Должен ли этот класс быть обобщенным?

Этот клас не обязан быть обобщенным, т.к. он вложен в обобщенный класс Table_ex4 и может использоват
его обобщенные типы.
 */
public class Table_ex4<K, V> implements ITable<K, V> {
    private ArrayList<Entry> list = new ArrayList<>();

    public void add(Entry element) {
        list.add(element);
    }

    public V getValue(K key) {
        for (Entry element : list) {
            if (element.getKey().equals(key)) {
                return (V) element.getValue();
            }
        }
        throw new RuntimeException("Ключ не найден");
    }

    public boolean setValue(K key, V value) {
        for (Entry element : list) {
            if (element.getKey().equals(key)) {
                element.setValue(value);
                return true;
            }
        }
        return false;
    }

    public boolean removeKey(K key) {
        for (Entry element : list) {
            if (element.getKey().equals(key)) {
                list.remove(element);
                return true;
            }
        }
        return false;
    }

    public int size() {
        return list.size();
    }

    public class Entry {
        private K k;
        private V v;

        public Entry(K k, V v) {
            this.k = k;
            this.v = v;
        }

        public K getKey() {
            return k;
        }

        public V getValue() {
            return v;
        }

        public void setValue(V v) {
            this.v = v;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Entry entry = (Entry) o;
            return Objects.equals(k, entry.k) &&
                    Objects.equals(v, entry.v);
        }

        @Override
        public int hashCode() {
            return Objects.hash(k, v);
        }
    }
}
