package Part_6.Ex_3_4;

public interface ITable <K, V> {
    V getValue(K key) ;

    boolean setValue(K key, V value) ;

    boolean removeKey(K key) ;

    int size();
}
