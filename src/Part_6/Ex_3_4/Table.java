package Part_6.Ex_3_4;

import java.util.ArrayList;
/*
Реализуйте обобщенный класс Table<K, V>, управляющий списочным массивом, состоящим из
элементов типа Entry<K, V>. Предоставьте методы для получения значения, связанного с ключем,
установки значения по заданному ключу и удаления ключа.
 */
public class Table<K, V> implements ITable<K, V> {
    private ArrayList<Entry<K, V>> list = new ArrayList<>();

    public void add(Entry<K, V> element) {
        list.add(element);
    }

    public V getValue(K key) {
        for (Entry element : list) {
            if (element.getKey().equals(key)) {
                return (V) element.getValue();
            }
        }
        throw new RuntimeException("Ключ не найден");
    }

    public boolean setValue(K key, V value) {
        for (Entry element : list) {
            if (element.getKey().equals(key)) {
                element.setValue(value);
                return true;
            }
        }
        return false;
    }

    public boolean removeKey(K key) {
        for (Entry element : list) {
            if (element.getKey().equals(key)) {
                list.remove(element);
                return true;
            }
        }
        return false;
    }

    public int size() {
        return list.size();
    }
}
