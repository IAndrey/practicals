package Part_6;

import java.util.List;
/*
К чму приведет стирание типов в приведенных ниже методах из класса
Collection.

Cтирание типов приведет эти методы к такому виду:
    public static Comparable void sort(List list) {

    }

    public static Object T max(List list) {
        return null;
    }
 */
public class Ex_16 {
    public static <T extends Comparable<? super T>> void sort(List<T> list) {

    }

    public static <T extends Object & Comparable<? super T>> T max(List<T> list) {
        return null;
    }
}
