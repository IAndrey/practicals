package Part_6.Ex_7_8;
/*
Ex_7
Реализуйте класс обобщенный класс Pair<E> позволяющий сохранять пару элементов типа E.
Предоставьте методы доступа для получения первого и второго элементов.
Ex_8
Видоизмените класс введя методы max() и min() для получения наибольшего и наименьшего из
двух элементов. Наложите соответствующее ограничение на обобщенный тип E.
 */
public class Pair<E extends Number> {
    private E element1;
    private E element2;

    public Pair(E e1, E e2) {
        this.element1 = e1;
        this.element2 = e2;
    }

    public E getElement1() {
        return element1;
    }

    public E getElement2() {
        return element2;
    }

    public E max() {
        return (element1.doubleValue() > element2.doubleValue())? element1 : element2;
    }
    public E min() {
        return (element1.doubleValue() < element2.doubleValue())? element1 : element2;
    }
}
