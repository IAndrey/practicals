package Part_6;

import java.lang.reflect.Array;
import java.util.function.IntFunction;

/*
Рассмотрите следующий метод, представленный в разделе 6.6.3

        public static <T>ArrayList<T> repeat (int n, T obj) {
        ArrayList<T> elements = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            elements.add(obj);
        }
        return elements;
        }

Этот метод без особых хлопот  составляет списочный массив типа ArrayList<T>
из элементов обобщенного типа T. Можно ли получить массив типа []T из
этого списочного массива, не пользуясь объектом типа Class или ссылкой
на конструктор? Если нельзя, то почему?

Нет, создание объектов (new T() или new T[] ) обобщенного типа невозможо, так как цитата:
"они не воплощают того, что намеревается сделать программист, когда стирается
обобщенный тип Т.".
Ниже приведены примеры, как создать массивы обобщенных типов.

Примитивные типы так же не могут быть обобщенными, т.к. при стирании типов,
примитивный тип не может быть типом Object.
 */
public class Ex_19 {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
         String[] ss = repeat(10, "hi");
         Integer[] ss2= repeat2(5, 10, Integer[]::new);
    }

    public static <T> T[] repeat (int n, T obj) throws IllegalAccessException, InstantiationException {
        T[] elements = (T[]) Array.newInstance(obj.getClass(), n);  //пример использования рефлексии для создания объекта.
        return elements;
    }

    public static <T> T[] repeat2 (int n, T obj, IntFunction<T[]> constructor)  {
        T[] elements = constructor.apply(n); // пример использования функционального интерфейса для создания объекта.
        return elements;
    }

}
