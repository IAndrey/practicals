package Part_6;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/*
Реализуйте метод map(), получающий списочный массив и объект типа
Function<T, R> и возвращающий списочный массив, состоящий из результатов
применения функции к заданным элементом этого массива.
 */
public class Ex_15 {
    public static <T, R> List<R> map(List<T> list, Function<T, R> function) {
        List<R> result = new ArrayList<>();
        for (T element : list) {
            result.add(function.apply(element));
        }
        return result;
    }
}
