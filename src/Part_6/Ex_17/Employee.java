package Part_6.Ex_17;
/*
Определите класс Employee, реализующий интерфейс Comparable<Employee>
Используя утилиту javap, продемонстрируйте, что мостовой метод был
синтезирован. Каково его назначение?

Для того чтобы это продемонстрировать, нужно добавить в парамеры среды windows
java. Затем запустить командную строку, перейти в месторасположение скомпилированного
класса и выполнить команду javap Employee.class
В результате вывод в командную строку будет следующим :

Compiled from "Employee.java"
public class Part_6.Ex_17.Employee implements java.lang.Comparable<Part_6.Ex_17.Employee> {
  public Part_6.Ex_17.Employee();
  public int compareTo(Part_6.Ex_17.Employee);
  public int compareTo(java.lang.Object);
}

Видно что был синтезироан мостовый метод public int compareTo(java.lang.Object);
Он необходим для того что бы вызвать метод compareTo из класса Employee, если ссылке
класса родителя присвоить объект класса наследника :
Comparable<Employee> comp = new Employee();
так как во время компиляции, при стирании типов, метод интерфейса Comparable
имеет вид compareTo(Object o) у которого тип параметра не совпадает с методом compareTo(Employee o)
класса Employee. И поэтому при вызове метода, будет вызван метод класса родителя а не наследника.
Что бы этого не произошло компилятор автоматически синтезирует метод:
    int compareTo(java.lang.Object obj) {
         add ((Employee) obj)
     }
благодаря этому вызывается нужный нам переопределенный в классе наследнике метод.
 */
public class Employee implements Comparable<Employee>{
    @Override
    public int compareTo(Employee o) {
        return 0;
    }
}
