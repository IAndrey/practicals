package Part_6.Ex_23;

import java.io.IOException;
import java.util.concurrent.Callable;

/*
Во врезке "Внимание!" из раздела 6.6.7 упоминается вспомогательный метод
throwAs (),  применямый для "приведения" типа исключения ex к типу
RuntimeException и его генерирования. Почему для этой цели нельзя воспользоваться обычным
приведением типов, т.е. throw (RuntimeException) ex?

Обычным приведением типов нельзя воспользоваться, т.к. если будет поймана ошибка не
производная от RuntimeException, то при приведении типов возникнет исключение.

В случае же использования метода throwAs, обобщенный тип Т стирается в Throwable. И в строке
вызывающей его компилятор подставит приведение типа.
 */
public class Exceptions {
//    ВАЖНО если выполнить метод main,
//    ошибка будет в строке 26, а не в 32,
//    т.к. Integer x = test("s"); компилируется в
//         Integer x = (Integer) test("s");
//    А сам тип Т стирается в Object!!!.

//    public static void main(String[] args) {
//        Integer x = test("s");
//    }
//
//    public static <T> T test(String str) {
//        T t = null;
//        try {
//            t = (T) str;
//        } catch (ClassCastException e) {
//            e.printStackTrace();
//        }
//
//        return t;
//    }


    public static <T extends Throwable> void throwAs(Throwable e) throws T {
        throw (T) e;
    }

    public static <V> V doWork(Callable<V> c) throws IOException {
        try {
            return c.call();
        } catch (Throwable ex) {
            Exceptions.<IOException>throwAs(ex);
            return null;
        }
    }
}
