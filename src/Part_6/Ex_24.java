package Part_6;
/*
Какие методы можно вызвать для переменной типа Class<?>, не прибегая
к приведению типов?

для переменной типа Class<?> можно вызвать любой метод, относящийся к классу Class.
Т.к. <?> означает, что ссылка класса Class<?> может ссылаться на любой объект.
 */
public class Ex_24 {
    public static void main(String[] args) {
        String ints = "sss";
        Class<?> myclass = Integer.class;
    }

    public static void test(Class iclass) {
        System.out.println(iclass.getSimpleName());
    }
}
