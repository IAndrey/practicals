package Part_9;

import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

/*
Если кодировщик типа Charset лишь частично охватывает Юникод и не в
состоянии закодировать символ, то он замняется символом по умолчанию,
как правило, хотя и не всегда, знаком ?. Найдите замены всех доступных
наборов символов, поддерживающих кодировку. Воспользуйтесь методом
newEncoder(), чтобы получить кодировщик, и вызовите его метод replacement(),
чтобы получить соответствующую замену. Для каждого однозначного результата
выведите отчет о канонических именах, используемых для замены наборов символов.
 */
public class Ex_5 {
    public static void main(String[] args) throws CharacterCodingException {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("UTF_8", getReplaceSimbol("UTF-8"));
        map.put("UTF_16", getReplaceSimbol("UTF-16"));
        map.put("UTF_32", getReplaceSimbol("UTF-32"));
        map.put("ascii", getReplaceSimbol("ascii"));
        map.put("iso-8859-1", getReplaceSimbol("iso-8859-1"));
        map.put("iso-8859-5", getReplaceSimbol("iso-8859-5"));
        map.put("Windows-1253", getReplaceSimbol("Windows-1253"));
        map.forEach((K, V) -> System.out.println(K + " " + V));
    }

    public static String getReplaceSimbol(String charset) {
        Charset chars = Charset.forName(charset);
        CharsetEncoder encoder = chars.newEncoder();
        return new String(encoder.replacement());
    }
}
