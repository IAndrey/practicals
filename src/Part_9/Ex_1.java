package Part_9;

import java.io.*;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileAttribute;
import java.util.Scanner;
import java.util.stream.Stream;

/*
Ex_1
Напишите служебный метод для копирования всего содержимого из потока
ввода InputStream в поток вывода OutputStream, не пользуясь временными
файлами. Предоставьте другое решение без цикла, используя методы из класса
Files и временный файл.
 */
public class Ex_1 {

    public static void main(String[] args) throws IOException {
        String str = "testStr";
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        myCopy2(new BufferedInputStream(new ByteArrayInputStream(str.getBytes())), out);
        System.out.println(out.toString());
    }

    //  1. Без Files
    public static void myCopy(InputStream in, OutputStream out) throws IOException {
        byte[] bytes = in.readAllBytes();
        in.close();
        out.write(bytes);
        out.close();
    }

    //  2. C Files
    public static void myCopy2(InputStream in, OutputStream out) throws IOException {
        File tempFile = File.createTempFile("text", ".txt", new File("./"));
        Files.copy(in, tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        in.close();
        Files.copy(tempFile.toPath(), out);
        out.close();
        tempFile.delete();
    }
}
