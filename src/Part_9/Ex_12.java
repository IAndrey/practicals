package Part_9;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Придумайте реальный пример применения ссылок на группы в методе Mather.replaceAll()
и реализуйте его на практике.
 */
public class Ex_12 {
    public static void main(String[] args) {
        Matcher math = Pattern.compile("\\d+").matcher("5555aaaa5555");
//        System.out.println(math.group(1));
        System.out.println(math.replaceAll(MatchResult::group)); // в данном случае
        // если прокинуть ссылку на группу в метод replaceAll, строка в итоге не изменется,
        // так как найденные совпадения будут заменяться тем же совпадением.
    }
}
