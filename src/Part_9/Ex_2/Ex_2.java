package Part_9.Ex_2;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Напишите программу для чтения тестового файла и создания файла с таким же именем,
но с расширением .toc. Этот файл должен содержать список всех слов из входного файла,
а так же список номеров строк, в которых встречается каждое слово. Имейте в виду,
что содержимое входного файла представлено в кодеровке UTF-8.
 */
public class Ex_2 {
    public static void main(String[] args) throws IOException {
        createToc("./src/Part_9/Ex_2/test.txt");
    }

    public static void createToc(String path) throws IOException {
        if (!path.endsWith(".txt")) throw new FileNotFoundException("Только формат .txt");
        Path path_ = Paths.get(path);
        Map<String, String> map = getMap(Arrays.stream(Files.readString(path_).split("\\PL+")), path_);
        File f = new File(path_.getParent() + "/" + path_.getFileName().toString()
                .replaceAll("\\..+", " ") + ".toc"); // название файла беру используя методы класса Path
        f.createNewFile();
        PrintWriter writer = new PrintWriter(f); // для записи мапы в файл.
        for (Map.Entry<String, String> pair : map.entrySet()) {
            writer.println(pair.getKey() + " " + pair.getValue());
        }
        writer.close();
    }

    public static Map<String, String> getMap (Stream<String> stream, Path path_) {
        return stream.distinct().collect(Collectors.toMap(
                (key) -> key,
                (key) -> {
                    try {
                        return searchString(key, Files.readAllLines(path_));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                },
                (key1, key2) -> key1,
                LinkedHashMap::new));
    }

    private static String searchString(String str, List<String> lines) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < lines.size(); i++) {
            if (lines.get(i).contains(str)) {
                stringBuilder.append((i + 1) + ", ");
            }
        }
        return stringBuilder.substring(0, stringBuilder.length() - 2);
    }
}
