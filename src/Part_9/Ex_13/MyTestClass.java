package Part_9.Ex_13;

import java.io.Serializable;

public class MyTestClass implements Serializable {
    private String name;
    private int cash = 10;

    public MyTestClass(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public int getCash() {
        return cash;
    }
}
