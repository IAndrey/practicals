package Part_9.Ex_13;

import java.io.*;

/*
Реализуйте метод для получения клона любого объекта сереализуемого
сначала в массив байтов и затем десериализируемого оттуда
 */
public class Ex_13 {


    public static void main(String[] args) throws IOException, ClassNotFoundException {
        MyTestClass human1 = new MyTestClass("Вася");
        MyTestClass human2 = myClone(human1);
        human2.setCash(15);
        System.out.println(human1.getCash());
        System.out.println(human2.getCash());
    }

    public static <T> T myClone(T originalObject) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(byteArray);
        out.writeObject(originalObject);
        ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(byteArray.toByteArray()));
        T clone = (T) in.readObject();
        return clone;
    }
}
