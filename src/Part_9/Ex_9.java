package Part_9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/*
Используя класс URLConnection, введите данные с веб-страницы, защищенной паролем
с элементарной аутентификацией. Соедините вместе имя пользователя, двоеточие,
пароль и определите кодировку Base64 полученного результата следующим обра-
зом:
String input = username + ":" + password;
String encoding = Base64.getEncoder().encodeToString(input.getBytes(StandardCharsets.UTF_8));
Установите в HTTP-заголовке Authorization значение "Basic " + encoding, а затем введите содер-
жимое страницы и выведите его на экран.

логин andrey19931
пароль andrey199311
Base64 — стандарт кодирования двоичных данных при помощи только 64 символов ASCII
 */
public class Ex_9 {
    public static void main(String[] args) throws IOException {
        System.out.println(method());
    }

    public static String method() throws IOException {
        String res = "";
        URL url = new URL("https://ru.wikipedia.org/wiki/Заглавная_страница");
        String input = "andrey19931" + ":" + "andrey199311";
        String encoding = Base64.getEncoder().encodeToString(input.getBytes(StandardCharsets.UTF_8));

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestProperty("Authorization", "Basic " + encoding);
        InputStream content = connection.getInputStream();
        BufferedReader in = new BufferedReader(new InputStreamReader(content));
        String line;
        while ((line = in.readLine()) != null) {
            res += line + "\n";
        }
        return res;
    }
}
