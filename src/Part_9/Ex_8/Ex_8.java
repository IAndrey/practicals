package Part_9.Ex_8;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;

/*
Напишите служебный метод для создания архивного ZIP - файла, содержащего
все файлы из выбранного каталога и его подкаталогов.
 */
public class Ex_8 {
    public static void main(String[] args) throws IOException, URISyntaxException {
        myWalk("src/Part_9/Ex_8/TestFiles", "src/Part_9/Ex_8/Result/result.zip");
    }

    private static void myWalk(String root, String zipPath) throws IOException, URISyntaxException {
        Path rootPath = Paths.get(root);
        Path zippath = Paths.get(zipPath);
        Map<String, String> env = new HashMap<>();
        env.put("create", "true");
        URI zip = new URI("jar", zippath.toUri().toString(), null);
        try (FileSystem zipf = FileSystems.newFileSystem(zip, env)) {
            Files.walk(rootPath).forEach(f -> {
                try {
                    Files.copy(f, zipf.getPath("/" + replace(f.toString(), rootPath.getFileName().toString())), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }
    //Чтобы в архиве дерево папок шло от нашей рутовой папки, удаляем все что до нее.
    private static String replace(String str, String root) {
        return str.substring(str.indexOf(root));
    }
}
