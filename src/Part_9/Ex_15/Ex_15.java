package Part_9.Ex_15;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

/*
Продолжите предыдущее упражнение, но измените представление данных
типа Point таким образом, чтобы хранить координаты точки в массиве.
Что произойдет при попытке прочитать в новой версии файл, сформированный
в предыдущей версии? И что произойдет, если исправить идентификатор
serialVersionUID? Что бы вы сделали, если бы ваша жизнь зависела от
создания новой версии, совместимой с прежней?

serialVersionUID - идентификатор, который при сериализации направляется в поток вывода, этот идентификатор может
определить разработчик класса следующим образом:
private static final Long serialVersionUID = 1l;
Если идентификатор не определен, то он генерируется автоматически получая хеш код из полей класса.(поэтому если
изменить поля класса, то изменится и serialVersionUID)

При попытке прочитать в новой версии файл, сформированный
в предыдущей версии, произойдет ошибка InvalidClassException, с описанием:
local class incompatible:
stream classdesc serialVersionUID = 6660640297243977837, local class serialVersionUID = 6212089576125502886

Если исправить идентификатор serialVersionUID, что бы в новой версии класс имел такой же идентификатор как в
14 задании, то будет выбрасываться ошибка ClassCastException, так как новый класс Part_9.Ex_15.Point, содержит
массив вместо int x и int y и при воссоздании int x и int y, возникает ClassCastException.

 */
public class Ex_15 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
//        Point[] points = {new Point(5, 5), new Point(2, 2), new Point(10, 15)};
//        writeInArray(points, "./src/Part_9/Ex_15/resultSerialization.txt");
        Point[] points1 = readInArray("./src/Part_9/Ex_15/resultSerialization.txt");
    }

    public static void writeInArray(Point[] points, String path) throws IOException {
        Path path_ = Paths.get(path);
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(path_.toFile()));
        out.writeObject(points);
    }

    public static Point[] readInArray(String path) throws IOException, ClassNotFoundException {
        Path path_ = Paths.get(path);
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(path_.toFile()));
        return (Point[]) in.readObject();
    }
}
