package Part_9.Ex_15;

import java.io.Serializable;

public class Point implements Serializable {
    static final long serialVersionUID = 42;

    int[] coordinates = new int[2];

    public Point(int x, int y) {
        coordinates[0] = x;
        coordinates[1] = y;
    }

    public int getX() {
        return coordinates[0];
    }

    public int getY() {
        return coordinates[1];
    }

    @Override
    public String toString() {
        return "Point{x=" + coordinates[0] + ", y=" + coordinates[1] + '}';
    }
}
