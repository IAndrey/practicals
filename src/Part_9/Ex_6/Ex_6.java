package Part_9.Ex_6;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Paths;

/*
Формат BMP для файлов несжатых изображений хорошо документирован и прост.
Используя произвольный доступ, напишите программу, отражающую положение
каждого ряда пикселей, не прибегая к записи в новый файл.
 */
public class Ex_6 {
    public static void main(String[] args) throws IOException {
        BufferedImage image = ImageIO.read(Paths.get("./src/Part_9/Ex_6/test.bmp").toFile());
        //использую цикл в котором i - ряды пикселей, image.getHeight() - кол - во рядов.
        // image.getWidth() - длина ряда
        for (int i = 0; i < image.getHeight(); i++) {
            for (int j = 0; j < image.getWidth(); j++) {
                if (image.getRGB(j, i) != -1 )
                    System.out.print(image.getRGB(j, i) + " ");  // выводим все пиксели в оду строку для ряда i, j -
                // длина ряда
            }
            System.out.println();
        }
    }
}
