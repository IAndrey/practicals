package Part_9;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/*
Пользоваться классом Scanner удобно, но он действует чуть медленнее,
чем класс BufferedReader. Организуйте построчное чтение длинного
файла, подсчитываю кол-во вводимых строк, во-первых, с помощью класса
Scanner и методов hasNextLine() и nextLine(); во вторых, с помощью
класса BufferedReader и метода readLine(); а в третьих, с помощью
класса BufferedReader и метода Lines(). Каким из способов читать из файла
быстрее и удобнее всего?
 */
public class Ex_4 {

    public static int meReadBufferedReader2(Path path) throws IOException {
        List<String> list;
        BufferedReader reader = new BufferedReader(new FileReader(path.toFile()));
        list = reader.lines().collect(Collectors.toList());
        return list.size();
    }

    public static int meReadBufferedReader(Path path) throws IOException {
        List<String> list = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(path.toFile()));
        String str;
        while ((str = reader.readLine()) != null) {
            list.add(str);
        }
        return list.size();
    }

    public static int meReadScanner(Path path) throws IOException {
        List<String> list = new ArrayList<>();
        Scanner scan = new Scanner(path);
        while (scan.hasNextLine()) {
            list.add(scan.nextLine());
        }
        return list.size();
    }
}
