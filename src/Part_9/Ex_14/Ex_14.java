package Part_9.Ex_14;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

/*
Реализуйте сериализируемый класс Point с переменными экземпляра для
хранения координат точки x и y. Напишите одну программу для сериали-
зации массива объектов типа Point в файл, а другую - для чтения из файла.
 */
public class Ex_14 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
//        Point[] points = {new Point(5, 5), new Point(2, 2), new Point(10, 15)};
//        writeInArray(points, "./src/Part_9/Ex_14/resultSerialization.txt");
        Point[] points1 = readInArray("./src/Part_9/Ex_14/resultSerialization.txt");
    }

    public static void writeInArray(Point[] points, String path) throws IOException {
        Path path_ = Paths.get(path);
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(path_.toFile()));
        out.writeObject(points);
    }

    public static Point[] readInArray(String path) throws IOException, ClassNotFoundException {
        Path path_ = Paths.get(path);
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(path_.toFile()));
        return (Point[]) in.readObject();
    }
}
