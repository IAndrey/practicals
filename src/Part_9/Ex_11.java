package Part_9;

import java.util.Arrays;

/*
Используя регулярное выражение, извлеките имена катологов (в виде
массива символьных строк), имя и расширение файла из абсолютного
или относительного пути вроде /home/cay/mylife.txt
 */
public class Ex_11 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(nameFolders("/home/cay/mylife.txt")));
        System.out.println(Arrays.toString(nameFolders("C:/Practicals/src/Part_9/Ex_8/res.txt")));
    }

    public static String[] nameFolders(String path) {
        path = (path.contains(":")) ? path : path.substring((path.charAt(0) == '/') ? 1 : 0);
        String[] folders = path.split(":?\\/");
        return folders;
    }
}
