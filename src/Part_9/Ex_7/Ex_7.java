package Part_9.Ex_7;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/*
Посмотрите документацию на класс MessageDigest и напишите программу,
вычисляющую свертку файла по алгоритму SHA-1. Снабдите блоками байтов
объект типа MessageDigest с помощью метода update(), а затем отобразите
результат вызова метода digest(). Убедитесь в том, что написанная вами программа
выдает такой же результат, как и утелита sha1sum

MessageDigest - предназначен для получения хеш из строки, для каждой строки будет уникальных хеш ключ.
Используется для защиты от взломов, так как пароли и другая информация в базе данных, хранится в виде
хеш сообщения.
Для расчета дайджеста сообщения, используется метод digest(), он возвращает блок байт, для получения хеш зн,
этот блок байт представляется в виде 16-ричного значенмя
Если есть несколько блоков данных для включения в один и тот же дайджест сообщения, вызовите метод update()

В результате этих действий мы можем вычислить весю иформацию из файла, в виде хеш ключа, длина всегда составляет 40
символов.

Результат проверял тут http://www.sha1-online.com/ (В тестах ожидаемое зн беру с этого сайта).
 */
public class Ex_7 {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        System.out.println(getFileHash("./src/Part_9/Ex_7/test.txt"));
    }

    public static String getFileHash(String path) throws IOException, NoSuchAlgorithmException {
        byte[] data = Files.readAllBytes(Path.of(path)); //считываю весь файл в блок байт, методом update()
        // не обязательо пользоваться в данном случае.
        MessageDigest ms = MessageDigest.getInstance("SHA-1");
        return getHash(ms.digest(data));
    }


    private static String getHash(byte[] digest) {
        BigInteger bigInteger = new BigInteger(1, digest); // использую BigInteger, чтобы перевести зн байтов
//      из массива в 16 разрядный вид.
        return bigInteger.toString(16);
    }
}
