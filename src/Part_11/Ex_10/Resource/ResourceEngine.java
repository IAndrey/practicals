package Part_11.Ex_10.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class ResourceEngine<T> {
    private T object;

    public List<String> returnPageInStr(T object) throws IllegalAccessException, MalformedURLException {
        List<String> result = new ArrayList<>();
        List<String> urls = getFieldResourceUrl(object);
        for (String s : urls) {
            result.add(readPage(new URL(s)));
        }
        return result;
    }

    private String readPage(URL url) {
        CompletableFuture<String> com = new CompletableFuture<>();
        String res = "";
        String input = "andrey19931" + ":" + "andrey199311";
        String encoding = Base64.getEncoder().encodeToString(input.getBytes(StandardCharsets.UTF_8));
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
//            connection.setRequestProperty("Authorization", "Basic " + encoding);
            InputStream content = connection.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(content));
            String line;
            while ((line = in.readLine()) != null) {
                res += line + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public List<String> getFieldResourceUrl(T object) throws IllegalAccessException {
        this.object = object;
        Class<T> tClass = (Class<T>) object.getClass();
        Field[] fields = getField(tClass.getDeclaredFields(), Resource.class);
        List<String> url = getUrl(fields, object);
        return url;
    }

    private List<String> getUrl(Field[] fields, T object) throws IllegalAccessException {
        ArrayList<String> list = new ArrayList<>();
        for (Field field : fields) {
            field.setAccessible(true);
            list.add(field.get(object).toString());
        }
        return list;
    }

    private Field[] getField(Field[] fields, Class<? extends Annotation> annotation) {
        List<Field> result = new ArrayList<>();
        for (Field field : fields) {
            if (checkAnnotation(field, annotation)) result.add(field);
        }
        return result.toArray(new Field[result.size()]);
    }

    private boolean checkAnnotation(Field field, Class<? extends Annotation> annotation) {
        if (field.getDeclaredAnnotation(annotation) == null) return false;
        Resource resource = (Resource) field.getDeclaredAnnotation(annotation);
        if (!resource.name().equals("URL")) return false;
        return true;
    }
}
