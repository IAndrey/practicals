package Part_11.Ex_10;

import Part_11.Ex_10.Resource.ResourceEngine;

import java.net.MalformedURLException;
import java.util.List;

/*
Реализуйте процессор аннотаций @Resource, принимающий объект некоторого класса и обнаруживающий поля типа
String, помечаемые аннотацией @Resource(name="URL"). Затем организуйте загрузку содержимого по заданному
URL и внедрите строковую переменную с этим содержимым, используя рефлесию.
 */
public class Ex_10 {
    public static void main(String[] args) throws IllegalAccessException, MalformedURLException {
        ResourceEngine<MyClass> engine = new ResourceEngine<>();
        List<String> listUrl = engine.returnPageInStr(new MyClass());
        listUrl.forEach(System.out::println);
    }
}
