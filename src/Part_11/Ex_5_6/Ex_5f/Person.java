package Part_11.Ex_5_6.Ex_5f;

import MyProcessor.Todo;

public class Person {
    String name;
    int age;

    @Todo
    public void setName(String name) {
        this.name = name;
    }

    @Todo
    public void setAge(int age) {
        this.age = age;
    }

    @Todo
    public String getName() {
        return name;
    }

    @Todo
    public int getAge() {
        return age;
    }
}
