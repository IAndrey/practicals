package Part_11.Ex_5_6.Ex_6f.ProcAndAnnot;

import java.lang.annotation.*;

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.METHOD)
@Repeatable(Todoes.class)
public @interface Todo2 {
}
