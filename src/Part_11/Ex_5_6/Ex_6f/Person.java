package Part_11.Ex_5_6.Ex_6f;

import MyProcessor2.Todo2;

public class Person {
    String name;
    int age;

    @Todo2
    @Todo2
    public void setName(String name) {
        this.name = name;
    }

    @Todo2
    @Todo2
    public void setAge(int age) {
        this.age = age;
    }

    @Todo2
    @Todo2
    public String getName() {
        return name;
    }

    @Todo2
    @Todo2
    public int getAge() {
        return age;
    }
}
