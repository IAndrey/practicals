package Part_11.Ex_2_3_4;

import Part_11.Ex_2_3_4.MySerializable.Serializable;
import Part_11.Ex_2_3_4.MySerializable.Transient;

@Serializable
public class MyClass {
    public String str;
    @Transient
    public int x;
    @Transient
    public double y;
    MyClass2 mc = new MyClass2("cl2", 10, 10);

    public MyClass(String str, int x, double y) {
        this.str = str;
        this.x = x;
        this.y = y;
    }
}
