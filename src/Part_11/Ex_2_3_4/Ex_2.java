package Part_11.Ex_2_3_4;

import Part_11.Ex_2_3_4.MySerializable.MyObjectInputStream;
import Part_11.Ex_2_3_4.MySerializable.MyObjectOutputStream;

import java.io.IOException;
import java.nio.file.Paths;

/*
Если бы аннотации присутствовали в первых версиях java, то интерфейс
Serializable, безусловно, был бы снабжен анотацией. Реализуйте анотацию
@Serializable. Выберите текстовый или двоичный формат для сохраняемости.
Предоставте классы для потоков ввода-вывода, чтения и записи, сохраняющих
состояние объектов путем сохранения и восстановления всех полей, содержащих
значения примитивных типов или же самих поддающихся сериализации. Не обращайте
пока что внемание на циклические ссылки.
 */
public class Ex_2 {
    public static void main(String[] args) throws IOException, IllegalAccessException {
        MyClass cl = new MyClass("cl1", 5, 5);
        MyObjectOutputStream out = new MyObjectOutputStream(Paths.get("./src/Part_11/Ex_2_3_4/MySerializable/file.txt").toFile());
        out.write(cl);
        out.close();
        MyObjectInputStream ois = new MyObjectInputStream(Paths.get("./src/Part_11/Ex_2_3_4/MySerializable/file.txt"));
        ois.read(cl);
        System.out.println(cl.str);
        System.out.println(cl.x);
        System.out.println(cl.y);
    }
}
