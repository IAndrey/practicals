package Part_11.Ex_2_3_4;

import Part_11.Ex_2_3_4.MySerializable.Serializable;

@Serializable
public class MyClass2 {
    String str;
    int x = 0;
    double y;

    public MyClass2(String str, int x, double y) {
        this.str = str;
        this.x = x;
        this.y = y;
    }
}
