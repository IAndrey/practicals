package Part_11.Ex_2_3_4;

import Part_11.Ex_2_3_4.MySerializable.MyObjectInputStream;
import Part_11.Ex_2_3_4.MySerializable.MyObjectOutputStream;

import java.io.IOException;
import java.nio.file.Paths;

/*
Введите анотацию @Transient в механизм сериализации, действующий подобно модификатору доступа transient.
 */
public class Ex_4 {
    public static void main(String[] args) throws IOException, IllegalAccessException {
        MyClass cl = new MyClass("s", 5, 5);
        MyObjectOutputStream out = new MyObjectOutputStream(Paths.get("./src/Part_11/Ex_2_3_4/MySerializable/file.txt").toFile());
        out.write(cl);
        out.close();
        MyObjectInputStream ois = new MyObjectInputStream(Paths.get("./src/Part_11/Ex_2_3_4/MySerializable/file.txt"));
        ois.read(cl);
        System.out.println(cl.str);
        System.out.println(cl.x);
        System.out.println(cl.y);
        System.out.println(cl.mc.str);
        System.out.println(cl.mc.x);
        System.out.println(cl.mc.y);
    }

}

