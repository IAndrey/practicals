package Part_11.Ex_2_3_4.MySerializable;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.Queue;

public class MyObjectInputStream {
    private Path path;
    private Queue<String> queue;

    public MyObjectInputStream(Path path) throws IOException {
        this.path = path;
        queue = new LinkedList<>(Files.readAllLines(path));
    }

    public void read(Object obj) throws IOException, IllegalAccessException {
        if (!verifyAnnotationSerializ(obj.getClass()))
            throw new RuntimeException("Не поддерживает аннотацию Serializable");
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field f : fields) {
            if (verifyAnnotationTransient(f)) {
                f.set(obj, (f.get(obj) instanceof String)? "": 0);
                continue;
            }
            f.setAccessible(true);
            setter(f, obj, queue.poll());
        }
    }

    private void setter(Field field, Object object, String value) throws IllegalAccessException, IOException {
        if (!set(field, object, value)) {
            read(field.get(object));
        }
    }

    private boolean set(Field f, Object obj, String value) throws IllegalAccessException {
        switch (f.getType().getSimpleName()) {
            case "String": {
                f.set(obj, value);
                return true;
            }
            case "double": {
                f.setDouble(obj, Double.parseDouble(value));
                return true;
            }
            case "int": {
                f.setInt(obj, Integer.parseInt(value));
                return true;
            }
            case "float": {
                f.setFloat(obj, Float.parseFloat(value));
                return true;
            }
            case "short": {
                f.setShort(obj, Short.parseShort(value));
                return true;
            }
            case "char": {
                f.setChar(obj, value.charAt(0));
                return true;
            }
            case "long": {
                f.setLong(obj, Long.parseLong(value));
                return true;
            }
            case "byte": {
                f.setByte(obj, Byte.parseByte(value));
                return true;
            }
            case "boolean": {
                f.setBoolean(obj, (value.equals("true")));
                return true;
            }
        }
        return false;
    }

    private boolean verifyAnnotationSerializ(Class cl) {
        for (Annotation an : cl.getDeclaredAnnotations()) {
            if (an.annotationType().getSimpleName().equals("Serializable")) {
                return true;
            }
        }
        return false;
    }

    private boolean verifyAnnotationTransient(Field field) {
        for (Annotation an : field.getDeclaredAnnotations()) {
            if (an.annotationType().getSimpleName().equals("Transient")) ;
            return true;
        }
        return false;
    }
}
