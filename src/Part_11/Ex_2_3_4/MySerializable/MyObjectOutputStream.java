package Part_11.Ex_2_3_4.MySerializable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class MyObjectOutputStream {
    private File file;
    private PrintWriter writer;

    public MyObjectOutputStream(File file) throws FileNotFoundException {
        this.file = file;
        writer = new PrintWriter(file);
    }

    public void write(Object object) throws IllegalAccessException, FileNotFoundException {
        if (!verifyAnnotationSerializable(object.getClass()))
            throw new RuntimeException(object.getClass() + " не поддерживает аннотацию Serializable");
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (verifyAnnotationTransient(field)) continue;
            field.setAccessible(true);
            writer(field.get(object), field.getType());
        }
    }

    // записываем примитивные поля в файл, либо дальше раскрываем
    private void writer(Object object, Class<?> type) throws IllegalAccessException, FileNotFoundException {
        if (type.isPrimitive() || object instanceof String) {
            writeInFile(String.valueOf(object));
        } else {
            writeInFile(String.valueOf(object.toString()));
            write(object);
        }
    }

    private void writeInFile(String str) throws FileNotFoundException {
        writer.println(str);
    }

    private boolean verifyAnnotationTransient(Field field) {
        for (Annotation an : field.getDeclaredAnnotations()) {
            if (an.annotationType().getSimpleName().equals("Transient")) {
                return true;
            }
        }
        return false;
    }

    private boolean verifyAnnotationSerializable(Class<?> cl) {
        for (Annotation an : cl.getDeclaredAnnotations()) {
            if (an.annotationType().getSimpleName().equals("Serializable")) {
                return true;
            }
        }
        return false;
    }

    public void close() {
        writer.close();
    }
}
