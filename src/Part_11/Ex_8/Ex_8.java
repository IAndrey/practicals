package Part_11.Ex_8;
/*
Реализуйте аннотацию @Testcase, сформировав исходный файл, имя которого состоит из
имени класса, где эта аннотация встречается, а так же из имени Test. Так, если исходный файл MyMath.java
содержит следующие строки:

@TestCase (params = "4", expected = "24")
@TestCase (params = "0", expected = "1")
public static long factorial(int n) {...}

то сформируйте исходный файл MyMathTest.java со следующими операторами:
assert(MyMath.factorial(4) == 24);
assert(MyMath.factorial(0) == 1);
Можете допустить, что тестовые методы являются статическими и что элемент аннотации
params содержит разделяемый запятыми список параметров соответствующего типа.
 */
public class Ex_8 {
    public static boolean sassert(boolean bol) {
        return bol;
    }

    public static void main(String[] args) {

    }

}
