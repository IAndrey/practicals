package Part_11.Ex_8.Proc;

import java.lang.annotation.*;

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.METHOD)
@Repeatable(TestCases.class)
public @interface TestCase {
    int params();
    int expected();
}
