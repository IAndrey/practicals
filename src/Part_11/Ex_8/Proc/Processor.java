package Part_11.Ex_8.Proc;

import MyProcessorEx8.TestCase;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SupportedAnnotationTypes({"MyProcessorEx8.TestCase", "MyProcessorEx8.TestCases"})
public class Processor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (annotations.isEmpty()) return false;
        String className = getClassFullName(roundEnv);
        List<String> meth = getMethods(roundEnv, annotations);
        try {
            writeClass(className, meth);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    private List<String> getMethods(RoundEnvironment roundEnv, Set<? extends TypeElement> annotations) {
        List<String> result = new ArrayList<>();
        for (TypeElement annotation : annotations) {
            List<Element> elements = new ArrayList<>(roundEnv.getElementsAnnotatedWith(annotation));
            for (Element element : elements) {
                result.addAll(sdsds(element, roundEnv));
            }
        }
        return result;
    }

    private List<String> sdsds(Element element, RoundEnvironment roundEnv) {
        List<String> resss = new ArrayList<>();
        List<? extends AnnotationMirror> list = element.getAnnotationMirrors();
        for (AnnotationMirror annotation : list) {
            if (annotation.getAnnotationType().asElement().getSimpleName().toString().equals("TestCases")) {
                resss.addAll(getResTestCases(element, roundEnv, annotation));
            } else {
                resss.add(getResTestCase(element, roundEnv, annotation).toString());
            }
        }
        return resss;
    }

    private List<String> getResTestCases(Element element, RoundEnvironment roundEnv, AnnotationMirror annotation) {
        List<String> result = new ArrayList<>();
        String[] ress = new ArrayList<>(annotation.getElementValues().values()).get(0).getValue().toString().split(",@");
        for (String str : ress) {
            StringBuilder res = new StringBuilder();
            res.append(getClassName(getClassFullName(roundEnv))).append("." + element.getSimpleName()).append("(");
            getNumeric(str, res);
            result.add(res.toString());
        }
        return result;
    }

    private StringBuilder getNumeric(String str, StringBuilder res) {
        Matcher math = Pattern.compile("=\\d+").matcher(str);
        List<String> list = new ArrayList<>();
        while (math.find()) {
            list.add(math.group().substring(1));
        }
        res.append(list.get(0)).append(")").append(" == ").append(list.get(1)).append(")");
        return res;
    }

    private StringBuilder getResTestCase(Element element, RoundEnvironment roundEnv, AnnotationMirror annotation) {
        StringBuilder res = new StringBuilder();
        res.append(getClassName(getClassFullName(roundEnv))).append("." + element.getSimpleName()).append("(");
        List<String> list1 = new ArrayList<>();
        for (AnnotationValue value : annotation.getElementValues().values()) {
            list1.add(value.getValue().toString());
        }
        res.append(list1.get(0) + ") == ").append(list1.get(1) + ")");
        return res;
    }

    private String getClassName(String classFullName) {
        int lastDot = classFullName.lastIndexOf('.');
        return classFullName.substring(lastDot + 1);
    }

    private String getClassFullName(RoundEnvironment roundEnv) {
        List<? extends Element> head = new ArrayList<>(roundEnv.getElementsAnnotatedWith(TestCase.class));
        return ((TypeElement) head.get(0).getEnclosingElement()).getQualifiedName().toString();
    }

    private void writeClass(String className, List<String> test) throws IOException {
        String packageName = null;
        int lastDot = className.lastIndexOf('.');
        if (lastDot > 0) {
            packageName = className.substring(0, lastDot);
        }
        String toStringsClassName = className.substring(lastDot + 1) + "Test";
        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(toStringsClassName);
        try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
            if (packageName != null) {
                out.print("package ");
                out.print(packageName);
                out.println(";");
                out.println();
            }
            out.println("public class " + toStringsClassName + " {");
            out.println("public static void main(String[] args) {");
            for (String str : test) {
                out.println("assert(" + str + ";");
            }
            out.println("}");
            out.println("}");
        }
    }
}
