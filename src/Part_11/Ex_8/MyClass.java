package Part_11.Ex_8;

import MyProcessorEx8.TestCase;

public class MyClass {

    @TestCase(params = 10, expected = 100)
    public static int p2ow(int n) {
        return n * n;
    }

    @TestCase(params = 2, expected = 4)
    @TestCase(params = 3, expected = 9)
    @TestCase(params = 9, expected = 81)
    public static int pow(int n) {
        return n * n;
    }
}
