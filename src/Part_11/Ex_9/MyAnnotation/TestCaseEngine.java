package Part_11.Ex_9.MyAnnotation;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TestCaseEngine<T> {
    private T object;

    public void testObject(T object) {
        this.object = object;
        Class<T> oClass = (Class<T>) object.getClass();
        Method[] methods = oClass.getDeclaredMethods();
        testMethods(methods);
    }

    private void testMethods(Method... methods) {
        for (Method method : methods) {
            if (checkAnnotations(method, TestCase.class)) testCases(method, method.getDeclaredAnnotation(TestCase.class));
            if (checkAnnotations(method, TestCases.class))testCases(method, method.getDeclaredAnnotation(TestCases.class).value());
        }
    }

    private boolean checkAnnotations(Method method, Class cl) {
        return null != method.getDeclaredAnnotation(cl);
    }

    private void testCases(Method method, TestCase... tests) {
        try {
            System.out.println("Test method - " + method.getName() + ":");
            for (TestCase ts : tests) {
                int expected = ts.expected();
                int parameter = ts.parameter();
                System.out.println("parameter - " + parameter + ", expected - "
                        + expected + ", result - " + invokeMethod(method, expected, parameter));
            }
            System.out.println();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private boolean invokeMethod(Method method, int expected, int parameter) throws InvocationTargetException, IllegalAccessException {
        method.invoke(object, parameter);
        return expected == (Integer) method.invoke(object, parameter);
    }
}
