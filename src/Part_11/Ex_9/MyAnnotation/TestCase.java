package Part_11.Ex_9.MyAnnotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(TestCases.class)
public @interface TestCase {
    int parameter();
    int expected();
}
