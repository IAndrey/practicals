package Part_11.Ex_9.MyAnnotation;

public class TestClass {

    @TestCase(parameter = 10, expected = 100)
    public int pow(int n) {
        return n*n;
    }

    @TestCase(parameter = 3, expected = 6)
    @TestCase(parameter = 7, expected = 13)
    public int x2(int n) {
        return n*2;
    }
}
