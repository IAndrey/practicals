package Part_11.Ex_9;

import Part_11.Ex_9.MyAnnotation.TestClass;
import Part_11.Ex_9.MyAnnotation.TestCaseEngine;

/*
Реализуйте аннотацию @TestCase как динамическую и предоставьте инструментальное средство для
ее проверки. И в этом случае можете допустить, что тестовые методы являются статическими.
Можете так же ограничиться умеренным набором параметров и возвращаемых типов, описываемых
символьными строками в элементах аннотации.
 */
public class Ex_9 {
    public static void main(String[] args) {
        TestCaseEngine<TestClass> dsf = new TestCaseEngine<>();
        dsf.testObject(new TestClass());
    }
}
