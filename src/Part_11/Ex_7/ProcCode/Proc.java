package Part_11.Ex_7.ProcCode;

import MyProcessorEx7.Body;
import MyProcessorEx7.Head;
import MyProcessorEx7.Path;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@SupportedAnnotationTypes({"MyProcessorEx7.Head", "MyProcessorEx7.Body", "MyProcessorEx7.Path"})
public class Proc extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (annotations.isEmpty()) return false;
        List<? extends Element> head = new ArrayList<>(roundEnv.getElementsAnnotatedWith(Head.class));
        String className = ((TypeElement) head.get(0).getEnclosingElement()).getQualifiedName().toString();
        List<? extends Element> body = new ArrayList<>(roundEnv.getElementsAnnotatedWith(Body.class));
        List<? extends Element> path = new ArrayList<>(roundEnv.getElementsAnnotatedWith(Path.class));
        Queue<String> elements = new LinkedList<>();
        elements.add(path.get(0).getSimpleName().toString());
        elements.add(head.get(0).getSimpleName().toString());
        elements.add(body.get(0).getSimpleName().toString());
        try {
            writeClass(className, elements);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    private void writeClass(String className, Queue<String> elements) throws IOException {
        String packageName = null;
        int lastDot = className.lastIndexOf('.');
        if (lastDot > 0) {
            packageName = className.substring(0, lastDot);
        }
        String toStringsClassName = "HTML_Engine";
        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(toStringsClassName);
        try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
            if (packageName != null) {
                out.print("package ");
                out.print(packageName);
                out.println(";");
                out.println();
            }
            out.println("import java.io.File;");
            out.println("import java.io.FileNotFoundException;");
            out.println("import java.io.PrintWriter;\n");
            out.print("public class ");
            out.print(toStringsClassName);
            out.println("{");
            out.println("public static void execute(CreateHTML obj) throws FileNotFoundException {");
            out.println("PrintWriter writer = new PrintWriter(new File(obj." + elements.poll() + "()));");
            out.println("StringBuilder html = new StringBuilder();");
            for (String str : elements) {
                out.println("html.append(obj." + str + "() + \"\\n\");");
            }
            out.println("writer.print(html.toString());");
            out.println("writer.close();");
            out.println("}");
            out.println("}");
        }
    }
}
