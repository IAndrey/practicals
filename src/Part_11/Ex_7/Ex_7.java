package Part_11.Ex_7;



import Part_11.Ex_7.HTML_Engine;

import java.io.*;
import java.nio.file.Paths;

/*
Если бы аннотации существовали в первых версиях java, они скорее всего
выполняли бы роль утилиты javadoc. Определите аннотации @Param, @Return
и т.д. и составьте из них элементарный HTML-документ с помощью процессора
аннотаций.
 */
public class Ex_7 {
    public static void main(String[] args) throws IOException {
        HTML_Engine.execute(new CreateHTML());
    }
}
