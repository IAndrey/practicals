package Part_2.Ex_10;

import java.util.ArrayList;

public class RandomNumbers {
    // метод нельзя зделать методом экземпляра т.к. по условию задачи сказано что он должен быть статическим.
    public static int randomElement (int [] elements) {
        if (elements.length == 0) return 0;
        int rand = (int) (Math.random() * elements.length - 1);
        return elements[rand];
    }

    public static int randomElement (ArrayList<Integer> elements) {
        if (elements.size() == 0) return 0;
        int rand = (int) (Math.random() * elements.size() - 1);
        return elements.get(rand);
    }
}
