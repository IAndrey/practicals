package Part_2;

import java.util.Random;
import java.util.Scanner;

/**
 * Metod nextInt() класса сканер является методом доступа, т.к.
 * он не изменяет объект у которого вызывается, а возвращает int
 *
 *То же самое с методом nextInt() класса Random
 *
 *
 *
 */
public class Ex_2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int x = scan.nextInt();

        Random rand = new Random();
        int y = rand.nextInt();
    }
}
