package Part_2.Ex_9;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Car {
    //    private int speed = 100;
    private final BigDecimal maxCapacityFuel = new BigDecimal(50);
    private BigDecimal capacityFuel = new BigDecimal(0).setScale(2,RoundingMode.HALF_UP);
    private BigDecimal efficiency ;
    private BigDecimal distance = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);

    public Car(double efficiency) {
        this.efficiency = new BigDecimal(efficiency).setScale(2, RoundingMode.HALF_UP);
    }

    public void setCapacityFuel(double capacityFuel) {
        BigDecimal value = new BigDecimal(capacityFuel).setScale(2, RoundingMode.HALF_UP);
        if (capacityFuel <= 0) throw new RuntimeException("Нельзя заправиться на "+ capacityFuel + " литров");
        System.out.print("Заливаем " + notEndNull(value) + " литров бензина в бак.");
        if ( (this.capacityFuel.add(value).compareTo(maxCapacityFuel)) == 1) {
            System.out.print(" Превышен лимит, объем бака " + notEndNull(maxCapacityFuel) + " в баке " + notEndNull(this.capacityFuel));
            System.out.println(" литров. Залили " + notEndNull(maxCapacityFuel.subtract(this.capacityFuel)) + " литров, бак полный");
            this.capacityFuel = maxCapacityFuel;
        } else {
            this.capacityFuel = this.capacityFuel.add(value);
            System.out.println(" В бак залили " + notEndNull(value) + " литров. В баке - " + notEndNull(this.capacityFuel) + " литров");
        }
    }

    public void setDistance(double distance) {
        BigDecimal dist = new BigDecimal(distance).setScale(2, RoundingMode.HALF_UP);

        if (distance < 0) throw new RuntimeException("Укажите расстояние");
        if ((capacityFuel.compareTo(new BigDecimal(0))) == 0) throw new RuntimeException("В баке 0 литров, нужно заправиться");

        BigDecimal requiredFuel = dist.divide(efficiency).setScale(2, RoundingMode.HALF_UP);;

        if (requiredFuel.compareTo(capacityFuel) == 1) {
            BigDecimal currentDist = capacityFuel.multiply(efficiency).setScale(2, RoundingMode.HALF_UP);
            System.out.println("Топлива хватило на " + notEndNull(currentDist) + " км. Проехали " + notEndNull(currentDist) + " км");
            this.capacityFuel = new BigDecimal(0);
            this.distance = this.distance.add(currentDist);
        } else {
            this.capacityFuel = this.capacityFuel.subtract(requiredFuel);
            System.out.print("Проехали " + notEndNull(dist) + " км.");
            System.out.println(" Осталось топлива " + getCapacityFuel() + " литров.");
            this.distance = this.distance.add(dist);
        }

    }

    public String getDistance() {
        return "Всего пройдено км - " + notEndNull(distance);
    }

    public String getCapacityFuel() {
        return notEndNull(capacityFuel);
    }

    private static String notEndNull(BigDecimal number){
        String rezult = number.toString();
        if (number.toString().endsWith(".00")) {
            rezult = number.toString().replaceAll("\\.00", "");
        }
        if (number.toString().matches(".*\\.\\d0")) {
            rezult = (number.toString() + "+").replaceAll("0\\+", "");
        }
        return rezult;
    }
}
