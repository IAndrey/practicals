package Part_2.Ex_9;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Ex_9 {
    public static void main(String[] args) {
        Car lada = new Car(10);

        lada.setCapacityFuel(10);

        lada.setDistance(150.6);

        lada.setCapacityFuel(100.7);
        lada.setDistance(150.7);

        System.out.println(lada.getDistance());
    }
}
