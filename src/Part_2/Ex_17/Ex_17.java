package Part_2.Ex_17;

import java.util.Iterator;

public class Ex_17 {
    public static void main(String[] args) {
        Queue<String> myQue = new Queue();
        myQue.add("1");
        myQue.add("2");
        myQue.add("3");
        myQue.add("4");
        myQue.add("5");
        myQue.add("6");
        myQue.add("7");
        myQue.add("8");

        Queue.Iterator myIterator = myQue.iterator();
        while (myIterator.hasNext()) {
            System.out.println(myIterator.next());
        }

        System.out.println(myQue.size());

    }
}
