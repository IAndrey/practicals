package Part_2.Ex_17;

/**
 * Внутренний класс Iterator не может быть статическим, так как для
 * выполнения метода next() мы должны взять последний элемент из очереди и удалить его.
 * А что бы это сделать, мы должны иметь доступ к нестатическим полям внешнего класса.
 * У статического внутреннего класа есть доступ только к статическим полям и методам внешнего класса.
 * @param <T>
 */

public class Queue<T> {

    private Node lastNode;
    private int size = 0;

    public Queue() {
        lastNode = new Node(null, null);
    }

    private class Node<T> {
        private T currentElement;
        private Node prevElement;

        public Node(T currentElement, Node<T> prevElement) {
            this.currentElement = currentElement;
            this.prevElement = prevElement;
        }

        Node<T> getPrevElement() {
            return prevElement;
        }

        T getCurrentElement() {
            return currentElement;
        }

        void setCurrentElement(T currentElement) {
            this.currentElement = currentElement;
        }
    }

    public void add(T newElement) {
        if (newElement == null) {
            throw new NullPointerException("No add is null");
        }
        Node newLast = new Node<>(newElement, lastNode);
        lastNode = newLast;
        size++;
    }

    public void remove() {
        if (lastNode.getCurrentElement() == null) throw new RuntimeException("В очереди нет элементов");
        lastNode = lastNode.getPrevElement();
        size--;
    }

    public T getLastElementAndRemove() {
        if (lastNode.getCurrentElement() == null) throw new RuntimeException("В очереди нет элементов");
        Object element = lastNode.getCurrentElement();
        lastNode = lastNode.getPrevElement();
        size--;
        return (T) element;
    }

    public int size() {
        return size;
    }

    public Iterator iterator() {
        return new Iterator();
    }

    public class Iterator {
        T next() {
            T element = (T) lastNode.currentElement;
            lastNode = lastNode.prevElement;
            return element;
        }

        boolean hasNext() {
            if (lastNode.prevElement == null) return false;
            size--;
            return true;
        }

    }

}


