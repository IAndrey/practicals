package Part_2.Ex_5;

public class Ex_5 {
    public static void main(String[] args) {
        Point point1 = new Point(3, 4);
        Point point2 = point1;
        point2 = point2.translate(1,3).scale(0.5);
        point1.draw();
        System.out.println();
        point2.draw();
    }
}
