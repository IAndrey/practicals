package Part_2.Ex_5;
/**
Является неизменияемым (immutable) классом. Указывает на точку в плоскости.
Методы translate(double x, double y) и scale(double scale),
являются методами доступа и возвращают новые объекты.
*/
public class Point {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point() {
        x = 0;
        y = 0;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Point translate(double x, double y) {
        return new Point(this.x + x, this.y + y);
    }

    public Point scale(double scale) {
        return new Point(x * scale, y * scale);
    }

    public void draw() {
        char[][] table = new char[10][10];
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                table[i][j] = 'O';
            }
        }
        table[(int)y][(int) x] = 'X';
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }
}
