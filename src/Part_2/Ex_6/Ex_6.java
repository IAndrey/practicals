package Part_2.Ex_6;

import Part_2.Ex_5.Point;

public class Ex_6 {
    public static void main(String[] args) {
        Point point1 = new Point(3, 4);
        Point point2 = point1;
        point2.translate(1,3).scale(0.5);
        point1.draw();
        System.out.println();
        point2.draw();
    }
}
