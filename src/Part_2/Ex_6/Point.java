package Part_2.Ex_6;
/**
 Является изменияемым (mutable) классом. Указывает на точку в плоскости.
 Методы translate(double x, double y) и scale(double scale),
 являются модифицирующими методами, меняют свойства объекта
 у которого они вызваны и возвращают этот лбъект.
 */
public class Point {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point() {
        x = 0;
        y = 0;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Point translate(double x, double y) {
        this.x += x;
        this.y += y;
        return this;
    }

    public Point scale(double scale) {
        this.x *= scale;
        this.y *= scale;
        return this;
    }

    public void draw() {
        char[][] table = new char[10][10];
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                table[i][j] = 'O';
            }
        }
        table[(int)y][(int) x] = 'X';
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }
}
