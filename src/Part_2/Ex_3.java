package Part_2;

import java.util.ArrayList;

/**
 * Да, модифицирующий метод может возвращать какой либо объект, к примеру boolean зн.
 * К примеру метод add, класса ArrayList. Данный метод является модифицирующим и вовр зн. boolean.
 * Метод доступа может ничего не возвращать, просто выводить что то в консоль к примеру outList (String str);
 */
public class Ex_3 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("1");

    }

    public void outList(ArrayList<String> list) {
        for (String s : list) System.out.println(s);
    }
}
