package Part_2.Ex_13;

import com.opencsv.CSVReader;

import java.io.FileReader;

public class CSV {
    public static void main(String[] args) {
        try {
            CSVReader reader = new CSVReader(new FileReader("yourfile.csv"));
            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                // nextLine[] is an array of values from the line
                System.out.println(nextLine[0] + nextLine[1] + "etc...");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
