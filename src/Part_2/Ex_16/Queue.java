package Part_2.Ex_16;

public class Queue<T> {

    private Node lastNode;
    private int size = 0;

    public Queue() {
        lastNode = new Node(null, null);
    }

    private class Node<T> {
        private T currentElement;
        private Node prevElement;

        public Node(T currentElement, Node<T> prevElement) {
            this.currentElement = currentElement;
            this.prevElement = prevElement;
        }

        Node<T> getPrevElement() {
            return prevElement;
        }

        T getCurrentElement() {
            return currentElement;
        }

        void setCurrentElement(T currentElement) {
            this.currentElement = currentElement;
        }
    }

    public void add(T newElement) {
        if (newElement == null) {
            throw new NullPointerException("No add is null");
        }
        Node newLast = new Node<>(newElement, lastNode);
        lastNode = newLast;
        size++;
    }

    public void remove() {
        if (lastNode.getCurrentElement() == null) throw new RuntimeException("В очереди нет элементов");
        lastNode = lastNode.getPrevElement();
        size--;
    }

    public T getLastElementAndRemove() {
        if (lastNode.getCurrentElement() == null) throw new RuntimeException("В очереди нет элементов");
        Object element = lastNode.getCurrentElement();
        lastNode = lastNode.getPrevElement();
        size--;
        return (T) element;
    }

    public int size() {
        return size;
    }

}


