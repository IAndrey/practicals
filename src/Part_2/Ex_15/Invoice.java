package Part_2.Ex_15;

import java.util.ArrayList;

public class Invoice {
    private static class Item {
        String description;
        int quantity;
        double unitPrice;

        double price() {
            return quantity * unitPrice;
        }
    }

    private ArrayList<Item> items = new ArrayList<>();

    public void addItem (String description, int quantity, double unitPrice) {
        Item newItem = new Item();
        newItem.description = description;
        newItem.quantity = quantity;
        newItem.unitPrice = unitPrice;
        items.add(newItem);
    }

    public void outInvoice () {
        String format = "%20s    %3d        %6.2f      %6.2f";
        System.out.println("       ItemName        quantity   unitPrice   totalPrice");
        double allPrice = 0;
        for (Item item : items) {
            allPrice += item.price();
            System.out.printf(format, item.description, item.quantity, item.unitPrice, item.price());
            System.out.println();
        }
        System.out.printf("All price: %.2f", allPrice);
    }
}
