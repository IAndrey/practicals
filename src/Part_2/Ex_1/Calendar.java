package Part_2.Ex_1;

import java.time.LocalDate;
import java.util.ArrayList;

public class Calendar {
    private final String[] weekDays = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    private int withColumn = 6;
    private LocalDate date;

    public Calendar(int year, int month) {
        date = LocalDate.of(year, month, 1);
    }

    public Calendar() {
        date = LocalDate.now();
        date = LocalDate.of(date.getYear(), date.getMonth(), 1);
    }

    public String getTitle() {
        StringBuilder title = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 7; j++) {
                title.append(String.format("%-" + withColumn + "s", weekDays[j]));
            }
            title.append("|   ");
        }
        return title.toString();
    }

    public String daysOfWeek() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 7; i++) {
            date = date.plusDays(1);
            stringBuilder.append(String.format("%6s", date.getDayOfMonth()));
        }
        return stringBuilder.toString();
    }

    private String firstWeek() {
        int mnog = date.getDayOfWeek().getValue();
        if (mnog == 7) {
            mnog = 0;
        }
        StringBuilder stringBuilder = new StringBuilder();
        String format = "%" + (mnog * withColumn + 2) + "s";
        stringBuilder.append(String.format(format, date.getDayOfMonth()));
        date = date.plusDays(1);

        for (int i = 0; i <= 7; i++) {
            i = date.getDayOfWeek().getValue();
            stringBuilder.append(String.format("%6s", date.getDayOfMonth()));
            if (i == 6) break;
            date = date.plusDays(1);
        }
        return stringBuilder.toString();
    }

    public String getCalendar() {
        StringBuilder rezult = new StringBuilder(date.getMonth().toString()+"\n");
        rezult.append(getTitle() + "\n");

        ArrayList<String> weeks = new ArrayList<>();
        weeks.add(firstWeek());
        for (int i = 0; i < 6; i++) {
            weeks.add(daysOfWeek());
        }
        rezult.append(weeks.get(0));
        rezult.append("    " + weeks.get(2));
        rezult.append("    " + weeks.get(4));
        rezult.append("\n" + weeks.get(1).substring(4));
        rezult.append("    " + weeks.get(3));
        rezult.append("    " + weeks.get(5));

        return rezult.toString();
    }
}