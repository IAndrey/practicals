package Part_2;

//import org.omg.CORBA.IntHolder;

/**
 * В java все классы обертки над примитивными типами являются immutable классами,
 * т.е. неизменяемыми классами. Соответственно если передать в параметры метода
 * Integer или int, то туда передаются клоны оригенальных объектов, и все изменения
 * происходят с их клонами. Поэтому нельзя написать метод, модифицирующий классы immutable.
 */

public class Ex_4 {
    public static void main(String[] args) {
//        int x = 5;
//        int y = 10;
//        swap(x, y);
//        System.out.println(x + "\n" + y);

//        IntHolder q = new IntHolder(5);
//        IntHolder w = new IntHolder(10);
//
//        swap(q, w);
//
//        System.out.println(q.value + "\n" + w.value);
//
//    }
//
//    public static void swap(int x, int y) {
//        int rep = y;
//        y = x;
//        x = rep;
//    }
//
//    public static void swap(IntHolder q, IntHolder w) {
//        IntHolder rep = new IntHolder(w.value);
//        w.value = q.value;
//        q.value = rep.value;
    }
}
