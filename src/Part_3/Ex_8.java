package Part_3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.function.Consumer;

public class Ex_8 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("W");
        list.add("G");
        list.add("D");
        list.add("B");
        list.add("A");
        luckySort(list, String::compareTo);
//        luckySort(list, (o1, o2) -> {return 16;});

        out(list, System.out::println);
    }

    private static void luckySort(ArrayList<String> strings, Comparator<String> comp) {
        for (int i = 0; i < strings.size() - 1; i++) {
            if (comp.compare(strings.get(i), strings.get(i + 1)) > 0) {
                Collections.shuffle(strings);
            } else break;
        }
        strings.forEach(System.out::println);
    }

    private static void out(ArrayList<String> strings, Consumer<String> cons) {
        for (int i = 0; i < strings.size(); i++) {
            cons.accept(strings.get(i));
        }
    }
}
