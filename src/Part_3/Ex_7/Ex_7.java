package Part_3.Ex_7;

public class Ex_7 {
    public static void main(String[] args) {
        DigitSequence ds = new DigitSequence(1579);
        ds.forEachRemaining(System.out::println); //в метод forEachRemaining передается интерфейс Consumer, в котором есть метод void accept(T t),
        // в который закидывается след число из объекта DigitSequence с помощью метода next(); В данном случае можно прокинуть ссылку на метод println,
        // который получает элемент с помощью next();
        ds.remove();
    }
}
