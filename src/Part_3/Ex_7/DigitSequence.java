package Part_3.Ex_7;

import java.util.Iterator;
/*
При переходе из 7 версии к 8, код будет компилироваться, т.к.
в 8 версии java в интерфейсе Iterator метод remove() стал методом
по умолчанию, а так же добавлен метод по умолчанию forEachRemaining(Consumer<? super E> action);
Методы по умолчанию имеют реализацию по умолчанию и не обязаны быть переопределены в классах,
имплементирующих данные интерфейсы. Если вызвать метод remove() ничего не произойдет. Так как по условию задачи
он переопределен и является пустым. Если бы я его не переопределял, он прокинул бы мне ошибку throw new UnsupportedOperationException("remove");
т.к. это его реализация по умолчанию.
 */
//Реализация класса, соответствует реализации из книги.
public class DigitSequence implements Iterator<Integer> {
    private int number;

    public DigitSequence(int number) {
        this.number = number;
    }

    @Override
    public boolean hasNext() {
        return number != 0;
    }

    @Override
    public Integer next() {
        int rezult = number % 10;
        number /=10;
        return rezult;
    }

    @Override
    public void remove() {

    }
}
