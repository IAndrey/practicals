package Part_3.Ex_9;

public class Ex_9 {
    public static void main(String[] args) {
        Greeter gr1 = new Greeter(10, "Vasia");
        Greeter gr2 = new Greeter(5, "Petia");
        Thread th1 = new Thread(gr1);
        Thread th2 = new Thread(gr2);
        th1.start();
        th2.start();
    }
}
