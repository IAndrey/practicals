package Part_3;

public class Ex_14 {
    public static void main(String[] args) {
        Runnable [] runnables = new Runnable[3];
        runnables[0] = () -> System.out.println("1");
        runnables[1] = () -> System.out.println("2");
        runnables[2] = () -> System.out.println("3");

        Runnable runnable = myRunnable(runnables);
        runnable.run();
    }

    public static Runnable myRunnable(Runnable[] runnables) {
        return () -> {
          for (Runnable runnable : runnables) {
              runnable.run();
          }
        };
    }
}
