package Part_3.Ex_1_and_2;

public class Employee implements Measurable {
    private double wage;
    private  String name;

    public Employee(double wage, String name) {
        this.name = name;
        this.wage = wage;
    }

    public String getName() {
        return name;
    }

    @Override
    public double getMeasure() {
        return wage;
    }
}
