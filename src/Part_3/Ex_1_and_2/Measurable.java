package Part_3.Ex_1_and_2;

public interface Measurable {
    double getMeasure();
}
