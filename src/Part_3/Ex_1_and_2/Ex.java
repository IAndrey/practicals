package Part_3.Ex_1_and_2;

/**
 * В данной задаче необходимо выполнить нисходящее приведение,
 * так как ссылка на Объект Employee имеет тип Measurable,
 * чтобы у нее вызвать метод getName() класса Employee.
 */
public class Ex {
    public static void main(String[] args) {
        Employee emp1 = new Employee(15600, "1");
        Employee emp2 = new Employee(30000, "2");
        Employee emp3 = new Employee(25890, "3");
        Employee emp4 = new Employee(33880, "4");
        Measurable[] measurables = {emp1, emp2, emp3, emp4};

        System.out.println(average(measurables));
        System.out.println(((Employee)largest(measurables)).getName());
    }


    private static double average(Measurable[] objects) {
        int count = 0;
        double rezult = 0;

        for (Measurable measurable : objects) {
            rezult += measurable.getMeasure();
            count++;
        }
        return rezult / count;
    }

    private static Measurable largest(Measurable[] objects) {
        double maxWage = 0;
        Measurable ms = null;
        for (Measurable measurable : objects) {
            if (measurable.getMeasure() > maxWage) {
                maxWage = measurable.getMeasure();
                ms = measurable;
            }
        }
        return ms;
    }
}
