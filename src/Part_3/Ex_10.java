package Part_3;

public class Ex_10 {
    public static void main(String[] args) throws InterruptedException {
        Runnable [] runnables = init();
        runTogether(runnables);
        Thread.sleep(100);
        System.out.println();
        runInOrder(runnables);

    }

    public static void runTogether(Runnable... tasks) {
        for (Runnable runnable : tasks) {
            new Thread(runnable).start();
        }
    }

    public static void runInOrder(Runnable... tasks) {
        for (Runnable runnable : tasks) {
            runnable.run();
        }
    }

    public static Runnable[] init() {
        Runnable[] runnables = new Runnable[5];
        for (int i = 0; i < runnables.length; i++) {
            String is ="I runnable is " + (i+1);
            runnables[i] = () -> System.out.println(is);
        }
        return runnables;
    }
}
