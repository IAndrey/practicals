package Part_3.Ex_4;

import java.util.Arrays;

public interface IntSequence {
    static int[] of(int... numbers) {
        Arrays.sort(numbers);
        return numbers;
//Для доп задания :
//        int[] newMumbers = numbers;
//        return new IntSequence() {
//            public int[] number = newMumbers;
//        };
    }
}
