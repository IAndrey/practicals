package Part_3.Ex_6;

import java.math.BigInteger;

public class SquareSequence implements Sequence<BigInteger> {
    BigInteger i;

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public BigInteger next() {
        i.add(i);
        return i.pow(2);
    }
}
