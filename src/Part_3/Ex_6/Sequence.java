package Part_3.Ex_6;

public interface Sequence<T> {
    boolean hasNext();
    T next();
}
