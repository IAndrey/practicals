package Part_3.Ex_15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class Ex_15 {
    public static void main(String[] args) {
        Worker[] workers = init();

        Arrays.sort(workers, compareWorker());            //по возрастанию, сначала зп, потом имена.
        out(workers);
        Arrays.sort(workers, compareWorkerRevert());        //по убыванию, сначала зп, потом имена.
        out(workers);
    }

    private static Comparator<Worker> compareWorker() {
        Comparator<Worker> comp = Comparator.comparing(Worker::getWage);
        comp = comp.thenComparing(Worker::getName);
        return comp;
    }

    private static Comparator<Worker> compareWorkerRevert() {
        Comparator<Worker> comp = (worker1, worker2) -> (int) (worker2.getWage() - worker1.getWage());
        comp = comp.thenComparing((worker1, worker2) -> worker2.getName().compareTo(worker1.getName()));
        return comp;
    }

    private static Worker[] init() {
        ArrayList<Worker> workers = new ArrayList<>();
        Worker worker1 = new Worker("Vasia", 11000);
        Worker worker2 = new Worker("Petia", 11500);
        Worker worker3 = new Worker("Masha", 10000.20);
        Worker worker6 = new Worker("Masha", 19000.20);
        Worker worker4 = new Worker("Vasia", 13000.8);
        Worker worker5 = new Worker("Anton", 10000.20);
        workers.add(worker1);
        workers.add(worker2);
        workers.add(worker3);
        workers.add(worker4);
        workers.add(worker5);
        workers.add(worker6);
        return workers.toArray(new Worker[workers.size()]);
    }

    public static void out(Worker[] workers) {
        for (Worker worker :
                workers) {
            System.out.println(worker.getName() + " " + worker.getWage());
        }
        System.out.println();
    }
}
