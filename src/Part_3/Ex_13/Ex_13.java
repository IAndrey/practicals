package Part_3.Ex_13;

import java.io.File;
//Сделал сортировку в обратном порядке для наглядности, т.к.
// при добавлении файлов idea автоматически сортирует фалы по возрастанию
public class Ex_13 {
    public static void main(String[] args) {
        File [] files = new File("./src/Part_3/Ex_13/test folder/").listFiles();
        files = MyFileSorting.mySortFiles(files);
        out(files);
    }
    public static void out(File [] files) {
        for (File file :
                files) {
            System.out.println(file.getName());
        }
        System.out.println();
    }
}
