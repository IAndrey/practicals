package Part_3.Ex_13;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class MyFileSorting {
    private static File[] file;

    private static File[] getFolder() {
        ArrayList<File> folders = new ArrayList<>();
        for (File file :
                file) {
            if (file.isDirectory()) folders.add(file);
        }
        return folders.toArray(new File[folders.size()]);
    }

    private static File[] getFiles() {
        ArrayList<File> files = new ArrayList<>();
        for (File file :
                file) {
            if (!file.isDirectory()) files.add(file);
        }
        return files.toArray(new File[files.size()]);
    }

    private static void sort(File[] objectFile) {
//        Arrays.sort(objectFile, Comparator.comparing(File::getAbsoluteFile)); //сортировака по возрастанию
        Arrays.sort(objectFile, ((o1, o2) -> o2.getAbsoluteFile().compareTo(o1.getAbsoluteFile()))); // сортировка по убыванию
    }

    public static File[] mySortFiles(File[] files) {
        file = files;
        File[] folders = getFolder();
        sort(folders);
        File[] files1 = getFiles();
        sort(files1);
        ArrayList<File> rezult = new ArrayList<>();
        for (int i = 0; i < folders.length; i++) {
            rezult.add(folders[i]);
        }
        for (int i = 0; i < files1.length; i++) {
            rezult.add(files1[i]);
        }
        return rezult.toArray(new File[rezult.size()]);
    }
}
