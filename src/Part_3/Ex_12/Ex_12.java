package Part_3.Ex_12;

import java.io.File;

public class Ex_12 {
    public static void main(String[] args) {
        File[] filesInFolder = MyFiles.getSubdirectories("./src/Part_3/Ex_12/test folder/", ".txt");
        File[] filesInFolder2 = MyFiles.getSubdirectories("./src/Part_3/Ex_12/test folder/", ".docx");

        out(filesInFolder);
        out(filesInFolder2);
    }

    public static void out(File [] files) {
        for (File file :
                files) {
            System.out.println(file.getName());
        }
        System.out.println();
    }
}
