package Part_3.Ex_12;

import java.io.File;

public class MyFiles {
    private static File file;

    public static File[] getSubdirectories(String directory, String fileFormat) {
        file = new File(directory);
        File[] files = file.listFiles((file, name) ->{
            if (name.endsWith(fileFormat)) return true; // лямбдой захватывается fileFormat из общей области внутри метода.
            else return false;                          // Мы не можем присвоить ей зн. Т.к. если она испл внутри лямбды, она становится final полем.
        });
        return files;
    }
}
