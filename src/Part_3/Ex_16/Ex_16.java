package Part_3.Ex_16;

import java.util.Random;

public class Ex_16 {
    private static Random generator = new Random();

    public static void main(String[] args) {
        System.out.println(randomInts(10, 100).next());
    }

    public static IntSequence randomInts(int low, int hight) {
        return new RandomSequence(low, hight);
    }

    public static class RandomSequence implements IntSequence {
        private int low;
        private int hight;

        public RandomSequence(int low, int hight) {
            this.low = low;
            this.hight = hight;
        }

        @Override
        public int next() {
            return low + generator.nextInt(hight - low + 1);
        }
    }
}
