package Part_3.Ex_16;

public interface IntSequence {
    default boolean hasNext(){
        return true;
    }
    int next();
}
