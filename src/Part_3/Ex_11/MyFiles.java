package Part_3.Ex_11;

import java.io.File;
import java.io.FileFilter;

public class MyFiles {
    private static File file;

//    С помощью лямбды
    public static File[] getSubdirectoriesLambda(String directory) {
        file = new File(directory);
        File[] directoryes = file.listFiles(pathname -> pathname.isDirectory());
        return directoryes;
    }

    //    С помощью ссылки на метод
    public static File[] getSubdirectoriesMethodReference(String directory) {
        file = new File(directory);
        File[] directoryes = file.listFiles(File::isDirectory);
        return directoryes;
    }

    //    С помощью анонимного класса
    public static File[] getSubdirectoriesAnonimClass(String directory) {
        file = new File(directory);
        FileFilter filter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        };
        File[] directoryes = file.listFiles(filter);
        return directoryes;
    }
}
