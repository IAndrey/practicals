package Part_3.Ex_11;

import java.io.File;

public class Ex_11 {
    public static void main(String[] args) {
        File[] filesInFolder = MyFiles.getSubdirectoriesLambda("./src/Part_3/Ex_11/test folder/");
        File[] filesInFolder2 = MyFiles.getSubdirectoriesMethodReference("./src/Part_3/Ex_11/test folder/");
        File[] filesInFolder3 = MyFiles.getSubdirectoriesAnonimClass("./src/Part_3/Ex_11/test folder/");

        out(filesInFolder);
        out(filesInFolder2);
        out(filesInFolder3);

    }

    public static void out(File [] files) {
        for (File file :
                files) {
            System.out.println(file.getName());
        }
        System.out.println();
    }
}
