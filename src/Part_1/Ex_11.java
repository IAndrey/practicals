package Part_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ex_11 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        char[] chars = reader.readLine().toCharArray();
//        chars[0] = 1000;
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] > 255) string.append(chars[i]);
        }
        System.out.println(string);
    }
}
