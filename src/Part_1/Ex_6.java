package Part_1;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Ex_6 {
    public static void main(String[] args) {
        System.out.println(calculateFactorial(1000));
    }

    public static BigInteger calculateFactorial(int n){
        BigInteger result = new BigInteger("1");
        for (int i = 1; i <= n; i ++){
            result = result.multiply(new BigInteger(String.valueOf(i)));
        }
        return result;
    }
}
