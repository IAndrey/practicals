package Part_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ex_2 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println(nonFloorMod(Integer.parseInt(reader.readLine())));
//            System.out.println(Math.floorMod(Integer.parseInt(reader.readLine()), 360));
        }
    }

    public static String nonFloorMod(int input) {
        if (input == 360 || input == 0) {
            return "0";
        }
        if (input > 360) {
            return input % 360 + "";
        }
        if (input < 0) {
            return 360 + (input % 360) + "";
        }
        return input + "";
    }
}
