package Part_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.security.Signature;

public class Ex_4 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            double n = Double.parseDouble(reader.readLine());
            System.out.println(Math.nextDown(n));
            System.out.println(Math.nextUp(n));
//            double v = 2.0 - 1.8;
//            System.out.println(v);
        }
    }
}
