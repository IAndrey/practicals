package Part_1.Ex_14;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Ex_14 {
    public static void main(String[] args) throws Exception {

        List<int[]> values = InputReader.getInputArrayList();

        boolean magicKub = false;
        if (checkKub(values)) {
            int[][] kub = getKub(values);
            boolean checkSumLinesInKub = checkSumLinesInKub(values);
            boolean checkSumColumnsInKub = checkSumColumnsInKub(values);
            boolean checkSumDiagonalsInKub = checkSumDiagonalsInKub(values);

            if (checkSumLinesInKub && checkSumColumnsInKub && checkSumDiagonalsInKub) {
                magicKub = true;
            }
        }
        outKub(values);
        System.out.println("\n" + magicKub);
    }

    private static boolean checkSumDiagonalsInKub(List<int[]> values) {
        int summDiagonalOne = 0;
        int summDiagonalTwo = 0;

        int[] valluesColumn = new int[values.size()];

        for (int i = 0; i < values.size(); i++) {
            valluesColumn[i] = values.get(i)[i];
        }
        summDiagonalOne = sumValuesInKub(valluesColumn);

        int k = 0;
        for (int i = values.size() - 1; i >= 0; i--) {
            valluesColumn[k] = values.get(k)[i];
            k++;
        }
        summDiagonalTwo = sumValuesInKub(valluesColumn);

        if (summDiagonalOne != summDiagonalTwo) return false;
        else return true;
    }

    private static boolean checkSumColumnsInKub(List<int[]> values) {

        int summColumnOne = 0;
        int[] valluesColumn = new int[values.size()];

        for (int i = 0; i < values.size(); i++) {
            valluesColumn[i] = values.get(i)[0];
        }
        summColumnOne = sumValuesInKub(valluesColumn);

        for (int i = 1; i < values.size(); i++) {
            for (int j = 0; j < values.size(); j++) {
                valluesColumn[i] = values.get(j)[i];
            }
            if (summColumnOne != sumValuesInKub(valluesColumn)) return false;
            summColumnOne = sumValuesInKub(valluesColumn);
        }
        return true;
    }

    private static boolean checkSumLinesInKub(List<int[]> values) {
        int summLineOne = sumValuesInKub(values.get(0));
        for (int[] vall : values) {
            if (sumValuesInKub(vall) != summLineOne) {
                return false;
            }
        }
        return true;
    }

    private static int sumValuesInKub(int[] values) {
        int summLineOne = 0;
        for (int element : values) {
            summLineOne += element;
        }
        return summLineOne;
    }

    private static boolean checkKub(List<int[]> values) {
        int length = values.get(0).length;
        if (values.size() != length) return false;
        for (int[] massiv : values) {
            if (massiv.length != length) return false;
        }
        return true;
    }

    private static int[][] getKub(List<int[]> values) {
        int with = values.get(0).length;
        int hight = values.size();
        int[][] kub = new int[hight][with];
        for (int i = 0; i < hight; i++) {
            for (int k = 0; k < values.get(i).length; k++) {
                kub[i] = values.get(i);
            }
        }
        return kub;
    }

    private static void outKub(List<int[]> values) {
        int with = 0;
        int hight = values.size();
        for (int[] x : values) {
            if (x.length > with) with = x.length;
        }
        int[][] kub = new int[hight][with];
        for (int i = 0; i < hight; i++) {
            for (int k = 0; k < values.get(i).length; k++) {
                kub[i] = values.get(i);
            }
        }
        for (int i = 0; i < values.size(); i++) {
            for (int k = 0; k < values.get(i).length; k++) {
                System.out.print(kub[i][k]);
            }
            System.out.println();
        }
    }
}
