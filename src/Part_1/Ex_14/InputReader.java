package Part_1.Ex_14;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class InputReader {
    public static ArrayList<int[]> getInputArrayList () throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<int []> values = new ArrayList<>();
        while (true) {
            String str = reader.readLine();
            if (str.isEmpty()) break;
            String [] stringValues = str.split(" ");
            int []  value = new int[stringValues.length];
            for (int i = 0; i < value.length; i++) {
                if (isNumeric(stringValues[i])) {
                    value[i] = Integer.parseInt(stringValues[i]);
                } else {
                    throw new Exception("В строке должны быть числа через пробел");
                }
            }
            values.add(value);
        }
        return values;
    }

    private static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }
}
