package Part_1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Ex_13 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 49; i++) {
            list.add(i + 1);
        }
        int [] lottery = new int[6];
        for (int i = 0; i < 6; i++) {
            lottery[i] = list.get(getRandom(i+1));
            list.remove(i +1);
        }
        Arrays.sort(lottery);
        for (int lot : lottery) System.out.println(lot);
    }

    private static int getRandom(int index) {
        int x = (int) (Math.random() * 49 - index);
        return x;
    }
}
