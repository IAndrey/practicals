package Part_1;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Ex_1 {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String input = reader.readLine();
            if (!input.contains(".")) {
                int x = Integer.parseInt(input);
                System.out.println(toBinary(x));
//                System.out.println(Integer.toBinaryString(x));
                System.out.println(toOctal(x));
//                System.out.println(Integer.toOctalString(x));
                System.out.println(toHexCeil(x));
//                System.out.println(Integer.toHexString(x));
            } else {
                double x = Double.parseDouble(input);
                System.out.println(toHexDouble(x));
            }
        }
    }

    public static String toBinary(int value) {
        boolean check = false;
        if (value < 0) {
            value *= -1;
            check = true;
        }
        StringBuilder rezult = new StringBuilder();
        if (value <= 1) {
            rezult.append(value);
            return (check) ? rezult.append("-").reverse().toString() : rezult.reverse().toString();
        }
        while (value > 1) {
            rezult.append(value % 2);
            value /= 2;
            if (value == 1) rezult.append("1");
        }
        return (check) ? rezult.append("-").reverse().toString() : rezult.reverse().toString();
    }

    public static String toOctal(int value) {
        boolean check = false;
        if (value < 0) {
            value *= -1;
            check = true;
        }
        StringBuilder rezult = new StringBuilder();
        if (value < 8) {
            rezult.append(value);
            return (check) ? rezult.append("-").reverse().toString() : rezult.reverse().toString();
        }
        while (value >= 8) {
            rezult.append(value % 8);
            value /= 8;
            if (value < 8) rezult.append(value);
        }
        return (check) ? rezult.append("-").reverse().toString() : rezult.reverse().toString();
    }

    public static String toHexCeil(int value) throws Exception {
        boolean check = false;
        if (value < 0) {
            value *= -1;
            check = true;
        }
        StringBuilder rezult = new StringBuilder();
        if (value < 10) {
            rezult.append(value);
            return (check) ? rezult.append("-").reverse().toString() : rezult.reverse().toString();
        }
        if (value < 16) {
            for (int i = 0; i < 6; i++) {
                if (i + 10 == value) {
                    rezult.append(getChar(value));
                }
            }
            return (check) ? rezult.append("-").reverse().toString() : rezult.reverse().toString();
        }
        while (value > 15) {
            if (value % 16 >= 10) {
                rezult.append(getChar(value % 16));
            } else {
                rezult.append(value % 16);
            }
            value /= 16;
            if (value < 10) rezult.append(value);
            if (value < 16 && value >= 10) {
                rezult.append(getChar(value));
            }
        }
        return (check) ? rezult.append("-").reverse().toString() : rezult.reverse().toString();
    }

    public static String toHexDouble(double value) throws Exception {
        boolean check = false;
        if (value < 0) {
            value *= -1;
            check = true;
        }
        StringBuilder rezult = new StringBuilder(toHexCeil((int) value));
        rezult.append(",");
        for (int i = 0; i < 5; i++) {
            int x = (int) ((value - (int) value) * 16);
            rezult.append(toHexCeil(x));
            value = (value - (int) value) * 16 - x;
        }
        return (check) ? rezult.insert(0, "-").toString() : rezult.toString();
    }

    private static char getChar(int value) throws Exception {
        char[] chars = {'A', 'B', 'C', 'D', 'E', 'F'};
        for (int i = 0; i < 6; i++) {
            if (i + 10 == value) {
                return chars[i];
            }
        }
        throw new Exception("Значение должно быть от 10 до 15, значение - " + value);
    }
}
