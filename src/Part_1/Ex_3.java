package Part_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ex_3 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader (System.in));
        while (true) {
            int x = Integer.parseInt(reader.readLine());
            int y = Integer.parseInt(reader.readLine());
            int z = Integer.parseInt(reader.readLine());
            System.out.println(maxValue(x, y, z));
            System.out.println((Math.max(x , y) > z ? Math.max(x , y) : z));
        }
    }

    public static int maxValue (int x, int y, int z) {
        if (x >= y && x >= z) {
            return x;
        } else if (y >= x && y >= z) {
            return y;
        } else return z;
    }
}
