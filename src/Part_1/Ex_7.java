package Part_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ex_7 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            int x = Integer.parseInt(reader.readLine());
            int y = Integer.parseInt(reader.readLine());
            if (x >= 0 && y >= 0 && x <= 65535 && y <= 65535) {
                short one = (short) x;
                short two = (short) y;
                System.out.println((short) (one + two));
                System.out.println((short) (one - two));
                System.out.println((short) (one * two));
                System.out.println((short) (one / two));
                System.out.println((short) (one % two));
            }
        }
    }
}
