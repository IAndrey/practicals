package Part_1;

public class Ex_12 {
    public static void main(String[] args) {
        label:
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (j > i) {
                    System.out.println();
                    continue label;
                }
                System.out.print(" " + (i * j));
            }
        }
        System.out.println();
        nonLabels();
    }

    public static void nonLabels () {
//        label:
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (j > i) {
                    System.out.println();
                    break;
                }
                System.out.print(" " + (i * j));
            }
        }
        System.out.println();
    }
}

