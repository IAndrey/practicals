package Part_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Ex_15 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<ArrayList<Integer>> triangle;
        while (true) {
            int n = Integer.parseInt(reader.readLine());
            triangle = createTriangle(n);
            outTriangle(triangle);
        }
    }

    private static ArrayList<ArrayList<Integer>> createTriangle(int n) {
        if (n == 0) {
            ArrayList<ArrayList<Integer>> triangle = new ArrayList<>();
            ArrayList<Integer> line = new ArrayList<>();
            line.add(1);
            triangle.add(line);
            return triangle;
        }
        ArrayList<ArrayList<Integer>> triangle = new ArrayList<>();
        triangle.add(new ArrayList<Integer>());
        triangle.get(0).add(1);
        for (int i = 0; i < n; i++) {
            addNewLine(triangle);
        }
        return triangle;
    }

    private static void addNewLine(ArrayList<ArrayList<Integer>> triangle) {
        ArrayList<Integer> line = triangle.get(triangle.size() - 1);
        ArrayList<Integer> newLine = new ArrayList<>();
        newLine.add(line.get(0));
        for (int i = 0; i < line.size(); i++) {
            if (i == line.size() - 1) {
                newLine.add(line.get(i));
            } else {
                newLine.add(line.get(i) + line.get(i + 1));
            }
        }
        triangle.add(newLine);
    }

    private static void outTriangle(ArrayList<ArrayList<Integer>> triangle) {
        int with = triangle.get(triangle.size() - 1).size();
        for (int i = 0; i < triangle.size(); i++) {
            ArrayList<Integer> line = triangle.get(i);
            StringBuilder strLine = new StringBuilder();
            for (int k : line) {
                strLine.append(k + " ");
            }
            System.out.println(strLine.insert(0, getSpace(with - i - 1)));
        }
    }

    private static StringBuilder getSpace(int with) {
        StringBuilder spaces = new StringBuilder();
        for (int i = 0; i < with; i++) {
            spaces.append(" ");
        }
        return spaces;
    }

}
