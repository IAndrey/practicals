package Part_13.Ex_9;

import java.util.Locale;
import java.util.ResourceBundle;

/*
Попробуйте осуществить интернационализацию всех сообщений в одной из ваших прикладных
программ хотя бы на двух языках, используя комплекты ресурсов.
 */
public class Ex_9 {
    public static void main(String[] args) {
        ResourceBundle res = ResourceBundle.getBundle("messege", new Locale("en"));
        Car myCar = new Car(res.getString("color"), res.getString("maxSpeed"), res.getString("description"));
        System.out.println(myCar);

        res = ResourceBundle.getBundle("messege", new Locale("de"));
        Car myCar2 = new Car(res.getString("color"), res.getString("maxSpeed"), res.getString("description"));
        System.out.println(myCar2);
    }
}
