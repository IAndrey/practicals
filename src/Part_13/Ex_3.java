package Part_13;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

/*
В каких из региональных настроек вашей виртуальной машины JVM применяются те же
условные обозначения дат (в формате месяц/день/год), что и в Соединенных Штатах.
 */
public class Ex_3 {
    public static void main(String[] args) {
        LocalDate ldt = LocalDate.now();
        printLocal(ldt);
    }

    public static void printLocal(LocalDate ld) {
        for (Locale loc : Locale.getAvailableLocales()) {
            DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(loc);
            if (getTemplate(Locale.US, ld).equals(formatter.format(ld))) {
                System.out.println(formatter.format(ld) + " " + loc + " / " + loc.getDisplayCountry());
            }
        }
    }

    public static String getTemplate(Locale loc, LocalDate ld) {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(loc);
        return formatter.format(ld);
    }
}
