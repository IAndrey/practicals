package Part_13;

import java.nio.charset.Charset;
import java.util.Locale;

/*
Предоставьте механизм для представления имеющихся кодировок символов в удобочитаемом виде,
как это сделано в вашем браузере. Названия языков должны быть локализованы.(воспользуйтесь
переводами на местные языки).

В приведенном ниже коде показано, что все кодировки символов для всех языков
имеют свое обозначение на латинскими буквами и выводятся одинаково на любом языке.
 */
public class Ex_10 {
    public static void main(String[] args) {
//        Charset.availableCharsets().forEach((K,V)-> System.out.println(K + " / " + V));
        for (Charset charset : Charset.availableCharsets().values()) {
            for (Locale lang : Locale.getAvailableLocales()) {
                String dispName = charset.displayName(lang);
                System.out.print(dispName + " ");
            }
            System.out.println("/-----------------------/");
        }
    }
}
