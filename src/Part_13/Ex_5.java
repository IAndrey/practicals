package Part_13;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Currency;
import java.util.Locale;

/*
Повторите предыдущее упражнение для наименования денежных едениц.
 */
public class Ex_5 {
    public static void main(String[] args) {
        //вывожу список всех валют по очереди на каждом языке.
        for (String lang : Locale.getISOLanguages()) {
            Currency.getAvailableCurrencies().forEach(currency -> System.out.println(currency.getDisplayName(new Locale(lang))));
        }
    }
}
