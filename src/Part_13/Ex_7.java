package Part_13;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/*
Напишите программу, выводящую списком отображаемые и самостоятельные названия месяцев
во всех региональных настройках, где они отличаются, учитывая то, что самостоятельные названия
месяцев могут состоять из цифр.
 */
public class Ex_7 {
    public static void main(String[] args) {
//      Вывожу названия месяцев на всех языках, если нужны только уникальные названия то есть метод accumulate
//        for (String lang : Locale.getISOLanguages()) {
//            if (lang.isEmpty()) continue;
//            System.out.println(Locale.forLanguageTag(lang).getDisplayLanguage());
//            getMonth(new Locale(lang)).forEach(System.out::println);
//            System.out.println();
//            System.out.println();
//        }

        accumulate().forEach(System.out::println);

    }

    private static List<String> accumulate() {
        List<String> result = new ArrayList<>();
        for (String lang : Locale.getISOLanguages()) {
            result.addAll(getMonth(new Locale(lang)));
        }
        return result.stream().distinct().collect(Collectors.toList());
    }

    public static List<String> getMonth(Locale loc) {
        List<String> result = new ArrayList<>();
        LocalDate date = LocalDate.of(2019, 1, 10);
        do {
            result.add(date.getMonth().getDisplayName(TextStyle.FULL_STANDALONE, loc));
            date = date.plusMonths(1);
        } while (date.getYear() == 2019);
        return result;
    }
}
