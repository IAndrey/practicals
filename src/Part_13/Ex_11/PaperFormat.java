package Part_13.Ex_11;

import java.util.Locale;
import java.util.ResourceBundle;

public class PaperFormat {
    private Locale locale;
    ResourceBundle res;

    public PaperFormat(Locale locale) {
        this.locale = locale;
        res = ResourceBundle.getBundle("format", locale);
    }

    public String getFormat() {
        return res.getString("form");
    }
}
