package Part_13;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

/*
Напишите программу, демонстрирующую стили форматирования даты и времени во французском,
китайском и тайском представлениях.
 */
public class Ex_1 {
    public static void main(String[] args) {
//        System.out.println(Arrays.toString(Locale.getAvailableLocales())); -- выводим все возможные форматы
        System.out.println(getTimeFormat("fr"));
        System.out.println(getTimeFormat("zh"));
        System.out.println(getTimeFormat("TW"));
        System.out.println(getTimeFormat("ru"));
    }

    public static String getTimeFormat(String locale) {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withLocale(new Locale(locale));
        LocalDateTime ldt = LocalDateTime.now();
        return formatter.format(ldt);
    }
}
