package Part_13;

import java.util.Arrays;
import java.util.Locale;

/*
Напишите программу, выводящую названия всех языков из региональных настроек вашей виртуальной
машины JVM на всех имеющихся языках. Отсортируйте их, исключив дубликаты.
 */
public class Ex_4 {
    public static void main(String[] args) {
        for (String lang : Locale.getISOLanguages()) {
            Arrays.stream(Locale.getAvailableLocales()).map(locale -> locale.getDisplayLanguage(new Locale(lang))).sorted(String::compareTo).distinct().forEach(System.out::println);
            System.out.println("/------------------------/");
        }
    }
}
