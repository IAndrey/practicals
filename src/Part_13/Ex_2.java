package Part_13;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

/*
В каких из региональных настроек вашей виртуальной машины JVM не употребляются
арабские цифры для форматирования чисел.

Арабские цифры: 0	1	2	3	4	5	6	7	8	9;
 */
public class Ex_2 {
    public static void main(String[] args) {
        Number number = 123456789;
        for (Locale str : Locale.getAvailableLocales()) {
            String res = getNumber(str, number);
            if (!containsNumber(res)) {
                System.out.printf("%s - %s locale %s\n", number, res, str);
            }
        }
    }

    public static String getNumber(Locale loc, Number number) {
        NumberFormat format = NumberFormat.getNumberInstance(loc);
        return format.format(number);
    }

    private static boolean containsNumber(String str) {
        String[] numbers = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"};
        for (String s: numbers) {
            if (str.contains(s)) return true;
        }
        return false;
    }
}
