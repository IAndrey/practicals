package Part_13;

import java.util.Currency;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

/*
Напишите программу, выводящую списком все денежные еденицы, обозначаемые разными знаками
хотя бы в двух региональных настройках.
 */
public class Ex_6 {
    public static void main(String[] args) {
        getSymbol(Locale.JAPAN).forEach(System.out::println);
        System.out.println(getSymbol(Locale.JAPAN).contains("L."));
        System.out.println(getSymbol(Locale.ITALY).contains("L."));
    }

    public static Set<String> getSymbol(Locale loc) {
        return Currency.getAvailableCurrencies().stream().map(currency -> currency.getSymbol(loc)).collect(Collectors.toSet());
    }
}
