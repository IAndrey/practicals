package Part_5.Ex_15;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.logging.*;

public class Ex_15 {
    public static void main(String[] args) throws IOException {
        Logger logger = getMyLogger("my.logger", "./src/Part_5/Ex_15/text.html");
        logger.info("asas");


    }

    public static void setMyFormatter(Handler handler) {
        StringBuilder htmlFormat = new StringBuilder(
                "<html>\n" +
                        " <head>\n" +
                        "   <title>Logger</title>\n" +
                        "   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                        " </head> \n" +
                        " <body>\n");
        Formatter formatter = new Formatter() {
            @Override
            public String format(LogRecord record) {
                htmlFormat.append(" <h3>" + new Date(record.getMillis()) + "</h3>\n");
                htmlFormat.append(" <h5>" + record.getLoggerName() + "</h5>\n");
                htmlFormat.append(" <h5>" + record.getLevel() + "</h5>\n");
                htmlFormat.append(" <h5>" + record.getMessage() + "</h5>\n");
                htmlFormat.append(" </body>\n");
                htmlFormat.append("</html>");
                return htmlFormat.toString();
            }
        };
        handler.setFormatter(formatter);

    }

    public static Logger getMyLogger(String nameLogger, String filePath) throws IOException {
        File file = new File(filePath);
        file.createNewFile();
        Logger logger = Logger.getLogger(nameLogger);
        logger.setLevel(Level.ALL);
        logger.setUseParentHandlers(false);
        Handler handler = new FileHandler(filePath);
        handler.setLevel(Level.ALL);
        setMyFormatter(handler);
        logger.addHandler(handler);
        return logger;
    }
}
