package Part_5.Ex_9;

import java.util.concurrent.locks.ReentrantLock;

public class MySupportingClass implements AutoCloseable {
    ReentrantLock lock;

    public MySupportingClass(ReentrantLock lock) {
        this.lock = lock;
    }

    @Override
    public void close() {
        lock.unlock();
    }
}
