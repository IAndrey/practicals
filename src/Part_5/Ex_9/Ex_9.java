package Part_5.Ex_9;

import java.io.*;
import java.util.concurrent.locks.ReentrantLock;

/*
Разработайте вспомогательный метод, чтобы воспользоваться классом ReentrantLook в операторе try
с ресурсами. Вызовите метод lock() и возвратите объект класса, который реализует интерфейс AutoCloseable
и в методе которого close() вызывается метод unlock(), но не генерируется никаких исключений.
 */
public class Ex_9 {
    static ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) {
        Thread th1 = new Thread(Ex_9::myMethod);
        th1.start();
        Thread th2 = new Thread(Ex_9::myMethod);
        th2.start();
    }

    public static MySupportingClass myMethod() {
        lock.lock();
        try (MySupportingClass myClass = new MySupportingClass(lock)) {
            for (int i = 0; i < 5; i++) {
                System.out.printf("%s { i=%s }\n", Thread.currentThread().getName(), i);
            }
            return myClass;
        }
    }
}
