package Part_5;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Scanner;

/*
Реализуйте метод, содержащий код, где применяются классы Scanner и PrintWriter.
Но вместо оператора  try c ресурсами, воспользуйтесь оператором catch. Непременно закройте
оба объекта.
 */
public class Ex_5 {
    public static void main(String[] args) {
        System.out.println(myMethod(System.in));
    }

    public static String myMethod(InputStream stream) {
        StringWriter str = new StringWriter();
        Scanner scan = new Scanner(stream);
        PrintWriter writer = new PrintWriter(str);
        try {
            while (true) {
                String line = scan.nextLine();
                if (line.equals("Exit")) break;
                writer.println(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            scan.close();
            writer.close();
            try {
                str.close();
            } catch (IOException e) {
                str = null; // обнуляем ссылку для закрытия потока, в случае исключения.
                e.printStackTrace();
            }

        }
        return str.toString();
    }
}
