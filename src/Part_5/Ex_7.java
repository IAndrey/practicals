package Part_5;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Scanner;

/*
Поясните почему следующий фрагмент кода:
try (Scanner in = new Scanner (Paths.get("/usr/share/dict/words"));
    PrintWriter out = new PrintWriter(outputFile)) {
        while (in.hasNext())
            out.println(in.next().toLowerCase());
}
Оказывается лучше, чем приведенный ниже.
Scanner in = new Scanner (Paths.get("/usr/share/dict/words"));
PrintWriter out = new PrintWriter(outputFile);
try (in; out) {
    while (in.hasNext())
        out.printLn (in.next().toLowerCase());
}

Первый фрагмент кода лучше, так как в строке
PrintWriter out = new PrintWriter(outputFile);
может возикнуть исключение IOException. И в таком случае поток in не будет закрыт.
 */
public class Ex_7 {
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(Paths.get("/usr/share/dict/words"));
        PrintWriter out = new PrintWriter("outputFile");
        try (in; out) {
            while (in.hasNext())
                out.println(in.next().toLowerCase());
        }
    }

}
