package Part_5;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

/*
Напишите рекуривный метод factorial(), выводящий все кадры стека
перед возвратом значения. Постройте но не генерируйте объект исключения любого типа
и получите результат трассировки его стека, как пояснялось в разделе 5.1.8
 */
public class Ex_11 {
    public static void main(String[] args) {
//        int x =factorial(4);
        System.out.println(method2());
    }

    public static int factorial(int n) {
        int result = 1;
        if (n == 1 || n == 0) return 1;
        result = n * factorial(n-1);
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        for (StackTraceElement element : elements) {
            System.out.println(element.getMethodName());
        }
        return result;
    }

    public static String method2() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        IOException exception = new IOException();
        exception.printStackTrace(new PrintStream(out));
        return out.toString();
    }
}
