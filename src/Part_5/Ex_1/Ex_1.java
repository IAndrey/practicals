package Part_5.Ex_1;

import java.io.*;
import java.util.ArrayList;

/*
Напишите метод, считывающий числа с плавающей точкой из файла.
Сгенереруйте исключения, если файл не удается открыть или, если
введены данные не относящиеся к числам с плавающей точкой.
 */
public class Ex_1 {
    public static void main(String[] args) {
        try {
            readValues("./src/Part_5/Ex_1/text.txt").forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Double> readValues(String filename) throws IOException {
        ArrayList<Double> doubles = new ArrayList<>();
        Reader fileReader = new FileReader(filename); // если нет указанного файла, прокидываем FileNotFoundException - наследника класса IOException.
        BufferedReader readLine = new BufferedReader(fileReader);
        String line;
        while ((line = readLine.readLine()) != null) {
            String[] doubls = line.split(" ");
            for (String doubl : doubls) {
                try {
                    doubles.add(Double.parseDouble(doubl)); // если формат числа не Double, то отлавливаем ошибку и выводим в консоль, считывание не приостанавливается.
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }
        return doubles;
    }
}
