package Part_5;

import Part_5.Ex_1.Ex_1;

import java.util.Arrays;
/*
Напишите метод, вызывающий метод из первого упр. и возвращающий сумму зн. в файле.
Организуйте распространение любых исключений вызывающему коду.
 */
public class Ex_2 {
    public static void main(String[] args) {
        try {
            System.out.println(sumOfValues("./src/Part_5/Ex_1/text.txt")); // Здесь не нужно выводить, след задание - вывести.
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static double sumOfValues(String fileName) throws Exception {  //распространение любых исключений вызывающему коду.
        double summ = 0;
        for (double doubl : Ex_1.readValues(fileName)) {
            summ +=doubl;
        }
        return summ;
    }
}
