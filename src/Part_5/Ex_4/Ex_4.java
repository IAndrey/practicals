package Part_5.Ex_4;

import java.io.*;
import java.util.ArrayList;
/*
Повторите упр. 1,2,3 но на этот раз не пользуйтесь исключениями.
Вместо этого организуйте возврат кодов ошибок из методов readValues() и sumOfValues().

Коды возврата. Основная идея — в случае ошибки возвращать специальное значение, которое не может быть корректным.
 */
public class Ex_4 {
    public static void main(String[] args) {
        System.out.println(sumOfValues("./src/Part_5/Ex_4/text.txt"));
    }

    public static ArrayList<Double> readValues(String filename) {
        ArrayList<Double> doubles = new ArrayList<>();
        Reader fileReader = null;
        try {
            fileReader = new FileReader(filename);
        } catch (FileNotFoundException e) {
            return null;
        }
        BufferedReader readLine = new BufferedReader(fileReader);
        String line;
        while (true) {
            try {
                if (((line = readLine.readLine()) == null)) break;
            } catch (IOException e) {
                return null;
            }
            String[] doubls = line.split(" ");
            for (String doubl : doubls) {
                try {
                    doubles.add(Double.parseDouble(doubl));
                } catch (NumberFormatException e) {

                }
            }
        }
        return doubles;
    }

    public static double sumOfValues(String fileName) {
        if (readValues(fileName) == null) return -1;
        double summ = 0;
        for (double doubl : readValues(fileName)) {
            summ += doubl;
        }
        return summ;
    }
}
