package Part_5.Ex_6;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

/*
Ниже приведен пример ошибочного внедрения блока операторов catch и finally
в блок кода оператора try. Исправьте этот код, во первых перехватив исключение
в операторе finally, во-вторых, внедрив блок операторов try/finally в блок операторов try/catch, и в третьих,
применив оператор try с ресурсами вместе с оператором catch.

        Path path = Path.of("./src/Part_5/Ex_6/Text.txt");
        BufferedReader in = null;
        try {
            in = Files.newBufferedReader(path, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            System.err.println("Caught IOException: " + ex.getMessage());
        } finally {
            if (in != null) {
                in.close();//Внимание: может быть сген исключение
            }
        }
 */
public class Ex_6 {
    public static void main(String[] args) {
        myMethodOne("./src/Part_5/Ex_6/Text1.txt");
    }

    public static void myMethodOne(String str) {
        Path path = Path.of(str);
        BufferedReader in = null;
        try {
            in = Files.newBufferedReader(path, StandardCharsets.UTF_8);
            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException ex) {
            System.err.println("Caught IOException: " + ex.getMessage());
        } finally {
            if (in != null) {
                try {
                    in.close();//Внимание: может быть сген исключение
                } catch (IOException e) {
                    in = null;
                    e.printStackTrace();
                }

            }
        }
    }

    public static void myMethodTwo(String str) {
        Path path = Path.of(str);
        BufferedReader in = null;
        try {
            try {
                in = Files.newBufferedReader(path, StandardCharsets.UTF_8);
                String line;
                while ((line = in.readLine()) != null) {
                    System.out.println(line);
                }
            } catch (IOException ex) {
                System.err.println("Caught IOException: " + ex.getMessage());
            } finally {
                if (in != null) {
                    in.close();//Внимание: может быть сген исключение
                }
            }
        } catch (IOException e) {
            in = null;
            e.printStackTrace();
        }
    }

    public static void myMethodThree(String str) {
        Path path = Path.of(str);
        try (BufferedReader in = Files.newBufferedReader(path, StandardCharsets.UTF_8)){
            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException ex) {
            System.err.println("Caught IOException: " + ex.getMessage());
        }
    }
}
