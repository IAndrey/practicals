package Part_5;
/*
Напишите программу, вызывающую метод из 2 упражнения и выводящую полученный результат.
Организуйте перехват исключений и предоставьте ответную реакцию на действия пользователя
в виде сообщений о любых ошибочных условиях.
 */
public class Ex_3 {
    public static void main(String[] args) {
        try {
            System.out.println(Ex_2.sumOfValues("./src/Part_5/Ex_1/text.txt"));

        } catch (Exception e) {
            e.printStackTrace(); // Выводим сообщение об ошибках.
        }
    }
}
