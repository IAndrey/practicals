package Part_5;

import java.util.Objects;

/*
Сравните вызов Objects.requireNonNull(obj) с утверждением assert obj != null.
Приведите убедительные примеры применения  того и другого.

"Для того что бы работал assert, необходимо добавить в параметр запуска -ea в run/debug configuration.
Так как по дефолту assert отключен."

Метод requireNonNull() является дженериком, соот-но я могу указать какого типа он может принимать объекты,
к примеру Objects.<String>requireNonNull(str), в данном случ я могу закинуть в метод только объекты принадлежащие
классу String. Данный метод придназначен только для проверки ненулевой ссылки, т.е. если ссылка null, то будет
сгенерировано исключение NullPointerException. Соответ-но если мне нужно проверить что ссылка не ссылается на null,
то лучше воспользоваться методом requireNonNull(obj).

assert предназначен для проверки условия, в случае не выполнения которого будет выброшено исключение AssertionError.
assert не может указать конкретный тип исключения, он только показывает что в определенной строке кода не выполнено
условие и генерирует ошибку AssertionError. Соответственно если мне нужно акцентировать внимание на определенном месте
в коде то можно воспользоваться ключ словом assert.
 */
public class Ex_12 {
    public static void main(String[] args) {
        String str = null;
//        int x = 10;
//        Object o = Objects.<String>requireNonNull(str);

        try {
            assert str != null;
//            assert  x < 5;
        } catch (AssertionError e) {
            e.printStackTrace();
        }

        System.out.println(1);

    }
}
