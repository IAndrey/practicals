package Part_12.Ex_12;

import java.time.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskEngine {
    private List<Task> tasks;
    private String currentTimeZone;

    public TaskEngine(String currentTimeZone) {
        this.currentTimeZone = currentTimeZone;
        tasks = new ArrayList<>();
    }

    public void addTask(Task... task) {
        tasks.addAll(Arrays.asList(task));
    }

    //Добавил чтобы брать год, месяц, день - сегодняшней даты.
    public List<Task> getTasks(LocalTime date, LocalTime timeInterval) {
        LocalDateTime LDT = LocalDateTime.now();
        return getTasks(LocalDateTime.of(LDT.getYear(), LDT.getMonth().getValue(), LDT.getDayOfMonth(), date.getHour(), date.getMinute()), timeInterval);
    }

    public List<Task> getTasks(LocalDateTime date, LocalTime timeInterval) {
        ZonedDateTime startTime = ZonedDateTime.of(date, ZoneId.of(currentTimeZone));
        ZonedDateTime endTime = ZonedDateTime.of(date.plusHours(timeInterval.getHour())
                .plusMinutes(timeInterval.getMinute()), ZoneId.of(currentTimeZone));
        return getCheckedTask(startTime, endTime);
    }

    private List<Task> getCheckedTask(ZonedDateTime startTime, ZonedDateTime endTime) {
        List<Task> result = new ArrayList<>();
        for (Task task : tasks) {
            if (checkTask(task, startTime, endTime)) result.add(task);
        }
        return result;
    }

//    Проверяем стартовое время таски, если оно подходит под интервал времени, добавляем ее.
    private boolean checkTask(Task task, ZonedDateTime startTime, ZonedDateTime endTime) {
        if (!task.getStartTime().isAfter(startTime) && !task.getStartTime().equals(startTime)) return false;
        if (!task.getStartTime().isBefore(endTime)) return false;
        return true;
    }
}
