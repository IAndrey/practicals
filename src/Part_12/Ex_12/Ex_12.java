package Part_12.Ex_12;

import java.time.*;
import java.util.List;

/*
Напишите программу, разрешающую затруднение, описанное в начале раздела 12.5. Проанализируйте ряд
назначенных мероприятий в разных часовых поясах и придупредите пользователя о тех из них,
которые должны состояться в течение следующего часа по местному времени.
 */
public class Ex_12 {
    public static void main(String[] args) {
        Task task1 = new Task(2019, 10, 18, 11, 0, 1, 0, "America/Belem");
        Task task2 = new Task(12, 30, 1, 0, "America/Belem");
        Task task3 = new Task(12, 0, 1, 0, "America/Belem");
        Task task4 = new Task(18, 13, 0, 1, 0, "America/Belem");
        TaskEngine myEngine = new TaskEngine("America/Belem");
        myEngine.addTask(task1, task2, task3, task4);
        List<Task> tasks = myEngine.getTasks(LocalTime.of(12, 0), LocalTime.of(1, 0));
        tasks.forEach(t -> System.out.println(t.getStartTime().getHour() + ":" + t.getStartTime().getMinute() + " " + t.getTitle()));
    }
}
