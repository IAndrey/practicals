package Part_12.Ex_12;

import java.time.*;
/*
Добавил в класс конструкторы, что бы объекты можно было создавать не указыва:
год, месяц, день. Если они не указаны, берутся текущие даты.
 */

public class Task {
    private String title = "No title";
    private String description = "No description";
    private ZonedDateTime startTime;
    private ZonedDateTime endTime;

    public Task(int year, int month, int day, int hour, int minute, int timeHour, int timeMinute, String timeZone, String... description) {
        checkDates(month, day, hour, minute, timeHour, timeMinute);
        startTime = ZonedDateTime.of(year, month, day, hour, minute, 0, 0, ZoneId.of(timeZone));
        init(timeHour, timeMinute, description);
    }

    public Task(int day, int hour, int minute, int timeHour, int timeMinute, String timeZone, String... description) {
        checkDates(day, hour, minute, timeHour, timeMinute);
        ZonedDateTime currentNow = ZonedDateTime.now(ZoneId.of(timeZone));
        startTime = ZonedDateTime.of(currentNow.getYear(), currentNow.getMonth().getValue(), day, hour, minute, 0, 0, ZoneId.of(timeZone));
        init(timeHour, timeMinute, description);
    }

    public Task(int hour, int minute, int timeHour, int timeMinute, String timeZone, String... description) {
        checkDates(hour, minute, timeHour, timeMinute);
        ZonedDateTime currentNow = ZonedDateTime.now(ZoneId.of(timeZone));
        startTime = ZonedDateTime.of(currentNow.getYear(), currentNow.getMonth().getValue(), currentNow.getDayOfMonth(), hour, minute, 0, 0, ZoneId.of(timeZone));
        init(timeHour, timeMinute, description);
    }

    private void init(int timeHour, int timeMinute, String... description) {
        endTime = startTime.plusHours(timeHour).plusMinutes(timeMinute);
        this.title = (description.length > 1) ? description[0] : "No title";
        this.description = (description.length > 1) ? description[1] : "No description";
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

//    Проверка корректности дат.
    private void checkDates(int month, int day, int hour, int minute, int timeHour, int timeMinute) {
        if (month > 12 || month < 1) throw new RuntimeException("некорректно - " + month);
        if (day > 31 || day < 1) throw new RuntimeException("некорректно - " + day);
        if (hour > 24 || hour < 0) throw new RuntimeException("некорректно - " + hour);
        if (minute > 60 || minute < 0) throw new RuntimeException("некорректно - " + minute);
        if (timeHour > 24 || timeHour < 0) throw new RuntimeException("некорректно - " + timeHour);
        if (timeMinute > 60 || timeMinute < 0) throw new RuntimeException("некорректно - " + timeMinute);
    }

    private void checkDates(int day, int hour, int minute, int timeHour, int timeMinute) {
        checkDates(2, day, hour, minute, timeHour, timeMinute);
    }

    private void checkDates(int hour, int minute, int timeHour, int timeMinute) {
        checkDates(2, 2, hour, minute, timeHour, timeMinute);
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
