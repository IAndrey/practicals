package Part_12;

import java.time.DayOfWeek;
import java.time.LocalDate;

/*
Выведите перечень всех пятниц, которые пришлись на 13-е число в XX веке.
 */
public class Ex_6 {
    public static void main(String[] args) {
        LocalDate ins = LocalDate.of(1900, 1, 13);
        printFriday(ins);
    }

    private static void printFriday(LocalDate date) {
        while (date.getYear() < 2000) {
            if (date.getDayOfWeek().equals(DayOfWeek.FRIDAY)) System.out.println(date);
            date = date.plusMonths(1);
        }
    }
}
