package Part_12;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.function.Predicate;

/*
Реализуйте метод next(), принимающий в качестве параметра местную дату типа
Predicate<LocalDate> и возвращающий корректор дат, который получает следующую
дату выполнения предиката. Например, в результате приведенного ниже вызова
вычисляется следующий рабочий день.
today.with(next(w -> getDayOfWeek().getValue() < 6))
 */
public class Ex_3 {
    public static void main(String[] args) {

        LocalDate date = LocalDate.now();
        for (int i = 0; i < 10; i++) {
            date = date.with(next(w -> w.getDayOfWeek().getValue() < 6));
            System.out.println(date);
        }


    }

    public static TemporalAdjuster next(Predicate<LocalDate> datePredicate) {
        return temporal -> {
            LocalDate result = (LocalDate) temporal;
            result = result.plusDays(1);
            if (datePredicate.test(result)) {
                return result;
            } else { // если условие не выполняется то нужно пропустить выходные.
                if (result.getDayOfWeek().getValue() == 6) return result.plusDays(2);
                return result.plusDays(1);
            }
        };
    }
}
