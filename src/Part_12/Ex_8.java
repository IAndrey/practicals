package Part_12;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/*
Получите смещения текущей даты во всех поддерживаемых часовых поясах для
текущего момента времени, преобразовав результат вызова метода ZoneId.getAvailableZoneIds()
в поток данных и используя потоковые операции.
 */
public class Ex_8 {
    public static void main(String[] args) {
        ZoneId.getAvailableZoneIds().forEach(t -> System.out.println(ZonedDateTime.of(LocalDateTime.now(), ZoneId.of(t))));
    }
}
