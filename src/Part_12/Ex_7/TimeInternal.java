package Part_12.Ex_7;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TimeInternal {
    private List<LocalDateTime> inters;

    public TimeInternal() {
        inters = new ArrayList<>();
    }

    public void setDate(int day, int startHour, int startMinute, int endHour, int endMinute) {
        if (intersection(day,startHour,startMinute, endHour, endMinute)) throw new RuntimeException("Время уже занято");
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime start = LocalDateTime.of(now.getYear(), now.getMonth().getValue(), day, startHour, startMinute);
        LocalDateTime end = LocalDateTime.of(now.getYear(), now.getMonth().getValue(), day, endHour, endMinute);
        writeSet(start, end);
    }

//    Вернет true если есть пересечение
    public boolean intersection(int day, int startHour, int startMinute, int endHour, int endMinute) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime start = LocalDateTime.of(now.getYear(), now.getMonth().getValue(), day, startHour, startMinute);
        LocalDateTime end = LocalDateTime.of(now.getYear(), now.getMonth().getValue(), day, endHour, endMinute);
        while (true) {
            if (end.equals(start)) break;
            if (inters.contains(start)) return true;
            start = start.plusMinutes(1);
        }
        return false;
    }

    private void writeSet(LocalDateTime start, LocalDateTime end) {
        while (true) {
            if (end.equals(start)) break;
            inters.add(start);
            start = start.plusMinutes(1);
        }
    }


}
