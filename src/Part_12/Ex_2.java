package Part_12;

import java.time.LocalDate;

/*
Что произойдет, если добавить один год, четыре года, четыре раза по одному году
при вызове метода LocalDate.of(2000, 2, 29)?

Каждый раз при вызове метода plusYears(Long) нам возвращается новый объект класса
LocalDate с новым значением года. Класс LocalDate является неизмениемым.
 */
public class Ex_2 {
    public static void main(String[] args) {
        LocalDate date = LocalDate.of(2000, 2, 29);
        LocalDate newDate = date.plusYears(1);
    }
}
