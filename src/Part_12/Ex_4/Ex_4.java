package Part_12.Ex_4;

import java.time.LocalDate;
import java.util.Comparator;

/*
Напишите программу, выполняющую такие же функции, как и команда cal в Unix,
выводящая календарь на текущий месяц. Например, по команде java Cal 3 2013
должен быть выведен календарь на март 2013 года, где 1 марта приходится на пятницу,
как показано ниже. (Обозначьте каким-нибудь образом в календаре конец каждой недели.)
 */
public class Ex_4 {
    public static void main(String[] args) {
        Calendar calendar = new Calendar(2019, 10);
        calendar.printDays();
    }

}
