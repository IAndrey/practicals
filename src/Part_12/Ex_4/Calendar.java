package Part_12.Ex_4;

import java.time.LocalDate;

public class Calendar {
    private int space = 6;
    private int month;
    private LocalDate date;

    public Calendar(int year, int month) {
        date = LocalDate.of(year, month, 1);
        this.month = month;
    }

    public Calendar() {
        date = LocalDate.now();
        date = LocalDate.of(date.getYear(), date.getMonth(), 1);
        month = date.getMonth().getValue();
    }

    private void printFirstDayMonth() {
        System.out.print(" ");
        System.out.printf(("%" + space * (date.getDayOfWeek().getValue() - 1) + "s"), date.getDayOfMonth());
        System.out.printf("%-" + (space - 1) + "s", " ");
        if (date.getDayOfWeek().getValue() == 7) System.out.println();
    }

    private void print() {
        System.out.printf("%-" + space + "s", date.getDayOfMonth());
    }


    public void printDays() {
        printFirstDayMonth();
        while (true) {
            date = date.plusDays(1);
            if (date.getMonth().getValue() != month) break;
            if (date.getDayOfWeek().getValue() == 7) {
                print();
                System.out.println();
            } else {
                print();
            }
        }
    }

}
