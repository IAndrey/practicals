package Part_12;

import java.time.LocalDate;

/*
Рассчитайте дату, на которую приходится День программиста, не пользуясь методом plusDays().
 */
public class Ex_1 {
    public static void main(String[] args) {
        LocalDate programmerDate = LocalDate.ofYearDay(2019, 256);
        System.out.println(programmerDate.getMonth() + " " + programmerDate.getDayOfMonth() + " " + programmerDate.getDayOfWeek());
    }
}
