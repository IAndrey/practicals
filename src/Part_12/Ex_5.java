package Part_12;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;

/*
Напишите программу, выводящую кол-во дней, которые вы прожили.
 */
public class Ex_5 {
    public static void main(String[] args) {
        LocalDate myDay = LocalDate.of(1993, 10, 12);
        LocalDate fatherDay = LocalDate.of(1963, 12, 23);
        LocalDate matherDay = LocalDate.of(1965, 2, 11);
        LocalDate dotherDay = LocalDate.of(2016, 11, 15);

        System.out.println(getDays(myDay));
        System.out.println(getDays(fatherDay));
        System.out.println(getDays(matherDay));
        System.out.println(getDays(dotherDay));
    }

    public static Long getDays(LocalDate happyBirthday) {
        LocalDate currentDate = LocalDate.now();
        return DAYS.between(happyBirthday, currentDate);
    }
}
