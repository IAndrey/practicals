package Part_12;

import java.time.*;

/*
Используя снова потоковые операции, выявите все часовые пояса, смещения которых не кратны полному часу.
 */
public class Ex_9 {
    public static void main(String[] args) {
        LocalDateTime localDateTime = LocalDateTime.now();
        ZoneId.getAvailableZoneIds().stream().filter(zone ->
                checkHour(ZonedDateTime.of(localDateTime, ZoneId.of(zone)).getOffset().toString())).forEach(t ->
                System.out.println(ZonedDateTime.of(localDateTime, ZoneId.of(t)).getOffset() + " " + t));

    }

    private static boolean checkHour(String time) {
        if (time.equals("Z")) return false;
        if (time.substring(4).equals("00")) return false;
        return true;
    }
}
