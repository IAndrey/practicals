package Part_12.Ex_10_11;

import java.time.LocalTime;

/*
Самолет авиарейсом из Лос-Анджелеса во Франкфурт вылетает в 3:05 по полудни местного
времени и находится в полете 10 часов 50 минут. Когда он прибывает во Франкфурт?
Напишите программу способную выполнять подобные расчеты времени.
 */
public class Ex_10 {
    public static void main(String[] args) {
        ClockEngine engine = new ClockEngine();
        engine.finishCityTime("Лос-Анджелес", LocalTime.of(3, 5), "Франкфурт", LocalTime.of(10, 50));
    }
}
