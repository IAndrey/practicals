package Part_12.Ex_10_11;

import java.time.*;
import java.util.HashMap;
import java.util.Map;

public class ClockEngine {
    private Map<String, String> cityZone;

    public ClockEngine() {
        cityZone = new HashMap<>();
        cityZone.put("Лос-Анджелес", "America/Los_Angeles");
        cityZone.put("Франкфурт", "Europe/Monaco");
    }

    public void addCityZone(String city, String zone) {
        cityZone.put(city, zone);
    }

//    3 параметр - город назначения, 4 - время в пути.
    public void finishCityTime(String startCity, LocalTime startTime, String finishCity, LocalTime timeTravel) {
        if (!cityZone.containsKey(finishCity) || !cityZone.containsKey(startCity)) {
            throw new RuntimeException("Пожалуйста укажите временную в пункте методом addCityZone");
        }
        ZonedDateTime startZone = ZonedDateTime.of(LocalDate.now(), startTime, ZoneId.of(cityZone.get(startCity)));
        ZonedDateTime finishTime = startZone.plusHours(timeTravel.getHour()).
                plusMinutes(timeTravel.getMinute()).withZoneSameInstant(ZoneId.of(cityZone.get(finishCity)));
        System.out.println("Вы прибудете в место назначения " + finishCity + " в " + finishTime.getHour() + " " + finishTime.getMinute() + " по местному времени");
    }

    public void timeTravel(String startCity, LocalTime startTime, String finishCity, LocalTime finisTime) {
        if (!cityZone.containsKey(finishCity) || !cityZone.containsKey(startCity)) {
            throw new RuntimeException("Пожалуйста укажите временную в пункте методом addCityZone");
        }
        LocalDate lD = LocalDate.now();
        ZonedDateTime startZone = ZonedDateTime.of(lD, startTime, ZoneId.of(cityZone.get(startCity)));
        ZonedDateTime finishTime = ZonedDateTime.of(lD, finisTime, ZoneId.of(cityZone.get(finishCity)));
        Long hours = Duration.between(startZone, finishTime).toHours();
        System.out.println("Ваш путь из " + startCity + " в " + finishCity + " будет длиться " + hours + " часов");
    }
}
