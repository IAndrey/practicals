package Part_12.Ex_10_11;

import java.time.LocalTime;

/*
Самолет обратным авиарейсом из Франкфурта вылетает в 14:05 и прибывает в
Лос-Анджелес в 16:40. Сколько времени длится полет? Напишите программу,
способную выполнять подобные расчеты времени.
 */
public class Ex_11 {
    public static void main(String[] args) {
        ClockEngine engine = new ClockEngine();
        engine.timeTravel("Франкфурт", LocalTime.of(14, 5), "Лос-Анджелес", LocalTime.of(16, 40));
    }
}
