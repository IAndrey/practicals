package MyPractics.Part_11_Annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
public @interface TestAnotat {
    int timeOut() default 1000;
}
