package MyPractics.Part_11_Annotations;
/*
Анотации могут иметь элемнты, которые перечисляются через запятую.
Елементы - это пары ключ, значение.

@BugReport (key = value,
            key2 = value,
            ... = ...)

В качестве элементов могут быть массивы:
@BugReport (key = {"value1", "value2"})

Несколько анатаций могу быть применены к одному элементу кода:

    @BugReport (key = value,
            key2 = value,
            ... = ...)
    @Deprecated
   public void checkTest() {

    }

Анотации могу применяться к классам, методлам, конструкторам, переменным экземпляра,
локальным переменным, переменным параметров и параметры оператора catch, параметры типа, пакеты.

   public void checkTest(@Anotation int x, ...) {

    }

    publick class Cache<@Immutable V>

 */
public class Annotation {
   @Deprecated
   public void checkTest() {

    }

}
