package MyPractics.Part_9_InputStream_OutPutStream_Reader_Writer;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.nio.channels.FileLock;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

/*
Заметки:
Для удобного чтения всех байт из файла есть метод Files.readAllBytes
byte[] bytes = Files.readAllBytes(path).

Для удобного чтения всех строк из файла есть метод Files.readAllLines
List<String> strings = Files.readAllLines(Paths.get("s"));

Чтобы считать весь файл в строку достаточно написать:
String content = new String(Files.readAllBytes(path), charset);

Чтобы записать строки в поток Stream, достаточно:
Stream<String> lines = Files.lines (path, charset);

Для чтения чесил или слов, лучше пользоваться классом Scanner.
С помощью Scanner можно разбить всю строку на слова:
Scanner in...
in.useDelimiter("\\PL+);
И создать поток слов Stream:
Stream<String> words = in.tokens();  !!!!!

Для вывода текста намного удобнее пользоваться классом PrintWriter,
где имеются методы print(), println(), printf():
PrintWriter out = new PrintWriter(OutputStream, "UTF-8");

Или воспользоваться методом write класса Files. Files.write().
Для добавления трок в файл в метод Files.write() необходимо закинуть
StandardOpenOption.APPEND - Files.write( ,..., StandardOpenOption.APPEND)

Для записи в файл в определенном месте используется класс RandomAccessFile
с помощью метода seek(), можно установить указатель в файле.
 */
public class Read_Write {
    public static void main(String[] args) throws IOException {

    }
}
