package MyPractics.Part_9_InputStream_OutPutStream_Reader_Writer;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/*
Pattern - класс, содержащий рег выражение
Mather - класс содержащий строку для поиска совпадений

Mather.matches() - метод позоляет проверить, соответствует ли
регулярное выражение шаблону строки.
mather.start() - начальный индекс совпадения
mather.end() - конечный индекс совпадения
mather.find()  - емеется ли в шаблоне еще совпадения
mather.results() - поток Stream, в котором объекты класса MatchResult.
mather.group(2) - возвращает группу найденную методами mather.matches() или mather.find()
mather.split() - разбивает строку на массив
mather.replaceAll() - заменяет все найденные совпадения строкой.




 */
public class RegExp {
    public static void main(String[] args) {
        Matcher mather = Pattern.compile("(ddd)+(e)").matcher("ddde");
//        mather.matches();
        System.out.println(mather.group(1));


    }
}
