package MyPractics.Part_6_Jeneric_types.Test_3;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
        Object obj = new Object();
        SuperClass sup = new SuperClass();
        ExtendetClass ext = new ExtendetClass();
        ExtendetExtendetClass extExt = new ExtendetExtendetClass();

//        System.out.println(sup[0]);
//        System.out.println(sup[1]);
        ArrayList<? extends SuperClass> list = new ArrayList<>();
//        list.add(sup);
//        list.add(ext);
//        list.add(extExt);

        ArrayList<SuperClass> list2 = new ArrayList<>();
        list2.add(sup);
        list2.add(ext);
        list2.add(extExt);
        myPrint(list2);

        ArrayList<Object> list3 = new ArrayList<>();
        list3.add(sup);
        list3.add(ext);
        list3.add(extExt);
        myPrint2(list3);

        MyListTest<? extends ExtendetClass> el = new MyListTest<>();
//        el.add(obj);
//        el.add(extExt);
//        el.add(sup);
//        myPrint3(el);

    }

    public static void joining(ArrayList<? super SuperClass> rezult) {

    }

    public static void myPrint (ArrayList<? extends SuperClass> list ) {
        for (int i = 0; i < 3; i++) {
            System.out.println(list.get(i));
        }
    }

    public static void myPrint2 (ArrayList<? super ExtendetClass> list ) {
        for (int i = 0; i < 2; i++) {
            System.out.println(list.get(i));
        }
    }
//
//    public static void myPrint3 (MyListTest<? super ExtendetClass> list ) {
//
//    }
}
