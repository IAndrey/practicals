package MyPractics.Part_6_Jeneric_types;
//Массивы обобщенных типов
public class Test2 {
    public static void main(String[] args) {
//        MyInnerTestClass<String>[] sas = new MyInnerTestClass<String>[10];  - ошибка компиляции
    }
}

class MyInnerTestClass<T> {
    T t;
//    static T m; - ошибка компиляции, обобщенный тип не может быть статичекским

//    public boolean equals(T obj) { - ошибка компиляции, нельзя создать метод, с таким же названием
//    как в супер классе(Фактически переопределить) и при этом заменить параметры метода, обобщенными типами.
//        return super.equals(obj);
//    }
}
