package MyPractics.Part_6_Jeneric_types;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.function.IntFunction;

//ОБОБЩЕННЫЕ МЕТОДЫ
public class Test {
    public static void main(String[] args) {
        String [] strings = myMethod("as", 5);
        System.out.println(Arrays.toString(strings));
        String [] strings1 = myMethod2("sa", 5, String[]::new);
        System.out.println(Arrays.toString(strings1));
    }

    public static <T> T[] myMethod (T obj, int n) {
        T[] rezult = (T[]) Array.newInstance(obj.getClass(), n);
        for (int i = 0; i < n; i++) {
            rezult[i] = obj;
        }
        return rezult;
    }

    public static <T> T[] myMethod2 (T obj, int n, IntFunction<T[]> constr) {
        T[] rezult = constr.apply(n);
        for (int i = 0; i < n; i++) {
            rezult[i] = obj;
        }
        return rezult;
    }
}
