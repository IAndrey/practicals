package MyPractics.Part_13_Internalizable;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class Formatter {
    public static void main(String[] args) {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);
        ZonedDateTime znd = ZonedDateTime.now();
        System.out.println(dateFormat.format(znd));
    }
}
