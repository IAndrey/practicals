package MyPractics.Part_13_Internalizable;

import java.util.Locale;

public class MyLocale {
    public static void main(String[] args) {
        Locale loc = Locale.forLanguageTag("de-CH");
        System.out.println(loc);
        System.out.println(loc.getDisplayName(Locale.GERMAN));
    }
}
