package MyPractics.Part_13_Internalizable;

import java.text.MessageFormat;
import java.util.Currency;
import java.util.Locale;

public class MessegeFormater {
    public static void main(String[] args) {
        meth3();
    }

    public static void meth1() {
        String trmplate = "{1} has malslas {0}";
        System.out.println(MessageFormat.format(trmplate, "0", "1"));
    }

    public static void meth2() {
        Currency currency = Currency.getInstance("USD");
        Locale.setDefault(Locale.US);
        String trmplate = "has malslas {0, number, currency}";
        System.out.println(MessageFormat.format(trmplate, 1023));
    }

    public static void meth3() {
        String trmplate = "Apple test : \n{0, choice, 0#No apple|1#1 apple|2#{0} Apples }";
        System.out.println(MessageFormat.format(trmplate, 2));
    }

}
