package MyPractics.Part_13_Internalizable;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/*
Чтобы получить компаратор с учетом региональных настроек, следует вызвать метод Collator.getInstance(Locale)
 */
public class Sort {
    public static void main(String[] args) {
        Locale locale = Locale.ENGLISH;
        Collator coll = Collator.getInstance(locale);
        List<String> list = new ArrayList<>();
        list.sort(coll);
    }
}
