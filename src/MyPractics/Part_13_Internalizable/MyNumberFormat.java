package MyPractics.Part_13_Internalizable;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Currency;
import java.util.Locale;

public class MyNumberFormat {
    public static void main(String[] args) throws ParseException {
        form();
        parse();
    }

    public static void form() {
        Locale loc = Locale.US;
        NumberFormat format = NumberFormat.getCurrencyInstance(loc);
        double testd = 1100.5;
        System.out.println(format.format(testd));
    }

    public static void parse() throws ParseException {
        String testd = "1,100.50";
        Locale loc = Locale.US;
        NumberFormat format = NumberFormat.getNumberInstance();
        Currency currency = Currency.getInstance(loc);
        format.setCurrency(currency);
        System.out.println(format.parse(testd).doubleValue());
    }
}
