package MyPractics.Part_14_Compiling;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

public class ExecCompiler {
    public static void main(String[] args) {
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        OutputStream outStream = new ByteArrayOutputStream();
        OutputStream errStream = new ByteArrayOutputStream();;
        int result = compiler.run(null, null, null, "-sourcepath", "src/MyPractics/Part_14_Compiling", "Test.java");
        System.out.println(result);
    }
}
