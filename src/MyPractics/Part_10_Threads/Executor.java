package MyPractics.Part_10_Threads;

import java.util.concurrent.Executors;
/*
Executors.newCachedThreadPool() - Задачи будут исполняться в простаивающем потоке, новый поток будет выделяться,
если все существующие потоки уже заняты.

Executors.newFixedThreadPool(); - создается пул, с фиксированным количеством потоков исполнения, задаваемых в пареме-
тре метода. В пуле находятся потоке исполнения, которые ожидают своей очереди запуска. Новый поток будет выделяться,
если все существующие потоки уже заняты.
 */
public class Executor {
    public static void main(String[] args) {
        Runnable task1 = () -> {
            for (int i = 0; i < 50; i++) {
                System.out.println("task1");
            }
        };
        Runnable task2 = () -> {
            for (int i = 0; i < 50; i++) {
                System.out.println("task2");
            }
        };
        java.util.concurrent.Executor exec = Executors.newCachedThreadPool();
        exec.execute(task1);
        exec.execute(task2);
    }
}
