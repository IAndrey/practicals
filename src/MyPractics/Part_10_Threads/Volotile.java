package MyPractics.Part_10_Threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
Если оба потока пользуются одним объектом то изменения производимые над ним пишутся в кеш памяти процессора.
В результате другой поток этих изменений не увидит.
 */
public class Volotile {
    static volatile boolean bool = false;
    static volatile int count = 0;

    public static void main(String[] args) throws InterruptedException {
//        main1();
        main2();
    }

    public static void main1() {
        Runnable run1 = () -> {
            int x = 0;
            while (x < 999) {
                x++;
                System.out.println("Hello " + x);
            }
            ;
            bool = true;
        };

        Runnable run2 = () -> {
            int y = 0;
            while (!bool) y++;
            System.out.println(y); // Поток не закончится, т.к. bool запишется в кеш память процессора
        };

        ExecutorService exec = Executors.newFixedThreadPool(2);
        exec.execute(run1);
        exec.execute(run2);
        exec.shutdown();
    }

    // Демонстрация состояния гонок.
    public static void main2() throws InterruptedException {
        ExecutorService exec = Executors.newFixedThreadPool(100);
        for (int i = 0; i < 100; i++) {
            Runnable run1 = () -> {
                for (int j = 0; j < 10; j++) {
                    count++;
                }
            };
            exec.execute(run1);
        }
        exec.shutdown();
        Thread.sleep(1000);
        System.out.println(count);
    }
}
