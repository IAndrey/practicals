package MyPractics.Part_10_Threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*
Блокировки используются для блокирования блока кода, который должен быть выполнен
в одном потоке.
 */
public class Blokers {
private static int x = 0;

    public static void main(String[] args) throws InterruptedException {
        Lock countLock = new ReentrantLock();
        ExecutorService exec = Executors.newFixedThreadPool(100);
        Runnable run1 = () -> {
            countLock.lock();
            try {
                increments();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                countLock.unlock();
            }
        };
        for (int i = 0; i < 100; i++) {
            exec.execute(run1);
        }
        exec.shutdown();
        Thread.sleep(6000);
        System.out.println(x);
    }

    private static void increments() throws InterruptedException {
        for (int j = 0; j < 10; j++) {
            x++;
        }
        Thread.sleep(50);
//        System.out.printf("%s %d \n", Thread.currentThread().getName(), x);
    }
}
