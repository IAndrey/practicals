package MyPractics.Part_10_Threads;
/*
Создаем примитивный поток, который завершается, после выполнения воего кода.
th.join() - поток будет ждать завершения потока, для которого этот метод вызван.
 */
public class Threads {
    public static void main(String[] args) throws InterruptedException {
        Runnable run = () -> {
          for (int i = 0; i < 10; i++) {
              System.out.println(i);
          }
        };
        Thread th = new Thread(run);
        th.start();
        th.join();
        System.out.println(Thread.currentThread().getName());
    }

}
