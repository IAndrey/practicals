package MyPractics.Part_10_Threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/*
Для сложения результата можно воспользоваться атомарными классами.
Метод incrementAndGet гарантирует что операция инкрементирование будет
выполнена одним потоком.
 */
public class Atomic {
    private static AtomicInteger count = new AtomicInteger();
    public static void main(String[] args) throws InterruptedException {
        ExecutorService exec = Executors.newFixedThreadPool(100);
        for (int i = 0; i < 100; i++) {
            Runnable run1 = () -> {
                for (int j = 0; j < 10; j++) {
                    count.incrementAndGet();
                }
            };
            exec.execute(run1);
        }
        exec.shutdown();
        Thread.sleep(1000);
        System.out.println(count);
    }
}
