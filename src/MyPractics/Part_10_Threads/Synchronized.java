package MyPractics.Part_10_Threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
Блок synchronized предназначен для блокирования нитям доступа к объекту, приходячему в параметр
блока. synchronized может так же быть метод. Синхронизированный блок работает по принципу монитора.
Когда поток заходит в блок, он помечает синхронизированный блок как занятый, и другие потоки ждут когда он выйдет
из этого блока.
 */
public class Synchronized {
    private static Integer x = 0;

    public static void main(String[] args) throws InterruptedException {
        main2();
    }

    public static void main1() throws InterruptedException {
        Runnable run = () -> {
            synchronized (x) {
                x++;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        ExecutorService exec = Executors.newFixedThreadPool(100);
        for (int i = 0; i < 100; i++) {
            exec.execute(run);
        }
        Thread.sleep(1000);
        exec.shutdown();
        System.out.println(x);
    }

    public static void main2() throws InterruptedException {
        Runnable run = () -> {
                increment();
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        };
        ExecutorService exec = Executors.newFixedThreadPool(100);
        for (int i = 0; i < 100; i++) {
            exec.execute(run);
        }

        Thread.sleep(1000);
        exec.shutdown();
        System.out.println(x);
    }

    private static synchronized void increment() {
        x++;
    }
}
