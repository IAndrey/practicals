package MyPractics.Part_10_Threads;
/*
ExecutorService - наследуется от нитерфейса Executor. Обладает такими методами как invokeAll(), выполняющим все задачи из коллекции
, в параметры которого так же можно включить таймаут, в этом влучае все задачи отменяются, если
они не были выполнены по истечению времени ожидания.

Методы возвращают объект интерфейса Future, который может отменить задачу или вернуть ее.
Метод get замораживает текущий поток, и ждет пока не будет получен результат из потока.
Если же мы боимся что наш фоновый поток не завершится никогда, можем использовать get(long,TimeUnit)
ExecutorService отличается от Executor тем что обладает методом submit(Callable<V>) который возвращает
результат.

После выполнения кода, потоки не завершают свою работу. Чтобы их остановить,
нужно вызвать метод Executor.shutdown().
 */

import java.util.concurrent.*;

public class ExecutorServic {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService exec = Executors.newFixedThreadPool(2);
        Callable<String> task = () -> {
            Thread.sleep(2000);
            return "task" + " " + Thread.currentThread().getName();
        };
        Future<String> result1 = exec.submit(task);
        Future<String> result2 = exec.submit(task);
        System.out.println(result1.get());
        System.out.println(result2.get());
        System.out.println(Thread.currentThread().getName());
        exec.shutdown();
    }
}
