package MyPractics.Part_8_Stream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class Test {
    public static void main(String[] args) throws IOException {
        String str = new String(Files.readAllBytes(Paths.get("./src/MyPractics/Part_8_Stream/text.txt")));
//        System.out.println(str.offsetByCodePoints(0, 1));
        List<String> words = Arrays.asList(str.split("\\PL+"));
        Optional<String> str3 = words.stream().filter(s-> s.equals("d")).findAny();
        System.out.println(str3.orElse("dddd"));
//        Stream<String> stream2 = Stream.of("1", "3");
//        words.forEach(s -> System.out.println(s));
//        Stream.iterate(0, n -> n < 100000, n -> n + 1).limit(10).forEach(System.out::println);
//        System.out.println(integers.count());

    }

}
