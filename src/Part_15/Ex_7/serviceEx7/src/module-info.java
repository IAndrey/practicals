module serviceEx7 {
    exports pack;
    provides pack.GreeterService with french.FrenchGreeter,
            german.GermanGreeter;
}