package french;

import pack.GreeterService;

import java.util.Locale;

public class FrenchGreeter implements GreeterService {
    @Override
    public String greet(String subject) {
        return "Bonjour " + subject;
    }

    @Override
    public Locale getLocale() {
        return Locale.FRENCH;
    }
}
