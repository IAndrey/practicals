package Part_15.Ex_7;
/*
Что произойдет в примере программы из раздела 15.12, если опустить в ее
исходном коде оператор provides или uses? Почему ошибки возникают не во
время компиляции?

Если опустить оператор provides, то ошибки не произойдет, класс ServiceLoader
не обнаружит не одной реализации интерфейса.

Если опустить оператор uses, то произойдет ошибка :
java.util.ServiceConfigurationError: pack.GreeterService: module consumerServiceEx7 does not declare `uses`
Ошибки возникают не во время компиляции, так как ServiceLoader.load(Class clazz) выполняется во время
выполнения программы, а не во время компиляции кода.
Продемонстрировано в модулях consumerServiceEx7 и serviceEx7.
 */
public class Ex_7 {
}
