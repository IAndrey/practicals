package com;

import pack.GreeterService;

import java.util.ServiceLoader;

public class Main {
    public static void main(String[] args) {
        ServiceLoader<GreeterService> greeterLoader = ServiceLoader.load(GreeterService.class);
        for (GreeterService greeter : greeterLoader) {
            System.out.println(greeter.getClass().getSimpleName());
        }
    }
}
