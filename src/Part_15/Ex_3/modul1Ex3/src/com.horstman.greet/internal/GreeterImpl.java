package com.horstman.greet.internal;

import com.horstman.greet.Greeter;

import java.util.logging.Level;
import java.util.logging.Logger;

/*
Для того что бы воспользоваться библиотекой java.util.logging необходимо
в модуле прописать зависимость от библиотеки: requires java.logging; (если в библиотеке Jar
нет файла module-info.java - он является автоматическим модулем. Это сделано для того, что бы
модульные программы могли пользоваться старыми не модульными библиотеками.)
 */

public class GreeterImpl implements Greeter {
    @Override
    public String greet(String subject) {
        Logger logger = Logger.getGlobal();
        if (Logger.getGlobal().getLevel().equals(Level.ALL)) return "";
        /*
         проверяю уровень общего логера,
         если ниже инфо - возвращаю пустую строку (как вариант можно в конструтор обязать
         прокидывать ссылку на свой логер, что бы проверить его уровень)
         */

        return subject;
    }
}