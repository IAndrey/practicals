package com.horstman.greet;

import com.horstman.greet.internal.GreeterImpl;

public interface Greeter {
    static Greeter newInstance() {
        return new GreeterImpl();
    }

    String greet(String str);
}
