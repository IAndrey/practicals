package com.horstman.hello;

import com.horstman.greet.Greeter;

import java.util.logging.Level;
import java.util.logging.Logger;

/*

 */
public class HelloWorld {
    public static void main(String[] args) {
        Greeter greeter = Greeter.newInstance();
        Logger.getGlobal().setLevel(Level.ALL);
//        Logger.getGlobal().setLevel(Level.INFO);
        System.out.println(greeter.greet("aassas"));
    }
}
