module com.horstman.greet.impl {
    exports com.horstmann.greet.impl;
    requires transitive com.horstman.greet;
    provides com.horstmann.greet.Greeter with com.horstmann.greet.impl.GreeterImpl;
}