package com.horstmann.greet.impl;

import com.horstmann.greet.Greeter;

public class GreeterImpl implements Greeter {

    @Override
    public String greet(String subject) {
        return "GreeterImpl " + subject;
    }
}
