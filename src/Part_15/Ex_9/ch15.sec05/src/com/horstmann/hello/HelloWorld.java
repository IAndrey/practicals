package com.horstmann.hello;

import com.horstmann.greet.Greeter;
import com.horstmann.greet.impl.GreeterImpl;

import java.util.ServiceLoader;

public class HelloWorld {

    public static void main(String[] args) {
        Greeter greeter = version1();
        System.out.println(greeter.greet("as"));
    }

    public static Greeter version1() {
        ServiceLoader<Greeter> greeterLoader = ServiceLoader.load(Greeter.class);
        Greeter greeter = null;
        for (Greeter greet : greeterLoader) {
            if (greet.getClass().equals(GreeterImpl.class)) {
                greeter = greet;
            }
        }
        return greeter;
    }

}
