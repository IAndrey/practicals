package com.horstmann.greet;


//import com.horstmann.greet.impl.GreeterImpl;

import java.util.ServiceLoader;

public interface Greeter {
//    Greeter newInstance();

    String greet(String subject);
}
