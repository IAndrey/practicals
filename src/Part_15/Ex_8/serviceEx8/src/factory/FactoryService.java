package factory;

import french.FrenchGreeter;
import german.GermanGreeter;
import service.GreeterService;
import serviceTipes.ServiceTipes;

public class FactoryService {
    public static GreeterService provider(ServiceTipes serviceTipes) {
        switch (serviceTipes) {
            case FRENCH:
                return new FrenchGreeter();
            case GERMAN:
                return new GermanGreeter();
        }
        throw new RuntimeException("Неизвестный объект " + serviceTipes);
    }
}
