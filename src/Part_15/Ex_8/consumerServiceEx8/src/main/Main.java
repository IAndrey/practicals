package main;

import factory.FactoryService;
import service.GreeterService;
import serviceTipes.ServiceTipes;

public class Main {
    public static void main(String[] args) {
        GreeterService greeterService = FactoryService.provider(ServiceTipes.FRENCH);
        System.out.println(greeterService.greet("asas"));
    }
}
