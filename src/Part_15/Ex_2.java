package Part_15;
/*
Попытайтиесь получить доступ к методу GreeterImpl() из класса HelloWorld
в примере программы, приведенном в разделе 15.5 Что при этом произойдет?
Возникнет ли ошибка во время компиляции или во время выполнения

Ошибка возникнет во время компиляции, т.к. в модуле com.horstman.greet открыт доступ
только для пакета com.horstman.greet (exports com.horstman.greet). А класс GreeterImpl
находится в пакете com.horstman.greet.com.horstman.greet.internal; Чтобы код компилировался и выполнялся
без ошибки нужно в модуле com.horstman.greet выдать доступ пакету com.horstman.greet.internal
(exports com.horstman.greet.com.horstman.greet.internal;)
 */
public class Ex_2 {
}
