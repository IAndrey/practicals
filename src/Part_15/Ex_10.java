package Part_15;
/*
Загрузите программу JFreeChart с открытым исходным кодом и воспользуйтесь
утилитой jdeps, чтобы проанализировать зависимости в демонстрационной вер-
сии этой программы и архивных Jar - файлах из каталога lib

Скачал версию jfreechart-1.0.19.
Зависимости в демонстрационной версии этой программы.
Используя команду jdeps -s D:/jfreechart-1.0.19-demo.jar
получаю вывод всех модулей от которых зависит jfreechart-1.0.19:

jfreechart-1.0.19-demo.jar -> java.base
jfreechart-1.0.19-demo.jar -> java.datatransfer
jfreechart-1.0.19-demo.jar -> java.desktop
jfreechart-1.0.19-demo.jar -> java.logging
jfreechart-1.0.19-demo.jar -> java.sql
jfreechart-1.0.19-demo.jar -> not found - означает, что имеет еще модуль, не входящий в состав
java.se (сторонний модуль). Будет найден при использовании команды со всеми JAR из папки lib
Показано ниже

Зависимости архивных Jar - файлах из каталога lib
Используя команду
jdeps -s D:/jfreechart-1.0.19-demo.jar D:/lib/hamcrest-core-1.3.jar
D:/lib/jcommon-1.0.23.jar D:/lib/jfreechart-1.0.19.jar D:/lib/jfreechart-1.0.19-experimental.jar
D:/lib/jfreechart-1.0.19-experimental.jar D:/lib/jfreechart-1.0.19-swt.jar
D:/lib/jfreesvg-2.0.jar D:/lib/junit-4.11.jar D:/lib/orsoncharts-1.4-eval-nofx.jar
D:/lib/orsonpdf-1.6-eval.jar D:/lib/servlet.jar D:/lib/swtgraphics2d.jar

Получаю следующий список зависимостей:
hamcrest-core-1.3.jar -> java.base
jcommon-1.0.23.jar -> java.base
jcommon-1.0.23.jar -> java.datatransfer
jcommon-1.0.23.jar -> java.desktop
jfreechart-1.0.19-demo.jar -> java.base
jfreechart-1.0.19-demo.jar -> java.datatransfer
jfreechart-1.0.19-demo.jar -> java.desktop
jfreechart-1.0.19-demo.jar -> java.logging
jfreechart-1.0.19-demo.jar -> java.sql
jfreechart-1.0.19-demo.jar -> D:\lib\jcommon-1.0.23.jar
jfreechart-1.0.19-demo.jar -> D:\lib\jfreechart-1.0.19.jar
jfreechart-1.0.19-demo.jar -> D:\lib\jfreesvg-2.0.jar
jfreechart-1.0.19-demo.jar -> D:\lib\orsoncharts-1.4-eval-nofx.jar
jfreechart-1.0.19-demo.jar -> D:\lib\orsonpdf-1.6-eval.jar
jfreechart-1.0.19-experimental.jar -> java.base
jfreechart-1.0.19-experimental.jar -> java.desktop
jfreechart-1.0.19-experimental.jar -> D:\lib\jcommon-1.0.23.jar
jfreechart-1.0.19-experimental.jar -> D:\lib\jfreechart-1.0.19.jar
jfreechart-1.0.19-swt.jar -> java.base
jfreechart-1.0.19-swt.jar -> java.desktop
jfreechart-1.0.19-swt.jar -> D:\lib\jcommon-1.0.23.jar
jfreechart-1.0.19-swt.jar -> D:\lib\jfreechart-1.0.19.jar
jfreechart-1.0.19-swt.jar -> not found
jfreechart-1.0.19.jar -> java.base
jfreechart-1.0.19.jar -> java.datatransfer
jfreechart-1.0.19.jar -> java.desktop
jfreechart-1.0.19.jar -> java.sql
jfreechart-1.0.19.jar -> java.xml
jfreechart-1.0.19.jar -> D:\lib\jcommon-1.0.23.jar
jfreechart-1.0.19.jar -> D:\lib\servlet.jar
jfreesvg-2.0.jar -> java.base
jfreesvg-2.0.jar -> java.desktop
jfreesvg-2.0.jar -> java.logging
jfreesvg-2.0.jar -> not found
junit-4.11.jar -> D:\lib\hamcrest-core-1.3.jar
junit-4.11.jar -> java.base
orsoncharts-1.4-eval-nofx.jar -> java.base
orsoncharts-1.4-eval-nofx.jar -> java.desktop
orsoncharts-1.4-eval-nofx.jar -> java.logging
orsonpdf-1.6-eval.jar -> java.base
orsonpdf-1.6-eval.jar -> java.desktop
orsonpdf-1.6-eval.jar -> java.logging
servlet.jar -> java.base
swtgraphics2d.jar -> java.base
swtgraphics2d.jar -> java.desktop
swtgraphics2d.jar -> D:\lib\jfreechart-1.0.19.jar
swtgraphics2d.jar -> not found

 */
public class Ex_10 {
}
