package processor;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@SupportedAnnotationTypes({"annotations.AnnotationEx6"})
public class Proc extends AbstractProcessor {

    public Proc() {
    }

    public void testMeth(){
    }

    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (TypeElement annotation : annotations) {
            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            List<Element> elements = new ArrayList(annotatedElements);
            if (!elements.isEmpty()) {
                List list = (List) elements.stream().map((element) -> {
                    return element.getSimpleName().toString();
                }).collect(Collectors.toList());
                try {
                    writeBuilderFile(list);
                } catch (IOException var10) {
                    var10.printStackTrace();
                }
            }
        }
        return true;
    }

    private void writeBuilderFile(List<String> elements) throws IOException {
        String packageName = "generated";
        String toStringsClassName = "ProcessorEx6";
        JavaFileObject builderFile = this.processingEnv.getFiler().createSourceFile(toStringsClassName, new Element[0]);
        PrintWriter out = new PrintWriter(builderFile.openWriter());

        try {
            if (packageName != null) {
                out.print("package ");
                out.print(packageName);
                out.println(";");
                out.println();
            }

            out.print("public class ");
            out.print(toStringsClassName);
            out.println("{");
            out.println("public String getAnnotationElementName(){ ");
            out.println("StringBuilder strGet = new StringBuilder();");
            Iterator var9 = elements.iterator();

            while (true) {
                if (!var9.hasNext()) {
                    out.println("return strGet.toString();");
                    out.println("}");
                    out.println();
                    out.println("}");
                    break;
                }

                String element = (String) var9.next();
                out.println("strGet.append(\"" + element + "\" +  \"\\n\");");
            }
        } catch (Throwable var12) {
            try {
                out.close();
            } catch (Throwable var11) {
                var12.addSuppressed(var11);
            }

            throw var12;
        }

        out.close();
    }

    private void writeBuilderFile2() throws IOException {
        String packageName = "com";
        String toStringsClassName = "AnnotationEx6";
        JavaFileObject builderFile = this.processingEnv.getFiler().createSourceFile(toStringsClassName, new Element[0]);
        PrintWriter out = new PrintWriter(builderFile.openWriter());


        if (packageName != null) {
            out.print("package ");
            out.print(packageName);
            out.println(";");
            out.println();
        }

        out.print("public class ");
        out.print(toStringsClassName);
        out.println("{");
        out.println("public String getAnnotationElementName(){ ");
        out.println("StringBuilder strGet = new StringBuilder();");
        out.println("return strGet.toString();");
        out.println("}");
        out.println();
        out.println("}");
        out.close();
    }
}
