module annotationEx6 {
    exports processor;
    exports annotations;
    requires java.compiler;
    provides javax.annotation.processing.Processor with processor.Proc;
}