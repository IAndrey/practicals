import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import java.io.IOException;



public class Main extends Application {
    public static void main(String[] args) throws IOException {
       launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Group group = new Group();
        Parent content = FXMLLoader.load(getClass().getResource("myForm"));
        BorderPane root = new BorderPane();
        root.setCenter(content);
        group.getChildren().add(root);
        stage.setScene(new Scene(group, 400, 300));
        stage.show();
    }
}
