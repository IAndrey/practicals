package Part_15.Ex_13;
/*
Попробуйте выполнить в версии java 9 программу JavaFX SceneBuilder,
разработанную в версии Java 8. С каких именно параметров командной
строки следует начать? Как это выяснить?

Для того что бы запустить JavaFX SceneBuilder, нужно запускать VM
со следующими параметрами:

Задаем явным образом модули, которые требуются для выполнения, т.к. данный
модуль MySceneBuilder не имеет файла Module-info.java
--add-modules=javafx.swing,javafx.graphics,javafx.fxml,javafx.media,javafx.web

Т.к. доступ к библиотекам в модуле javafx.controls закрыт, то
с таким параметром запуска можно открыть библиотеки для рефлексии
--add-opens javafx.controls/com.sun.javafx.charts=ALL-UNNAMED
--add-opens javafx.graphics/com.sun.javafx.iio=ALL-UNNAMED
--add-opens javafx.graphics/com.sun.javafx.iio.common=ALL-UNNAMED
--add-opens javafx.graphics/com.sun.javafx.css=ALL-UNNAMED
--add-opens javafx.base/com.sun.javafx.runtime=ALL-UNNAMED

Примечание, не удается запустить, т.к. проект изначально не был создан как
JavaFx
 */
public class Ex_13 {
}
