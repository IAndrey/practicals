package Part_15;
/*
Выполните утилиту jlink, чтобы получить образ демонстрационной
версии программы JFreeChart для стадии выполнения

Сначала перевел компандную строку на диск D
D:
cd D:\Projects перешел в корневую папку проекта.
Для создания образа, я использовал команду
jlink -p src/Part_15/Ex_11/JfreeChart/lib;
src/Part_15/Ex_11/JfreeChart/libJavaFX;
$JAVA_HOME/jmods;
out/artifacts/JfreeChart_jar --add-modules JfreeChart --output ex_11

Чтобы командная строка распознавала jlink, должна быть переменная среды path, включающая
путь C:\Program Files\Java\jdk-11.0.1\bin

-p - сокращенная команда --module-path

Далее через ; указываются дирректории в которых лежат JAR модули, от которых зависит мой
модуль из упр 11 - JfreeChart
src/Part_15/Ex_11/JfreeChart/lib - путь к библиотекам, от которых зависит модуль JfreeChart
src/Part_15/Ex_11/JfreeChart/libJavaFX - путь к библиотекам javaFX
$JAVA_HOME/jmods - JAVA_HOME - моя переменная среды (C:\Program Files\Java\jdk-11.0.1) в папке jmods
лежат все стандартные модули Java
out/artifacts/JfreeChart_jar - путь к скомпилированному моему модулю JfreeChart
--add-modules JfreeChart --output ex_11 - создаем образ для модуля JfreeChart и кладем его в ex_11
которая создается в том месте где мы указали путь в командной строке, т.е. в cd D:\Projects

 */
public class Ex_12 {
}
