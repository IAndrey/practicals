package Part_15.Ex_11;
/*
Преобразуйте демонстрационную версию программы JFreeChart в модуль,
тогда как архивные JAR-файлы из подкаталога lib - в автоматические
модули

Добавил в текущий проект новый Maven модуль JfreeChart.
Для того, чтобы модуляризировать проект, первым делом потребовалось
установить уровень компилятора 9 версии Java в pom.xml файле проекта
изначально стоял 6 уровень.
Добавил файл module-info.java в папку source, зависимости можно узнать
с помощью jdeps (10 задание)
module-info.java выглядит след образом

После этого так же потребовалось добавить в проект библиотеки JavaFX,
добавил их в пакет libJavaFX, и добавил их в модуль, как автоматические
модули.

module JfreeChart {
    requires java.desktop;
    requires java.datatransfer;
    requires java.logging;
    requires java.sql;
    requires jfreesvg;
    requires orsoncharts;
    requires orsonpdf;
    requires jcommon;
    requires servlet;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.base;
    requires javafx.swing;
}

Архивные JAR-файлы из подкаталога lib являются автоматическими модулями
т.к. у них нет файла module-info.java

Модуль JfreeChart успешно компилируется и собирается в  папку target/classes.

 */
public class Ex_11 {
}
