module JfreeChart {
    requires java.desktop;
    requires java.datatransfer;
    requires java.logging;
    requires java.sql;
    requires jfreesvg;
//    requires jfreechart;
    requires orsoncharts;
    requires orsonpdf;
    requires jcommon;
    requires servlet;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.base;
    requires javafx.swing;
}
