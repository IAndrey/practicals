package Part_14.Ex_2;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/*
Организуйте вызов из программы на Java метода JSON.parse() в Java Script, чтобы преобразовать
символьную строку из формата JSON в объект JavaScript, а затем обратно в символьную строку.
Сделайте это, во-превых, с помощью метода eval(); во-вторых, с помощью метода invokeMethod();
и в-третьих, вызвав метод Java через следующий интерфейс:
public interface JSON {
    Object parse(String str);
    String stringify(Object obj);
}
 */
public class Ex_2 {
    public static void main(String[] args) throws ScriptException, NoSuchMethodException {
//        part1Eval("{\\\"firstName\\\": \\\"John\\\"}");
//        part2InvokeMethod("{\"firstName\": \"John\"}");
        part3getInterface("{\"firstName\": \"John\"}");
    }

    public static void part1Eval(String str) throws ScriptException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");
        engine.eval("var testJson = JSON.parse(\"" + str + "\")");
        System.out.println(engine.eval("JSON.stringify(testJson)"));
    }

    public static void part2InvokeMethod(String str) throws ScriptException, NoSuchMethodException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");
//        Синтаксис объявления класс в JS.
        engine.eval("function MyJson (jString) { this.jString = jString}");
        engine.eval("MyJson.prototype.myParse = function(str) { var testJson = JSON.parse(str); " +
                "return JSON.stringify(testJson)}");
        Object myJson = engine.eval("new MyJson('Yo')");
        String result = (String) ((Invocable) engine).invokeMethod(myJson, "myParse", str);
        System.out.println(result);
    }

    public static void part3getInterface(String str) throws ScriptException, NoSuchMethodException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");
//        Синтаксис объявления класс в JS.
        engine.eval("function MyJson (jString) { this.jString = jString}");
        engine.eval("MyJson.prototype.myParse = function(str) { var testJson = JSON.parse(str); " +
                "return JSON.stringify(testJson)}");
        Object myJson = engine.eval("new MyJson('Yo')");
        JSON result = (JSON) ((Invocable) engine).getInterface(myJson, JSON.class);
        System.out.println(result.myParse(str));
    }
}
