package Part_14;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.StreamSupport;

/*
Выполните утилиту jjs и воспользуйтесь библиотекой потоков данных, чтобы выробатать
решение следующей задачи: вывести из файла все однозначные длинные слова (больше 12 букв)
в отсортированном порядке. Сначала организуйте чтение слов, затем фильтрацию длинных слов
и т.д.

Код на JS
var Files = Java.type('java.nio.file.Files')
var Paths = Java.type('java.nio.file.Paths')
var string = Files.readString(Paths.get("C:/Users/anni0219/Documents/1.txt")).split(' ')
var stringArray = Java.type('java.lang.String[]')
var words = Java.to(string, stringArray)
var Arrays = Java.type('java.util.Arrays')
Arrays.toString(words) - проверил что выдало верный массив
Arrays.stream(words).filter(function (word) word.length > 8).
sorted(function (wordA, wordB) wordA.compareTo(wordB)).forEach(function (word) print(word)) - выполняю сортировки
 */

public class Ex_6 {
    public static void main(String[] args) throws IOException {
        String s = Files.readString(Paths.get("C:/Users/anni0219/Documents/1.txt"));
        String [] ss = Files.readString(Paths.get("C:/Users/anni0219/Documents/1.txt")).split("\\PL+");
        Arrays.stream(ss).filter(word -> word.length() > 8).sorted((wordA, wordB)-> wordA.compareTo(wordB));
    }
}
