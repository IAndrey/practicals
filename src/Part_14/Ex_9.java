package Part_14;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;

/*
Напишите сценарий, где создается архивный JAR - файл, содержащий
все файлы классов в подкаталогах, входящих в заданный каталог. Выведите
имя архивного JAR-файла из последней составляющей в имени каталога.
Воспользуйтесь переменной окружения JAVA_HOME для обнаружения исполняемого
активного файла JAR-файла.

Код на JS (запускал jjs версии 11.0.4) (C:\Program Files\Java\jdk-11.0.4\bin)

var str = 'C:/Practicals/src/Part_14' -- мой заданный каталог
var Files = Java.type('java.nio.file.Files');
var Paths = Java.type('java.nio.file.Paths');

var path = 'C:/Users/anni0219/Documents/';  - путь куда создается jar файл
var fileName = Paths.get(str).getFileName(); - беру имя файла из последней составляющей в имени каталога
var pathFileName = path + fileName +'.jar'; -  результирующий путь к создаваемому файлу

var file = new java.io.File(pathFileName);
var outputStream = new java.util.jar.JarOutputStream(new java.io.FileOutputStream(file));

Files.walk(Paths.get(str)).filter(function (f) f.toFile().isFile()).forEach(function (f) {
    var entry = new java.util.zip.ZipEntry(f.getFileName().toString());
    outputStream.putNextEntry(entry);
 });

 outputStream.close();
 */
public class Ex_9 {
    public static void main(String[] args) throws IOException {
            meth("C:/Practicals/src/Part_14");
    }

    private static void meth(String str) throws IOException {
        File file = new File("C:/Users/anni0219/Documents/myJar.jar");
        JarOutputStream outputStream = new JarOutputStream(new FileOutputStream(file));
        Files.walk(Paths.get(str)).filter(f-> f.toFile().isFile()).forEach(path -> {
            ZipEntry entry = new ZipEntry(path.getFileName().toString());
            try {
                outputStream.putNextEntry(entry);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        outputStream.close();
    }
}
