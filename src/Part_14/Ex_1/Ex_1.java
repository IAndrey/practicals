package Part_14.Ex_1;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

/*
В технологии JavaServer Pages веб страница состоит из HTML - разметки и кода Java,
как показано в следующем примере:
<ul>
<% for (int i = 10; i >= 0; i--) { %>
    <li><%= i %></li>
<% } %>
<p> Liftoff!</p>
Все, что находится за пределами разметки <%...%> и <%=...%>, выводится в исходном виде,
а код в пределах этой разметки вычисляется. Если разметка начинается с разделителя <%=,
то результат вычисления добавляется к тому, что выводится. Реализуйте программу, читающую
такую веб-страницу, превращающую ее в метод Java, выполняющую его и получающую результирующую
страницу.
 */
public class Ex_1 {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("<ul>sadsa</ul>");
        list.add("<% for (int i = 10; i >= 0; i--) { %>");
        list.add("<li><%= i %></li>");
        list.add("<% } %>");
        list.add("<p> Liftoff!</p>");

        HTMLExecutor exec = new HTMLExecutor(list);
        exec.CreateСlassFromString();
    }
}
