package Part_14.Ex_1;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// С помощью этого класса я могу создать класс и строк и скомпилировать его, используя
// соответствующие методы.
public class ConvertStringToClass {

    private boolean compile;
    private String className;
    private StringBuilder cLass;
    private List<String> classes = new ArrayList<>();
    private List<String> methods = new ArrayList<>();

    public ConvertStringToClass(String className) {
        this.className = className;
        classes.add("public class " + className + " {\n");
        classes.add("}");
        createClass();
    }

//    Только для 1 задания так как захардкодил String result
    public void addMethod(String methodParameters, String body) {
        if (methodParameters.contains("{")) methodParameters = methodParameters.replace("{", "");
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        writer.println(methodParameters + " {");
        writer.println("String result = \"\";");
        writer.println(body);
        writer.println("return result;");
        writer.println("}");
        methods.add(stringWriter.toString());
        createClass();
    }

    public String getClassToString() {
        return cLass.toString();
    }

    private void createClass() {
        cLass = new StringBuilder();
        cLass.append(classes.get(0));
        methods.forEach(cLass::append);
        cLass.append(classes.get(1));
    }

    //C помощью данного метода я компилирую свой класс в динамическом режиме
    public boolean runtimeCompileInstance() {
        compile = true;
//      Класс StringSource предназначен для динамической компиляции строки.
        List<StringSource> sources = Arrays.asList(new StringSource(className, cLass.toString()));
//       Компиляция класса в динамическом режиме из строки.
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        JavaCompiler.CompilationTask task = compiler.getTask(null, null, null, null, null, sources);
        return task.call();
    }
}
