package Part_14.Ex_1;

import javax.tools.SimpleJavaFileObject;
import java.net.URI;

public class StringSource extends SimpleJavaFileObject {

    private String code;

    protected StringSource(String name, String code) {
        super(URI.create(name + ".java"), Kind.SOURCE);
        this.code = code;
    }

    public CharSequence getCharContent (boolean ignoreEncodingErrors) {
        return code;
    }
}
