package Part_14.Ex_1;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

public class HTMLExecutor {

    //C помощью данного метода, с помощью загрузчика классов URLClassLoader загружаю скомпилированный до этого класс и с помощью рефлексии выполняю метод
    public static String runtimeExecMethod() {
        String res = "";
//        if (!compile) throw new RuntimeException("Сначала необходимо скомпилировать класс методом runtimeCompileInstance()");
        try {
//              Динамический загрузчик класса.
            URLClassLoader loader = URLClassLoader.newInstance(new URL[]{new File("").toURI().toURL()});
            res = (String) Class.forName("HtmlExec", false, loader).getDeclaredMethod("htmlExecute").invoke(null);
        } catch (MalformedURLException | ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return res;
    }

    private List<String> html;
    private List<Integer> removeLine;
    private int replaceLine;
    private String resultCode;

    public HTMLExecutor(List<String> html) {
        this.html = html;
        removeLine = new ArrayList<>();
    }

    public void CreateСlassFromString() {
        ConvertStringToClass executeClass = new ConvertStringToClass("HtmlExec");
        executeClass.addMethod("public static String htmlExecute()", searchCodeInHTML());
        executeClass.runtimeCompileInstance();
        resultCode = runtimeExecMethod();
        getResultHtml();
        html.forEach(System.out::println);
    }

    //  С помощью этого метода вытаскиваю из развертки HTML код и выполняю его.
    public String searchCodeInHTML() {
        StringBuilder code = new StringBuilder();
//        for (String cod : html) {
        for (int i = 0; i < html.size(); i++) {
            String cod = html.get(i);
            if (!cod.contains("<%")) continue;
            if (cod.contains("<%=")) {
                code.append("result += ").append(cod.replaceAll("(.*<%=?)|(%>)|(<.+>)|\\s", "") + " + \"\";");
                replaceLine = i;
                continue;
            }
            if (cod.contains("</")) {
                code.append("result = " + cod.replaceAll("(<%=?)|(%>)|(<.+>)|\\s", "") + " + \"\";");
                replaceLine = i;
                continue;
            }
            code.append(cod.replaceAll("(<%=?)|(%>)|(<.+>)", ""));
            removeLine.add(i);
        }
        return code.toString();
    }

    private void getResultHtml() {
        String replace = html.get(replaceLine).replaceAll("<%=?.+%>", resultCode);
        html.set(replaceLine, replace);
        for (int i = 0; i < html.size(); i++) {
            String cod = html.get(i);
            if (cod.contains("<%")) {
                html.remove(i);
                i--;
            }
        }
    }
}
