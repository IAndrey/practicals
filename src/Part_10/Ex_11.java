package Part_10;

import MyPractics.Part_10_Threads.ExecutorServic;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

/*
Воспользуйтесь блокирующей очередью для обработки файлов в каталоге. В одном
потоке исполнения организуйте обход дерева каталога и ввод файлов в очередь,
а в нескольких потоках исполнения - удаление файлов и поиск в каждом из них
заданного ключевого слова с выводом любых совпадений. Как только задача поставщика
будет завершена, в очередь должен быть введен фиктивный файл.
 */
public class Ex_11 {
    public static void main(String[] args) throws InterruptedException {
        Path root = Paths.get("./Test/Part_10/TestDirectory");
        ArrayBlockingQueue<File> queue = new ArrayBlockingQueue<>(10);
        searchWordInFileOnTwoThreads("search", queue);
        Thread.sleep(500);
        searchFileOneThreadAndAddInQueue(root, queue);
    }

    public static void searchFileOneThreadAndAddInQueue(Path root, ArrayBlockingQueue queue) {
        new Thread(searchFileAndAddInQueue(root, queue)).start();
    }

    private static void searchFileInDirectoryAndAddInQueue(Path root, ArrayBlockingQueue<File> queue) throws InterruptedException {
        if (root.toFile().isFile()) throw new RuntimeException("Начальный путь должен быть дирректорией");
        File[] files = root.toFile().listFiles();
        for (File f : files) {
            if (f.isDirectory()) {
                searchFileInDirectoryAndAddInQueue(f.toPath(), queue);
            } else queue.put(f);
        }
    }

    private static Runnable searchFileAndAddInQueue(Path root, ArrayBlockingQueue<File> queue) {
        Runnable run = () -> {
            try {
                searchFileInDirectoryAndAddInQueue(root, queue);
                queue.put(new File("mirage.txt"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        return run;
    }

    public static void searchWordInFileOnTwoThreads(String word, ArrayBlockingQueue<File> queue) {
        ExecutorService exec = Executors.newFixedThreadPool(2);
        exec.execute(getRunnableSearchWordInFile(word, queue, exec));
        exec.execute(getRunnableSearchWordInFile(word, queue, exec));
    }

    private static Runnable getRunnableSearchWordInFile(String word, ArrayBlockingQueue<File> queue, ExecutorService exec) {
        Runnable run = () -> {
            String reult = "";
            try {
                while (true) {
                    File f = queue.take();
                    if (f.getName().equals("mirage.txt")) {
                        exec.shutdownNow();
                        break;
                    }
                    if (Files.readString(f.toPath()).contains(word)) {
                        reult = String.format("%s, файл - %s, поток - %s", word, f.getPath(), Thread.currentThread().getName());
                        System.out.println(reult);
                    }
                }
            } catch (InterruptedException | IOException e) {
            }
        };
        return run;
    }
}


