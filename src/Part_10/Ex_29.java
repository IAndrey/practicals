package Part_10;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

/*
Следующий метод:

static CompletableFuture<Object> CompletableFuture.anyOf(CompletableFuture<?>... cfs )

возвратит результат своего выполнения, как только обработака любого из его аргументов завер-
шится нормально, а иначе - исключение. Этим данный метод замето отличается от метода
ExecutorService.invokeAny(), который продолжает выполняться вплоть до удачного завершения
одной из задач, что припятствует его применению для параллельного поиска. Реализуйте приведенный
ниже метод, выдающий первый же конкретный результат или исключение типа NoSuchElementException,
если все действия завершатся исключениями.

static CompletableFuture<T> anyOf(List<Supplier<T>> actions, Executor exec)
 */
public class Ex_29 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        List<Supplier<String>> list = new ArrayList<>();
        String[] strings = {"test1", "test2", "test3", "test4", "test5", "test6", "test7"};
        list.add(getMySupplier(strings, "test10"));
        list.add(getMySupplier(strings, "test4"));
        CompletableFuture<String> cfs = anyOf(list, Executors.newFixedThreadPool(10));
        System.out.println(cfs.get());
    }

    public static <T> CompletableFuture<T> anyOf(List<Supplier<T>> actions, Executor exec) {

        CompletableFuture<T> com = new CompletableFuture<>();
        for (Supplier<T> supp : actions) {
            try {
                supp.get();
                com = com.completeAsync(supp, exec);
                return com;
            } catch (NoSuchElementException e) {
            }
        }
        throw new NoSuchElementException();
    }

    public static Supplier<String> getMySupplier(String[] strings, String searchWord) {
        return () -> {
            for (String s : strings) {
                if (s.equals(searchWord)) {
                    return s;
                }
            }
            throw new NoSuchElementException();
//            return null;
        };
    }
}
