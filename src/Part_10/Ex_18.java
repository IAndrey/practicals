package Part_10;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/*
Усовершенствуйте программу из предыдущего упражнения, используя блокировки.

При использовании локера, операция сложения блокируется для выполнения по очереди
в каждом потоке.
 */
public class Ex_18 {
    public static Long count = 0L;

    public static void main(String[] args) throws IOException, InterruptedException {
        List<Path> list = Files.walk(Paths.get("./Test/Part_10/TestDirectory")).filter(p -> p.toFile().isFile()).collect(Collectors.toList());
        Lock lock = new ReentrantLock();
        for (Path p : list) {
            new Thread(getRunnable(p.toFile(), lock)).start();
        }
        Thread.sleep(200);
        System.out.println(count);
    }

    private static Runnable getRunnable(File file, Lock lock) {
        Runnable run = () -> {
            int x = 0;
            try {
                x = Files.readString(file.toPath()).split("\\PL+").length;
            } catch (IOException e) {
                e.printStackTrace();
            }

            lock.lock();
            try {
                count += x;
            } finally {
                lock.unlock();
            }
        };
        return run;
    }
}
