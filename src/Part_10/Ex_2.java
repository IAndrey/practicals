package Part_10;

import java.util.Arrays;
import java.util.concurrent.locks.Lock;

/*
Насколько большим должен быть массив, чтобы метод Arrays.parallelSort() выполнялся
быстрее, чем метод Arrays.sort() на вашем компьютере?

Если массив будет состоять из 1000000 рандомных чисел, то разница ощутима.
 */
public class Ex_2 {
    public static void main(String[] args) {
        System.out.println(sort(init(new int[1000000])));
        System.out.println(parallelSort(init(new int[1000000])));
    }

    public static Long sort(int[] ints) {
        Long start = System.currentTimeMillis();
        Arrays.sort(ints);
        Long currentTime = System.currentTimeMillis();
        return currentTime - start;
    }

    public static Long parallelSort(int[] ints) {
        Long start = System.currentTimeMillis();
        Arrays.parallelSort(ints);
        Long currentTime = System.currentTimeMillis();
        return currentTime - start;
    }

    private static int[] init(int[] ints) {
        for (int i = 0; i < ints.length; i++) {
            ints[i] = random();
        }
        return ints;
    }

    private static int random() {
        return  (int) (Math.random() * 10000000);
    }
}
