package Part_10.Ex_5;

import java.util.*;
/*
Неизменяемый класс:
1 - в нем нельзя изменить сотояние объкта.
2 - не один из методов не должен быть модифицирующим.
3 - класс нужно объявить как final, что бы от класса нельзя было наследоваться, т.к. в наследниках можно
ввести модифицирующие методы.
 */
public final class Student {
    private String name;
    private int age;

    public static List<Student> listStudents = new ArrayList<>(); // Статическое поле, содержащее все объекты класса.

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
        // Небезопасно, потому что set имеет глобальную видимость
        listStudents.add(this);
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
