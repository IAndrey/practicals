package Part_10.Ex_5;

import java.util.Iterator;

/*
Создайте пример, в котором демонстрируется исчезновение ссылки this в
конструкторе неизменяемого класса (см. раздел 10.3.4). Постарайтесь
сделать этот пример достаточно убедительным. Если вы воспользуетесь
для этой цели приемником событий, как это зачастую делается во многих
веб-приложениях, то организуйте прием какого-нибудь интересного события,
что не так-то просто сделать в неизменяемом классе.
 */
public class Ex_5 {
    public static void main(String[] args) throws InterruptedException {
        getMyThread().start();
        Student st1 = new Student("Petia", 20);
        Student st2 = new Student("Vasia", 20);
        Thread.sleep(500);
        Student.listStudents.forEach((student -> System.out.println(student.getName())));
    }

    //  Добавил слушателя, который следит за состоянием объектов Student
    private static Runnable myListener() {
        return () -> {
            Iterator<Student> iter = Student.listStudents.iterator();
            while (true) {
                while (iter.hasNext()) {
                    Student student = iter.next();
                    if (student.getName().equals("Vasia")) {
                        iter.remove();
                    }
                }
            }
        };
    }

    private static Thread getMyThread () {
        Thread th = new Thread(myListener());
        th.setDaemon(true);
        return th;
    }
}
