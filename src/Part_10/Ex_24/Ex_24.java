package Part_10.Ex_24;

/*

 */
public class Ex_24 {
    public static void main(String[] args) throws InterruptedException {
        Stack stack = new Stack();
        pushInStack(stack);
        Thread.sleep(1000);
        System.out.println(stack.getSize());
    }

    private static void pushInStack(Stack stack) throws InterruptedException {
        for (int i = 0; i < 10000; i++) {
            new Thread(run1(stack)).start();
        }
    }

    private static Runnable run1(Stack stack) {
        Runnable run = () -> {
            stack.push("1");
        };
        return run;
    }
}
