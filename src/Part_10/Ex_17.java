package Part_10;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/*
Напишите программу для обхода дерева каталогов с формированием отдельного потока
исполнения для каждого файла. Подсчитайте в потоках исполнения кол-во слов в
файлах и, не прибегая к блокировкам, обновите общий счетчик, который объявляется
следующим образом:

public static Long count = 0;

Выполните эту программу неоднократно. Что при этом происходит и почему?

При выполнении этой программы происходит состояние гонок, т.к. каждый поток перезатирает
операцию сложения, в результате выводится разный результат.
 */
public class Ex_17 {

    public static Long count = 0L;

    public static void main(String[] args) throws IOException, InterruptedException {
        List<Path> list = Files.walk(Paths.get("./Test/Part_10/TestDirectory")).filter(p -> p.toFile().isFile()).collect(Collectors.toList());
        for (Path p : list) {
            new Thread(getRunnable(p.toFile())).start();
        }
        Thread.sleep(1000);
        System.out.println(count);
    }

    private static Runnable getRunnable (File file) {
        Runnable run = () -> {
            try {
                int x = Files.readString(file.toPath()).split("\\PL+").length;
                count += x;
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
        return run;
    }
}
