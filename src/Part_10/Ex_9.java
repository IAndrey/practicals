package Part_10;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

/*
Сформируйте 1000 потоков исполнения, в которых счетчик инкрементируется 100000 раз.
Сравните производительность при использовании классов AtomicLong и longAdder.

LongAdder выполняет операции инкрементирования гораздо производительнее
чем класс AtomicLong.
 */
public class Ex_9 {
    private static AtomicLong aLong = new AtomicLong();
    private static LongAdder longAdder = new LongAdder();

    public static void main(String[] args) throws InterruptedException {
        System.out.println(getTime(aLong));

        System.out.println(getTime(longAdder));
    }

    private static Long getTime(AtomicLong aLong) {
        Long currentTime = System.currentTimeMillis();
        executeThreads(1000, increments(100000, aLong));
        while (aLong.longValue() < 100000000L) {
        }
        Long time = System.currentTimeMillis();
        return  time - currentTime;
    }

    private static Long getTime(LongAdder longAdder) {
        Long currentTime = System.currentTimeMillis();
        executeThreads(1000, increments(100000, longAdder));
        while (aLong.longValue() < 100000000L) {
        }
        Long time = System.currentTimeMillis();
        return  time - currentTime;
    }

    private static Runnable increments(int count, LongAdder aLong) {
        Runnable run = () -> {
            for (int i = 0; i < count; i++) {
                aLong.increment();
            }
        };
        return run;
    }

    private static Runnable increments(int count, AtomicLong aLong) {
        Runnable run = () -> {
            for (int i = 0; i < count; i++) {
                aLong.incrementAndGet();
            }
        };
        return run;
    }

    private static void executeThreads (int count, Runnable run) {
        for (int i = 0; i < count; i++) {
            new Thread(run).start();
        }
    }
}
