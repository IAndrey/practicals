package Part_10;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/*
Повторите предыдущее упражнение, но на этот раз сформируйте в задаче
потребителя отображение слов и частоты, с которой они вводятся во
вторую очередь. В последнем потоке исполнения полученные словари должны
быть объеденены, а затем выведены десять наиболее употребительских слов.
Почему для этой цели не потребуется отображение типа ConcurrentHashMap?

ConcurrentHashMap не потребуется, так как каждый поток работает со своим
экземпляром HashMap.
 */
public class Ex_12 {
    private static HashMap<String, Integer> map1 = new HashMap<>();
    private static HashMap<String, Integer> map2 = new HashMap<>();

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Path root = Paths.get("./Test/Part_10/TestDirectory");
        ArrayBlockingQueue<File> queue = new ArrayBlockingQueue<>(10);
        searchFileOneThreadAndAddInQueue(root, queue);
        searchWordInFileOnTwoThreads(queue);
        Thread th =  new Thread(Ex_12::unionMap);
        th.setDaemon(false);
        th.start();
    }

    public static void searchFileOneThreadAndAddInQueue(Path root, ArrayBlockingQueue queue) {
        new Thread(getRunnableSearchFileAndAddInQueue(root, queue)).start();
    }

    private static void searchFileInDirectoryAndAddInQueue(Path root, ArrayBlockingQueue<File> queue) throws InterruptedException {
        if (root.toFile().isFile()) throw new RuntimeException("Начальный путь должен быть дирректорией");
        File[] files = root.toFile().listFiles();
        for (File f : files) {
            if (f.isDirectory()) {
                searchFileInDirectoryAndAddInQueue(f.toPath(), queue);
            } else queue.put(f);
        }
    }

    private static Runnable getRunnableSearchFileAndAddInQueue(Path root, ArrayBlockingQueue<File> queue) {
        Runnable run = () -> {
            try {
                searchFileInDirectoryAndAddInQueue(root, queue);
                queue.put(new File("mirage.txt"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        return run;
    }

    public static void searchWordInFileOnTwoThreads(ArrayBlockingQueue<File> queue) throws ExecutionException, InterruptedException {
        ExecutorService exec = Executors.newFixedThreadPool(2);
        exec.submit(getRunnableSearchWordInFile(queue, exec, map1));
        exec.submit(getRunnableSearchWordInFile(queue, exec, map2));
        while (!exec.isShutdown()) {
            exec.isShutdown();
        }
    }

    private static Runnable getRunnableSearchWordInFile(ArrayBlockingQueue<File> queue, ExecutorService exec, HashMap<String, Integer> map) {
        Runnable run = () -> {
            try {
                while (true) {
                    File f = queue.take();
                    if (f.getName().equals("mirage.txt")) {
                        exec.shutdownNow();
                        break;
                    }
                    String[] words = Files.readString(f.toPath()).split("\\PL+");
                    for (String s : words) {
                        map.put(s, (map.containsKey(s)) ? map.get(s) + 1 : 1);
                    }
                }
            } catch (InterruptedException | IOException e) {
            }
        };
        return run;
    }

    public static void unionMap () {
        Comparator<Map.Entry<String, Integer>> comp = (o1, o2) -> o2.getValue() - o1.getValue();
        Map<String, Integer> res = new HashMap<>(map1);
        for (Map.Entry<String, Integer> entry : map2.entrySet()) {
            res.put(entry.getKey(), (res.containsKey(entry.getKey())) ? res.get(entry.getKey()) + entry.getValue() : 1);
        }
        res =  res.entrySet().stream().sorted(comp).limit(10).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (k1, k2)-> k1, LinkedHashMap::new));
        res.forEach((K, V) -> System.out.println(K + " " + V));
    }
}
