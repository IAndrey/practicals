package Part_10.Ex_27;

import Part_3.Ex_1_and_2.Ex;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.function.Supplier;

public class MyCompletableFuture<T> extends CompletableFuture<T> {

    @Override
    public boolean cancel(boolean b) {
        try {
            Thread.sleep(1000);
            if (!this.isDone()) {
                super.cancel(true);
                return true;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }
}
