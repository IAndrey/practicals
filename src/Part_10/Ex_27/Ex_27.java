package Part_10.Ex_27;

import java.util.concurrent.*;
import java.util.function.Supplier;

/*
Реализуйте статический метод CompletableFuture<T> <T> supplyAsync(Supplier<T> action, Executor exec),
возвращающий экземпляр подкласса, производного от класса CompletableFuture<T>, метод cancel()
которого может прервать поток, где исполняется метод action(), при условии что выполняется задача.
Перехватите сначала текущий поток исполнения в задаче типа Runnable, а затем вызовите метод
action.get() и завершите будущее действие типа CompletableFuture конкретным результатом или исключением.
 */
public class Ex_27 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Executor exec = Executors.newFixedThreadPool(3);
        CompletableFuture<Runnable> com = supplyAsync(Ex_27::myRunnable, exec);
        com.get().run();

        com.cancel(true);
    }

    public static <T> CompletableFuture<T> supplyAsync(Supplier<T> action, Executor exec) {
        CompletableFuture<T> myComp = new CompletableFuture<T>();
        myComp = myComp.completeAsync(action, exec);
        return myComp;
    }

    private static Runnable myRunnable() {
        return () -> {
            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(500);
                    System.out.println("Run " + Thread.currentThread().getName());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }
}
