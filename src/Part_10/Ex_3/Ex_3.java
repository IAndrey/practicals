package Part_10.Ex_3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/*
Реализуйте метод, возвращающий задачу для чтения всех слов из файла с целью
найти в нем заданное слово. Если задача прерывается, она должна быть завершена немедленно
с выдачей отладочного сообщения. Запланируйте выполнение этой задачи для каждого файла в каталоге.
Как только одна из задач завершится успешно, все остальные задачи должны быть немедленно прер-
ваны.
 */
public class Ex_3 {
    public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
        System.out.println(method(Paths.get("./src/Part_10/Ex_3/Folder"), "word"));
    }

    public static String method(Path start, String word) throws IOException, ExecutionException, InterruptedException {
        List<Path> files = Files.walk(start).filter(f -> f.toFile().isFile()).collect(Collectors.toList()); // формирую
        // список файлов в каталоге, подкаталоги отфильтруются.
        ExecutorService exec = Executors.newFixedThreadPool(files.size());
        String result = exec.invokeAny(searchWord(files, word)); // выполняем для каждого файла Callable,
        // пока не будет найден первый удачный результат
        exec.shutdown();

        return result;

    }

    public static List<Callable<String>> searchWord(List<Path> paths, String word) throws IOException {
        List<Callable<String>> list = new ArrayList<>();
        for (Path path : paths) {
            Callable<String> callable = () -> {
                if (Files.readString(path).contains(word)) { // Если нужно проверить прерывание задачи то нужно
//                считывать в цикле while файл построчно и добавить проверку
//                Thread.currentThread().isInterrupted()
                    return word + " " + path.getFileName();
                } else throw new IOException(); // если слово не найдено прогружаем ошибку, т.к. нам важен
//                первый удачный результат
            };
            list.add(callable);
        }
        return list;
    }
}
