package Part_10;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/*
Повторите предыдущее упражнение, воспользовавшись глобальным отображением
типа ConcurrentHashMap для накопления частоты, с которой встречается каждое
слово.
 */
public class Ex_15 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Path root = Paths.get("./Test/Part_10/TestDirectory");
        ArrayBlockingQueue<File> queue = new ArrayBlockingQueue<>(200);
        searchFileOneThreadAndAddInQueue(root, queue);
        Map<String, Integer> res = searchWordInFileOnTwoThreads(queue);
        Thread.sleep(1000);
        res.forEach((K, V) -> System.out.println(K + " " + V));
    }

    public static void searchFileOneThreadAndAddInQueue(Path root, ArrayBlockingQueue queue) {
        new Thread(getRunnableSearchFileAndAddInQueue(root, queue)).start();
    }

    private static void searchFileInDirectoryAndAddInQueue(Path root, ArrayBlockingQueue<File> queue) throws InterruptedException {
        if (root.toFile().isFile()) throw new RuntimeException("Начальный путь должен быть дирректорией");
        File[] files = root.toFile().listFiles();
        for (File f : files) {
            if (f.isDirectory()) {
                searchFileInDirectoryAndAddInQueue(f.toPath(), queue);
            } else queue.put(f);
        }
    }

    private static Runnable getRunnableSearchFileAndAddInQueue(Path root, ArrayBlockingQueue<File> queue) {
        Runnable run = () -> {
            try {
                searchFileInDirectoryAndAddInQueue(root, queue);
//                queue.put(new File("mirage.txt"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        return run;
    }

    public static Map<String, Integer> searchWordInFileOnTwoThreads(ArrayBlockingQueue<File> queue) throws ExecutionException, InterruptedException {
        Comparator<Map.Entry<String, Integer>> comp = (o1, o2) -> o2.getValue() - o1.getValue();
        Map<String, Integer> map = new HashMap<>();
        ExecutorService exec = Executors.newFixedThreadPool(2);
        exec.submit(getRunnableSearchWordInFile(exec, queue, map));
        exec.submit(getRunnableSearchWordInFile(exec, queue, map));
        return map.entrySet().stream().sorted(comp).limit(10).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (k1, k2) -> k1, LinkedHashMap::new));
    }

    private static Runnable getRunnableSearchWordInFile(ExecutorService exec, ArrayBlockingQueue<File> queue, Map<String, Integer> map) {
        return () -> {
            while (queue.size() > 0) {
                try {
                    File f = queue.take();
                    String[] words = Files.readString(f.toPath()).split("\\PL+");
                    for (String s : words) {
                        map.put(s, (map.containsKey(s)) ? map.get(s) + 1 : 1);
                    }
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }
}
