package Part_10;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/*
Повторите предыдущее упражнение, воспользовавшись на этот раз интерфейсом
ExecutorCompletionService и объединив полученные результаты, как только
все они будут доступны.
 */
public class Ex_14 {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Path root = Paths.get("./Test/Part_10/TestDirectory");
        ArrayBlockingQueue<Future<String[]>> queue = new ArrayBlockingQueue<>(200);
        searchFileOneThreadAndAddInQueue(root, queue);
        Thread.sleep(1000);
        Map<String, Integer> map = searchWordInFileOnTwoThreads(queue);

    }

    public static void searchFileOneThreadAndAddInQueue(Path root, ArrayBlockingQueue<Future<String[]>> queue) {
        ExecutorService exec = Executors.newFixedThreadPool(1);
        new Thread(getRunnableSearchFileAndAddInQueue(root, queue, exec)).start();
    }

    private static Runnable getRunnableSearchFileAndAddInQueue(Path root, ArrayBlockingQueue<Future<String[]>> queue, ExecutorService exec) {
        Runnable run = () -> {
            try {
                searchFileInDirectoryAndAddInQueue(root, queue, exec);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        return run;
    }

    private static void searchFileInDirectoryAndAddInQueue(Path root, ArrayBlockingQueue<Future<String[]>> queue, ExecutorService exec) throws InterruptedException {
        if (root.toFile().isFile()) throw new RuntimeException("Начальный путь должен быть дирректорией");
        File[] files = root.toFile().listFiles();
        for (File f : files) {
            if (f.isDirectory()) {
                searchFileInDirectoryAndAddInQueue(f.toPath(), queue, exec);
            } else queue.put(exec.submit(getRunnableSearchWordInFile(exec, f)));
        }
    }


    public static Map<String, Integer> searchWordInFileOnTwoThreads(ArrayBlockingQueue<Future<String[]>> queue) throws InterruptedException, ExecutionException {
        Comparator<Map.Entry<String, Integer>> comp = (o1, o2) -> o2.getValue() - o1.getValue();
        HashMap<String, Integer> map = new HashMap<>();
        ExecutorService exec = Executors.newFixedThreadPool(2);
        ExecutorCompletionService<String[]> ex = new ExecutorCompletionService<>(exec, queue);
        Future<String[]> fut;
        while ((fut = ex.poll()) != null) {
            String[] str1 = fut.get();
            for (String s : str1) {
                map.put(s, (map.containsKey(s)) ? map.get(s) + 1 : 1);
            }
        }
        return map.entrySet().stream().sorted(comp).limit(10).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (k1, k2)-> k1, LinkedHashMap::new));
    }

    private static Callable<String[]> getRunnableSearchWordInFile(ExecutorService exec, File f) {
        return () -> {
            try {
                String[] words = Files.readString(f.toPath()).split("\\PL+");
                return words;
            } catch (IOException e) {
                e.printStackTrace();
                throw new Exception();
            }
        };
    }

}
