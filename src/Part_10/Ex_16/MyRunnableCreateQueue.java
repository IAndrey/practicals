package Part_10.Ex_16;

import java.io.File;
import java.nio.file.Path;
import java.util.concurrent.ArrayBlockingQueue;

public class MyRunnableCreateQueue {
    private static ArrayBlockingQueue<File> queue = new ArrayBlockingQueue<>(200);

    public static ArrayBlockingQueue<File> getQueue (Path root) {
        searchFileOneThreadAndAddInQueue(root, queue);
        return queue;
    }

    private static void searchFileOneThreadAndAddInQueue(Path root, ArrayBlockingQueue queue) {
        new Thread(getRunnableSearchFileAndAddInQueue(root, queue)).start();
    }

    private static void searchFileInDirectoryAndAddInQueue(Path root, ArrayBlockingQueue<File> queue) throws InterruptedException {
        if (root.toFile().isFile()) throw new RuntimeException("Начальный путь должен быть дирректорией");
        File[] files = root.toFile().listFiles();
        for (File f : files) {
            if (f.isDirectory()) {
                searchFileInDirectoryAndAddInQueue(f.toPath(), queue);
            } else queue.put(f);
        }
    }

    private static Runnable getRunnableSearchFileAndAddInQueue(Path root, ArrayBlockingQueue<File> queue) {
        Runnable run = () -> {
            try {
                searchFileInDirectoryAndAddInQueue(root, queue);
//                queue.put(new File("mirage.txt"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        return run;
    }
}
