package Part_10.Ex_16;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/*
Повторите предыдущее упражнение, воспользовавшись параллельными потоками данных.
Ни одна из потоковых операций не должна иметь никаких побочных эффектов.
 */
public class Ex_16 {
    public static void main(String[] args) throws InterruptedException {
        ArrayBlockingQueue<File> queue = MyRunnableCreateQueue.getQueue(Paths.get("./Test/Part_10/TestDirectory"));
        ArrayList<String> list = new ArrayList<>();
        Map<String, Integer> map = getMap(queue, list);
        map.forEach((K, V) -> System.out.println(K + " " + V));
    }

    public static Map<String, Integer> getMap(ArrayBlockingQueue<File> queue, ArrayList<String> list) throws InterruptedException {
        Map<String, Integer> result = new LinkedHashMap<>();
        Comparator<Map.Entry<String, Integer>> comp = (o1, o2) -> o2.getValue() - o1.getValue();
        Thread th1 = new Thread(getRunnableSearchWordInFile(queue, list));
        th1.start();
        Thread th2 = new Thread(getRunnableSearchWordInFile(queue, list));
        th2.start();
        th1.join();
        th2.join();
        for (String s : list) {
            result.put(s, (result.containsKey(s)) ? result.get(s) + 1 : 1);
        }
        return result.entrySet().stream().sorted(comp).limit(10).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (k1, k2) -> k1, LinkedHashMap::new));
    }

    private static Runnable getRunnableSearchWordInFile(ArrayBlockingQueue<File> queue, ArrayList<String> list) {
        Runnable run = () -> {
            try {
                while (true) {
                    File f = queue.poll(1, TimeUnit.SECONDS);
                    String[] words = Files.readString(f.toPath()).split("\\PL+");
                    list.addAll(Arrays.asList(words));
                }
            } catch (InterruptedException | IOException | NullPointerException e) {
            }
        };
        return run;
    }

}
