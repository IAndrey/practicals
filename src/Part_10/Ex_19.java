package Part_10;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.LongAdder;
import java.util.concurrent.locks.Lock;
import java.util.stream.Collectors;

/*
Усовершенствуйте программу из предыдущего упражнения, используя
класс LongAdder.
 */
public class Ex_19 {
    public static LongAdder count = new LongAdder();

    public static void main(String[] args) throws IOException, InterruptedException {
        List<Path> list = Files.walk(Paths.get("./Test/Part_10/TestDirectory")).filter(p -> p.toFile().isFile()).collect(Collectors.toList());
        for (Path p : list) {
            new Thread(getRunnable(p.toFile())).start();
        }
        Thread.sleep(200);
        System.out.println(count);
    }

    private static Runnable getRunnable(File file) {
        Runnable run = () -> {
            try {
                int x = Files.readString(file.toPath()).split("\\PL+").length;
                count.add(x);
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
        return run;
    }
}
