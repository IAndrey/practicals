package Part_10.Ex_20;

import java.util.IdentityHashMap;

/*
Первый путь заключается в том что, при добавлении элементов, я использую 2 потока.
В результате пре выполнении метода push, класса Stack, оба потока заходят в него
и перезатирают данные. В следствии это объекты могут потеряться. Выход - объявить
метод push синхронизированным.
 */
public class Ex_20_p1 {
    public static void main(String[] args) throws InterruptedException {
        Stack stack = new Stack();
        pushInStack(stack);
        for (int i = 0; i < 100; i++) {
            System.out.println(stack.pop());
        }
    }

    private static void pushInStack(Stack stack) throws InterruptedException {
        Thread th1 = new Thread(run1(stack));
        Thread th2 = new Thread(run2(stack));
        th1.start();
        th2.start();
        th1.join();
        th2.join();
    }

    private static Runnable run2(Stack stack) {
        Runnable run = () -> {
            for (int i = 50; i < 101; i++) {
                stack.push(i + " run2");
            }
        };
        return run;
    }

    private static Runnable run1(Stack stack) {
        Runnable run = () -> {
            for (int i = 0; i < 50; i++) {
                stack.push(i + " run1");
            }
        };
        return run;
    }

}
