package Part_10.Ex_20;

/*
Вторым путь, на мой взгляд заключается в том, что стек может содержать разные объекты,
т.е. разные потоки могу добавлять в него разные объекты данныъ
   */
public class Ex_20_p2 {
    public static void main(String[] args) throws InterruptedException {
        Stack stack = new Stack();
        pushInStack(stack);
        Thread.sleep(1000);
        for (int i = 0; i < 100; i++) {
            System.out.println(stack.pop());
        }
    }

    private static void pushInStack(Stack stack) throws InterruptedException {
        Thread th1 = new Thread(run1(stack));
        Thread th2 = new Thread(run2(stack));
        th1.start();
        th2.start();
    }

    private static Runnable run2(Stack stack) {
        Runnable run = () -> {
            for (int i = 50; i < 101; i++) {
                stack.push(i + " run2");
            }
        };
        return run;
    }

    private static Runnable run1(Stack stack) {
        Runnable run = () -> {
            for (int i = 0; i < 50; i++) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                stack.push(i);
            }
        };
        return run;
    }
}
