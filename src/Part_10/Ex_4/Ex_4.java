package Part_10.Ex_4;

import java.util.Arrays;
/*
Числа Фибоначчи 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765 ...
 */
public class Ex_4 {
    public static void main(String[] args) {
        System.out.println(getResultFibonachi(20));
    }
    public static int getResultFibonachi(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        int [][][] res = getArray(n);
        Arrays.parallelPrefix(res, Matrix::matrixMultiplication);
        return res[n-1][1][1];
    }

    private static int[][][] getArray(int n) {
        int[][] matr = {
                {1, 1},
                {1, 0}
        };
        int [][][] res = new int[n][2][2];
        Arrays.parallelSetAll(res, (i) -> matr);
        return res;
    }
}
