package Part_10.Ex_4;

public class Matrix {

    // матричное умножение двух матриц размера 2 на 2
    public static int[][] matrixMultiplication(int[][] a, int[][] b) {
        return new int[][]{
                {a[0][0] * b[0][0] + a[0][1] * b[1][0], a[0][0] * b[0][1] + a[0][1] * b[1][1]},
                {a[1][0] * b[0][0] + a[1][1] * b[1][0], a[1][0] * b[0][1] + a[1][1] * b[1][1]},
        };
    }
}
