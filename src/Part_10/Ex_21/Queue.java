package Part_10.Ex_21;

/*
Рассмотрите следующую реализацию очереди:

Опишите два разных пути, приводящих к тому, что эта структура данных
может содержать неверные элементы.
 */
public class Queue {
    private int size = 0;

    class Node {
        Object value;
        Node next;
    }

    private Node head;
    private Node tail;

    public void add(Object newValue) {
        Node n = new Node();
        if (head == null) head = n;
        else tail.next = n;
        tail = n;
        tail.value = newValue;
        size ++;
    }

    public Object remove() {
        if (head == null) return null;
        Node n = head;
        head = n.next;
        size --;
        return n.value;
    }

    public int getSize() {
        return size;
    }
}
