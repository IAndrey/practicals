package Part_10.Ex_21;


/*
Пример 1 - если одновременно 10 000 потоков будут пытаться
ввести в очередь элемент, то из них может что то потеряться,
т.к. очредь не синхронизирована.
 */
public class Ex_21_p1 {
    public static void main(String[] args) throws InterruptedException {
        Queue queue = new Queue();
        pushInStack(queue);
        Thread.sleep(1000);
        System.out.println(queue.getSize());
    }

    private static void pushInStack(Queue queue) throws InterruptedException {
        for (int i = 0; i < 10000; i++) {
            new Thread(run1(queue)).start();
        }
    }

    private static Runnable run1(Queue queue) {
        Runnable run = () -> {
            queue.add("1");
        };
        return run;
    }
}
