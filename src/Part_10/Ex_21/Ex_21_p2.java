package Part_10.Ex_21;
/*
Пример 2 - удаление из очереди, при многократном запуске этой программы, из очереди
удаляют элементы 10 000 потоков, в результате результат не всегда приводит к полному удалению
всех элементов.
 */
public class Ex_21_p2 {
    public static void main(String[] args) throws InterruptedException {
        Queue queue = new Queue();
        for (int i = 0; i < 10000; i++) {
            queue.add(i);
        }
        removeInStack(queue);
        Thread.sleep(1000);
        System.out.println(queue.getSize());
    }

    private static void removeInStack(Queue queue) throws InterruptedException {
        for (int i = 0; i < 10000; i++) {
            new Thread(run1(queue)).start();
        }
    }

    private static Runnable run1(Queue queue) {
        return queue::remove;
    }
}
