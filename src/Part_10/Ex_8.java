package Part_10;

import java.util.Map;
import java.util.concurrent.*;
import java.util.function.BiFunction;

/*
Найдите в отображении типа ConcurrentHashMap<String, Long> ключ
с максимальным значением, произвольно отбрасывая лишнее. Подсказска:
воспользуйтесь методом reduceEntries().
 */
public class Ex_8 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ConcurrentHashMap<String, Long> map = new ConcurrentHashMap<>();
        map.put("1", 1L);
        map.put("2", 2L);
        map.put("3", 3L);

        ExecutorService exec = Executors.newFixedThreadPool(3);
        Future result = exec.submit(getRunnable(map));
        Long x = ((Map.Entry<String, Long>) result.get()).getValue();
        exec.shutdown();
        System.out.println(x);
    }

    private static Callable getRunnable(ConcurrentHashMap<String, Long> map) {
        Callable run = () -> {
            Map.Entry<String, Long> entry = null;
            for (Map.Entry<String, Long> entries : map.entrySet()) {
                entry = map.reduceEntries(3, getMyBiFunction());
            }
            return entry;
        };
        return run;
    }

    private static BiFunction<Map.Entry<String, Long>, Map.Entry<String, Long>, Map.Entry<String, Long>>
    getMyBiFunction() {
        BiFunction<Map.Entry<String, Long>, Map.Entry<String, Long>, Map.Entry<String, Long>> function =
                (K, V) -> {
                    if (V.getValue() < K.getValue()) {
                        return K;
                    }
                    return V;
                };
        return function;
    }

}
