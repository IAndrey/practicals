package Part_10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Напишите программу, запрашивающую у пользователя URL, читающую веб-страницу
по этому URL и выводящую на экран все ссылки на ней. Воспользуйтесь для каждой
из этих стадий классом CompletableFuture. Только не вызывайте метод get().

CompletableFuture - предназначен для последовательного выполнения задач, с помо-
щью методов thenApplyAsync и тд, нам возвращается объект CompletableFuture<U>, где U -
результат выполнения функционального интерфейса Function, который приходит в параметры
методов thenApplyAsync ..., у которого мы можем запустить выполнение следующего задания
тем же методом. thenApply отличается от thenApplyAsync, тем, что второй выполняется в
отдельном потоке. Потоки можно закинуть в методы ...Async втором параметром - Executor.

Пример такого последовательного выполнения показан ниже в решении упражнения.
 */
public class Ex_25 {
    public static void main(String[] args) throws IOException {
//        URL url = new URL("https://ru.wikipedia.org/wiki/Заглавная_страница");
        CompletableFuture<URL> content = getUrl();
        List<String> result = content.thenApplyAsync(Ex_25::readPage).thenApplyAsync(Ex_25::getLinks).join();
        result.forEach(System.out::println);
    }

    public static CompletableFuture<URL> getUrl() {
        System.out.println("ВВедите URL");
        Scanner scan = new Scanner(System.in);
        CompletableFuture<URL> com = new CompletableFuture<>();
        try {
            URL url = new URL(scan.nextLine());
            com.obtrudeValue(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return com;
    }

    private static String readPage(URL url) {
        CompletableFuture<String> com = new CompletableFuture<>();
        String res = "";
        String input = "andrey19931" + ":" + "andrey199311";
        String encoding = Base64.getEncoder().encodeToString(input.getBytes(StandardCharsets.UTF_8));
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            InputStream content = connection.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(content));
            String line;
            while ((line = in.readLine()) != null) {
                res += line + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    private static List<String> getLinks(String page) {
        List<String> res = new ArrayList<>();
        Matcher matcher = Pattern.compile("href=\"https?.+?\"").matcher(page);
        while (matcher.find()) {
            res.add(matcher.group());
        }
        return res;
    }

}
