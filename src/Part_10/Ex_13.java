package Part_10;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/*
Повторите предыдущее упражнение, создав объект типа Callable<Map<String, Integer>> для
каждого файла и воспользовавшись подходящей службой исполнителя. Объедините получен-
ные результаты, как только все они будут доступны. Почему для этой цели не потребуется
отображение типа ConcurrentHashMap?
 */
public class Ex_13 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Path root = Paths.get("./Test/Part_10/TestDirectory");
        ArrayBlockingQueue<File> queue = new ArrayBlockingQueue<>(200);
        searchFileOneThreadAndAddInQueue(root, queue);
        Map<String, Integer> map = searchWordInFileOnTwoThreads(queue);

    }

    public static void searchFileOneThreadAndAddInQueue(Path root, ArrayBlockingQueue queue) {
        new Thread(getRunnableSearchFileAndAddInQueue(root, queue)).start();
    }

    private static void searchFileInDirectoryAndAddInQueue(Path root, ArrayBlockingQueue<File> queue) throws InterruptedException {
        if (root.toFile().isFile()) throw new RuntimeException("Начальный путь должен быть дирректорией");
        File[] files = root.toFile().listFiles();
        for (File f : files) {
            if (f.isDirectory()) {
                searchFileInDirectoryAndAddInQueue(f.toPath(), queue);
            } else queue.put(f);
        }
    }

    private static Runnable getRunnableSearchFileAndAddInQueue(Path root, ArrayBlockingQueue<File> queue) {
        Runnable run = () -> {
            try {
                searchFileInDirectoryAndAddInQueue(root, queue);
                queue.put(new File("mirage.txt"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        return run;
    }

    public static Map<String, Integer> searchWordInFileOnTwoThreads(ArrayBlockingQueue<File> queue) throws ExecutionException, InterruptedException {
        Comparator<Map.Entry<String, Integer>> comp = (o1, o2) -> o2.getValue() - o1.getValue();
        HashMap<String, Integer> map = new HashMap<>();
        ExecutorService exec = Executors.newFixedThreadPool(20);
        List<Callable<String[]>> list = new ArrayList<>();
        for (File f : queue) {
            list.add(getRunnableSearchWordInFile(exec, f));
        }
        List<Future<String[]>> futures = exec.invokeAll(list);
        for (Future<String[]> fu : futures) {
            String[] ss = fu.get();
            for (String s : ss) {
                map.put(s, (map.containsKey(s)) ? map.get(s) + 1 : 1);
            }
        }
        return map.entrySet().stream().sorted(comp).limit(10).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (k1, k2)-> k1, LinkedHashMap::new));
    }

    private static Callable getRunnableSearchWordInFile(ExecutorService exec, File f) {
        return () -> {
            if (f.getName().equals("mirage.txt")) {
                exec.shutdownNow();
                return new String[0];
            } else {
                try {
                    String[] words = Files.readString(f.toPath()).split("\\PL+");
                    return words;
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new Exception();
                }
            }
        };
    }
}
