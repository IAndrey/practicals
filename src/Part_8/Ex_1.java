package Part_8;

import java.lang.module.FindException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;


/*
Ex_1
Убедитесь, что запрос первых пяти длинных слов не требует вызова метода filter(),
если найдено пятое длинное слово. С этой целью просто организуйте протоко-
лирование вызова каждого метода.
 */
public class Ex_1 {
    private static Logger logger = myLogger();
    public static void main(String[] args) {
        logger.info("Вызов main()");
        List<String> list = Arrays.asList("asdaddd", "adasdsadasd", "erewdsgdsg", "olpsdpomsvksp", "papsdklfdposcmdskap", "dasdassp[k[,mp");
        String elementNumberFive = list.stream().skip(4).findFirst().orElseThrow(()-> new FindException("Елемент не найден"));// если в потоке нет 5 элемента, прогрузим ошибку.
        if (!(elementNumberFive.length() > 10)) {
            logger.info("Вызов filter()");
            list = list.stream().filter(t -> t.length() > 10).collect(Collectors.toList());
        }
    }

    public static Logger myLogger() {
        Logger myLogger = Logger.getGlobal();
        myLogger.setLevel(Level.INFO);
        myLogger.setUseParentHandlers(false);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.INFO);
        myLogger.addHandler(handler);
        return myLogger;
    }
}
