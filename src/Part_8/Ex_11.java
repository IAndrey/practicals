package Part_8;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Ex_11
Определите все символьные строки максимальной длинны в задданном
конечном потоке символьных строк.
 */
public class Ex_11 {
    public static void main(String[] args) {
        maxLengthWord(Stream.of("asa", "s", "asdds", "as", "oireg")).forEach(System.out::println);
    }

    public static List<String> maxLengthWord(Stream<String> stream) {
        List<String> list = stream.collect(Collectors.toList());
        int maxLength = list.stream().reduce(0,
                (totalLength, str) -> {
                    if (totalLength < str.length()) {
                        totalLength = str.length();
                    }
                    return totalLength;
                }, Integer::compareTo);
        return list.stream().filter(str -> str.length() == maxLength).collect(Collectors.toList());
    }
}
