package Part_8;

import java.util.Arrays;
import java.util.function.IntPredicate;
import java.util.function.Predicate;

/*
Ex_6
Воспользуйтесь методом String.codePoints() для реализации метода, проверяющего,
является ли симпвольная строка словом, состоящим только из букв.
(Подсказка: воспользуйтесь методом Character.isAlphabetic().) Реализуйте тем же самым способом
метод, проверяющий, является ли символьная строка достоверным в Java идентификатором.

Идентификаторы:
Имена переменных, классов, объектов, интерфейсов, методов называются идентификаторами. Названия идентификаторов выбираются по следующим правилам:
• они должны начинаться с буквы или символа подчеркивания;
• они могут содержать латинские буквы, символы подчеркивания или цифры без пробелов;
• названия идентификаторов не должны совпадать с ключевыми словами.
 */
public class Ex_6 {
    public static void main(String[] args) {
//        String str = "ssis";
//        isWord(str);
        String str2 = "_synchronized";
        System.out.println(isIdentificator(str2));
    }

    public static boolean isWord(String str) {
        return str.codePoints().allMatch(Character::isAlphabetic);
    }

    public static boolean isIdentificator(String str) {
        if (str.codePoints().findFirst().isEmpty()) return false;
        int x = str.codePoints().findFirst().getAsInt();
        if (!Character.isAlphabetic(x)) {
            if (!Character.valueOf((char) x).equals('_')) return false;
        }
        if (str.codePoints().anyMatch(Character::isSpaceChar)) return false;
        String[] keywords = {"abstract", "boolean", "Break", "byte", "case", "catch", "Char", "class", "const",
                "continue", "default", "do", "double", "else", "extends", "false", "final", "finally", "Float",
                "for", "goto", "if", "implements", "import", "instanceof", "int", "interface", "long", "native",
                "new", "null", "package", "private", "protected", "public", "return", "short", "static", "strictfp",
                "super", "switch", "synchronized", "this", "throw", "throws", "transient", "true", "try", "void", "volatile", //Все ключевые слова Java
                "while"};
//        if (Arrays.stream(keywords).anyMatch(t -> t.equals(str))) return false; //Или строка ниже
        if (Arrays.asList(keywords).contains(str)) return false;
        return true;
    }
}
