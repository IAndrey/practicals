package Part_8;

import java.util.ArrayList;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

/*
Ex_14
Соедините все элементы в потоках данных Stream<ArrayList<T>> и ArrayList<T>.
Покажите, как добиться этого с помощью каждой из трех форм метода
reduce().
 */
public class Ex_14 {
    public static void main(String[] args) {
//        String s = Stream.of(1, 2, 3).reduce("", (t1, t2)-> t2+"", String::concat);
        ArrayList<String> list1 = new ArrayList<>();
        list1.add("1"); list1.add("2"); list1.add("3");
        ArrayList<String> list2 = new ArrayList<>();
        list2.add("4"); list2.add("5"); list2.add("6");
        ArrayList<String> list3 = new ArrayList<>();
        list3.add("7"); list3.add("8"); list3.add("9");
        join(Stream.of(list1, list2), list3).forEach(System.out::println);
    }

    public static <T> Stream<T> join(Stream<ArrayList<T>> stream, ArrayList<T> list) {
        Stream<T> listStream = list.stream();
        return stream.reduce(listStream, (_stream, _list) -> Stream.concat(_stream, _list.stream()), Stream::concat);
    }
}
