package Part_8;

import java.util.ArrayList;
import java.util.stream.Stream;

/*
Ex_18
Каким образом можно исключить получение смежных дубликатов из потока
данных? Сможет ли написанный вами метод обрабатывать параллельный по-
ток?

Исключить получение смежных дубликатов из потока данных можно с помощью
метода distinct().Параллельный поток может его обработать, но выгоды
в этом нет, т.к. в потоке исполнения, обрабатывающем отдельный элемент,
неизвестно какие именно элементы следует отбросить, до тех пор, пока
сегмент не будет обработан. Что демонстирует пример ниже.
 */
public class Ex_18 {
    public static void main(String[] args) {
        System.out.println(getTime(init().stream()));
        System.out.println(getTime(init().parallelStream()));
    }

    private static Long getTime(Stream<String> stream) {
        Long start = System.currentTimeMillis();
        stream.unordered().distinct().forEach(t-> t.length());
        return System.currentTimeMillis() - start;
    }

    private static ArrayList<String> init() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 9000000; i++) {
            list.add(i+"");
        }
        for (int i = 0; i < 9000000; i++) {
            list.add(i+"");
        }
        return list;
    }
}
