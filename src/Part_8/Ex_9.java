package Part_8;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*
Прочитайте слова из файла в поток данных и получите массив всех слов, содержащих
пять отдельных гласных.
 */
public class Ex_9 {
    public static void main(String[] args)
    {
        System.out.println(filter("dadedudido"));
    }

    public static List<String> fiveWords(String path) throws IOException {
        String[] words = new String(Files.readAllBytes(Paths.get(path))).split("\\PL+");
        return Arrays.stream(words).filter(Ex_9::filter).collect(Collectors.toList());
    }

    private static boolean filter(String str) {
        char[] vowels = {'a', 'e', 'i', 'o', 'u'};
        boolean check = true;
        int total = 0;
        for (char c : str.toCharArray()) {
            if (contains(c, vowels) && check) {
                total++;
                check = false;
            } else {
                check = true;
            }
        };
        return total == 5;
    }

    private static boolean contains(char c, char[] chars) {
        for (char x : chars) {
            if (c == x) return true;
        }
        return false;
    }
}
