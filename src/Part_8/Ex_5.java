package Part_8;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/*
Метод CodePoints(), упоминавшийся в разделе 8.3, выглядит несколько неуклюже,
поскольку в нем сначала получается списочный массив, а затем он превращается в поток
данных. Напишите другой, однострочный вариант этого метода, используя метод IntStream.iterate(),
что бы создать сначала конечный поток смещений, а затем извлечь из него подстроки.

    public static Stream<String> CodePoints(String s) {
        List<String> result = new ArrayList<>();
        int i = 0;
        while (i < s.length()) {
            int j = s.offsetByCodePoints (i, 1);
            result.add (s.substring (i, j));
            i = j;
        }
        return result.stream();
    }

 */
public class Ex_5 {
    public static void main(String[] args) {
        myCodePoints("asd").forEach(System.out::println);
//        codePoints("asd").forEach(System.out::println);
    }

    public static Stream<String> codePoints(String s) {
        List<String> result = new ArrayList<>();
        int i = 0;
        while (i < s.length()) {
            int j = s.offsetByCodePoints(i, 1);
            result.add(s.substring(i, j));
            i = j;
        }
        return result.stream();
    }

    public static Stream<String> myCodePoints(String s) {
        return IntStream.range(0, s.length()).mapToObj(integer -> s.substring(integer, integer + 1));
    }
}
