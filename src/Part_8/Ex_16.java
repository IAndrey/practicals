package Part_8;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/*
Найдите 500 простых чисел с 50 десятичными цифрами, используя
параллельный поток данных типа BigInteger и метод
bigInteger.isProbablePrime(). Насколько это делается быстрее,
чем при использовании последовательного потока данных?

Десятичные цифры изображают литерами 0, 1, 2, 3, 4, 5, 6, 7, 8, 9.

Простое число — натуральное (целое положительное) число, имеющее ровно два
различных натуральных делителя — единицу и самого себя
 */
public class Ex_16 {
    public static void main(String[] args) {
        timeStream();
        timeParallelStream();
    }

    public static Long timeStream() {
        Long currentTimeStart = System.currentTimeMillis();
        ArrayList<BigInteger> list = init();
        AtomicInteger y = new AtomicInteger();
        list.stream().filter(bigInteger -> {
                    if (!(bigInteger.toString().length() == 50)) return false; // проверяем что длина 50 десятичных цифр
                    if (!bigInteger.isProbablePrime(1)) return false; // проверяем что число простое
                    return true;
                }
        ).limit(500).forEach(t->t.toString()); // находим 500 таких чисел. forEach(t->t.toString()) нужен,
        // чтобы выполнить поток, т.к. выполнение происходи при непосредственном вызове объекта.
        Long currentTimeFinish = System.currentTimeMillis();
        System.out.printf("Последовательный стрим. Время выполнения - %s мс\n", (currentTimeFinish - currentTimeStart));
//        stream.forEach(bigInteger -> System.out.println(bigInteger + " " + y.addAndGet(1)));
        return currentTimeFinish - currentTimeStart;
    }

    public static Long timeParallelStream() {
        Long currentTimeStart = System.currentTimeMillis();
        ArrayList<BigInteger> list = init();
        AtomicInteger y = new AtomicInteger();
        list.parallelStream().filter(bigInteger -> {
                    if (!(bigInteger.toString().length() == 50)) return false; // проверяем что длина 50 десятичных цифр
                    if (!bigInteger.isProbablePrime(1)) return false; // проверяем что число простое
                    return true;
                }
        ).limit(500).forEach(t->t.toString()); // находим 500 таких чисел. forEach(t->t.toString()) нужен,
        // чтобы выполнить поток, т.к. выполнение происходи при непосредственном вызове объекта.
        Long currentTimeFinish = System.currentTimeMillis();
        System.out.printf("Параллельный стрим. Время выполнения - %s мс\n", (currentTimeFinish - currentTimeStart));
//        stream.sequential().forEach(bigInteger -> System.out.println(bigInteger + " " + y.addAndGet(1)));
        return currentTimeFinish - currentTimeStart;
    }

    private static ArrayList<BigInteger> init() {
        ArrayList<BigInteger> list = new ArrayList<>();
        for (int i = 0; i < 100000; i++) {
            list.add(new BigInteger("21312421213214213421425045893409534573458974856384").add(new BigInteger(i + "")));
        }
        for (int i = 0; i < 500; i++) {
            list.add(new BigInteger("21312421213214213421425045893409534573458974856384").divide(new BigInteger(i + "100000000")));
        }
        return list;
    }
}
