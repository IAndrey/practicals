package Part_8.Ex_7;

import Part_8.Ex_6;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Ex_7
Преобразовав содержимое файла в поток лексем, выведите список первых 100
лексем, являющихся словами в том смысле, в каком они определены в предыдущем упражнении.
Прочитайте содержимое файла снова и выведите список из 10 наиболее часто употребляемых слов,
игнорируя регистр букв.

Лексемы (tokens) - это элементарные законченные слова языка.
 */
public class Ex_7 {
    public static void main(String[] args) throws IOException {
//        firstHundredWords("./src/Part_8/Ex_7/test.txt").forEach(System.out::println);
        tenOftenWords("./src/Part_8/Ex_7/test.txt").forEach((K, V) -> System.out.println(K + " " + V));
    }

    public static List<String> firstHundredWords(String paths) throws IOException {
        Stream<String> stream = Arrays.stream(new String(Files.readAllBytes(Paths.get(paths))).split("\\PL+"));
        return stream.filter(Ex_6::isWord).limit(100).collect(Collectors.toList());
    }

    public static Map<String, Integer> tenOftenWords(String paths) throws IOException {
        String[] strings = new String(Files.readAllBytes(Paths.get(paths))).split("\\PL+");
        try (Stream<String> stream = Arrays.stream(strings)) {
            Comparator<String> comp = (str1, str2) -> {
                int x = countWord(str1, Arrays.stream(strings));
                int y = countWord(str2, Arrays.stream(strings));
                return y - x;
            };
            comp.thenComparing(String::compareTo);
            return stream.filter(Ex_6::isWord).map(String::toLowerCase).sorted(comp).distinct().limit(10).
                    collect(Collectors.toMap(
                            (key) -> key,
                            (key) -> countWord(key, Arrays.stream(strings)),
                            (key1, key2) -> key1,
                            LinkedHashMap::new));
        }
    }

    private static int countWord(String str, Stream<String> stream) {
        int x = stream.reduce(0, (total1, string) ->
        {
            if (string.equals(str)) {
                total1++;
            }
            return total1;
        }, Integer::sum);
        stream.close();
        return x;
    }
}
