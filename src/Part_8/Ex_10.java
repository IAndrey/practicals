package Part_8;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Ex_10
Определите среднюю длину строки в заданном конечном потоке символьных строк.
 */
public class Ex_10 {
    public static void main(String[] args) {
        System.out.println(averageLength(Stream.of("1", "2", "3")));
    }
    public static int averageLength(Stream<String> stream) {
        List<String> list = stream.collect(Collectors.toList());
        int length = list.stream().reduce(0, (totalInt, str) -> totalInt+=str.length(), Integer::sum);
        int coumtWord = (int) list.stream().count();
        return length/coumtWord;
    }
}
