package Part_8;

import java.util.stream.Stream;

/*
Допустим, что ваш непосредственный начальник дал вам задание написать метод
public static <T> boolean isFinite(Stream<T> stream). Почему это не самая
удачная мысль? Все равно напишите этот метод.

Это не самая удачная мысль т.к. при стирании типов <T> будет выглядеть как
Stream<Object> stream и объекты этого стима будут иметь только методы класса
Object, следовательно этот поток нельзя отфильтровать по каким то критериям объектов
и применять другие подобные методы. Единственное что можно этот выводить объекты в консоль.
 */
public class Ex_12 {
    public static void main(String[] args) {
        String[] s1 = new String[1];
        String[] s2 = new String[1];
        String[] s3 = new String[1];
        isFinite(Stream.of(s1, s2, s3));
        isFinite(Stream.of(1, 2, 3));
        isFinite(Stream.of("1", "2", "3"));
    }

    public static <T> boolean isFinite(Stream<T> stream) {
        stream.forEach(System.out::println);
        return true;
    }
}
