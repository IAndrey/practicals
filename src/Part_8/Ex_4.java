package Part_8;

import java.util.stream.LongStream;
import java.util.stream.Stream;

/*
C помощью метода Stream.iterate() создайте бесконечный поток случайных чисел,
не вызываю метод Math.random(), а непосредственно реализуя линейный конгруэнтный
генератор. Такой генератор начинает действовать с числа, задаваемого выражением
x0 = начальное зачение, а затем производит случайные числа по формуле
Xn+1 = (a Xn + C) % m, при соответствующих значениях a, C и m. С этой целью реали-
зуйте метод, принимающий параметры a, C и m и получающий поток данных Stream<Long>.
Опробуйте его со следующими параметрами: a = 25214903917, C = 11, m = 2^48.
 */
public class Ex_4 {
    public static void main(String[] args) {
        myGenerator(0).forEach(System.out::println);
    }

    public static LongStream myGenerator(int x0) {
        Long m = new Double(Math.pow(2, 48)).longValue();
        return myHelperGenerator(25214903917L, 11l, m);
    }

    public static LongStream myHelperGenerator(Long a, Long C, Long m) {
        LongStream stream1 = LongStream.iterate(0, Xn -> {
            long x = (a * (Xn + 1) + C) % m;
            return x;
        });
        return stream1;
    }
}
