package Part_8;

import java.util.stream.Stream;

/*
Ex_15
Организуйте вызов метода reduce() таким образом, чтобы вычислить
среднее в потоке данных Stream<Double>. Почему нельзя просто вычислить
сумму и разделить ее на результат, возвращаемый методом count()?

По заданию если вызвать count(), ты будет выведено число 1, так как после
reduce(Double::sum).stream() вернется поток, в котором будет одно число - сумма.
Пример - ниже.
Stream.of(1.5, 1.5, 1.5).reduce(Double::sum).stream().count()
 */
public class Ex_15 {
    public static void main(String[] args) {
        double x = getAverage(Stream.of(1.5, 1.5, 1.5));
        System.out.println(x);
    }

    public static double getAverage (Stream<Double> stream) {
        Averager averager = new Averager(0, 0);
        double x = stream.reduce(averager, Averager::accept, Averager::combine).average();
        return x;
    }

//  Для того чтобы рассчитать среднее значение с помощью reduce(), ввел доп внутренний класс.
    static class Averager {
        private double total = 0;
        private int count = 0;

        public Averager(double total, int count) {
            this.total = total;
            this.count = count;
        }

        public double average() {
            return count > 0 ? ((double) total) / count : 0;
        }

        public Averager accept(double i) {
            total = +i;
            count = +1;
            return this;
        }

        public Averager combine(Averager other) {
            return new Averager(total + other.total, count + other.count);
        }
    }
}
